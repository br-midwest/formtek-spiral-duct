
ACTION InitPars: 

	// template
//	par[0].name			:= '';
//	par[0].code			:= '';
//	par[0].unit			:= '';
//	par[0].index		:= 16#;
//	par[0].subindex		:= 16#;	
//	par[0].type			:= DATATYPE_;	
//	par[0].access		:= ACCESS_;
//	par[0].visible		:= TRUE;

	
	par[0].name			:= 'Mains Voltage';
	par[0].code			:= 'ULN';
	par[0].unit			:= '0.1 V';
	par[0].index		:= 16#2002;
	par[0].subindex		:= 16#08;	
	par[0].type			:= DATATYPE_UINT;	
	par[0].access		:= ACCESS_R;
	par[0].visible		:= TRUE;
	
	par[1].name			:= 'Nameplate: Rated Motor Power';
	par[1].code			:= 'NPR';
	par[1].unit			:= '0.1 kW';
	par[1].index		:= 16#2042;
	par[1].subindex		:= 16#0E;	
	par[1].type			:= DATATYPE_UINT;	
	par[1].access		:= ACCESS_RWS;
	par[1].visible		:= TRUE;
	
	par[2].name			:= 'Nameplate: Rated Motor Voltage';
	par[2].code			:= 'UNS';
	par[2].unit			:= 'V';
	par[2].index		:= 16#2042;
	par[2].subindex		:= 16#02;	
	par[2].type			:= DATATYPE_UINT;	
	par[2].access		:= ACCESS_RWS;
	par[2].visible		:= TRUE;
	
	par[3].name			:= 'Nameplate: Rated Motor Frequency';
	par[3].code			:= 'FRS';
	par[3].unit			:= '0.1 Hz';
	par[3].index		:= 16#2042;
	par[3].subindex		:= 16#03;	
	par[3].type			:= DATATYPE_UINT;	
	par[3].access		:= ACCESS_RWS;
	par[3].visible		:= TRUE;

	par[4].name			:= 'Nameplate: Rated Motor Current';
	par[4].code			:= 'NCR';
	par[4].unit			:= '0.1 A';
	par[4].index		:= 16#2042;
	par[4].subindex		:= 16#4;	
	par[4].type			:= DATATYPE_UINT;	
	par[4].access		:= ACCESS_RWS;
	par[4].visible		:= TRUE;

	par[5].name			:= 'Nameplate: Power Factor';
	par[5].code			:= 'COS';
	par[5].unit			:= 'unitless';
	par[5].index		:= 16#2042;
	par[5].subindex		:= 16#07;	
	par[5].type			:= DATATYPE_UINT;	
	par[5].access		:= ACCESS_RWS;
	par[5].visible		:= TRUE;

	par[6].name			:= 'Nameplate: Rated Motor Speed';
	par[6].code			:= 'NSP';
	par[6].unit			:= 'rpm';
	par[6].index		:= 16#2042;
	par[6].subindex		:= 16#48;	
	par[6].type			:= DATATYPE_UINT;	
	par[6].access		:= ACCESS_RWS;
	par[6].visible		:= TRUE;
	
	
	par[10].name		:= 'Tuning Result: Stator Resistance';
	par[10].code		:= 'RSA';
	par[10].unit		:= 'Ohm';
	par[10].index		:= 16#2042;
	par[10].subindex	:= 16#2B;	
	par[10].type		:= DATATYPE_UINT;	
	par[10].access		:= ACCESS_RWS;
	par[10].visible		:= TRUE;

	par[11].name		:= 'Tuning Result: Magnetizing Current';
	par[11].code		:= 'IDW';
	par[11].unit		:= '0.1 A';
	par[11].index		:= 16#2042;
	par[11].subindex	:= 16#35;	
	par[11].type		:= DATATYPE_UINT;	
	par[11].access		:= ACCESS_RWS;
	par[11].visible		:= TRUE;

	par[12].name		:= 'Tuning Result: Leakage Inductance';
	par[12].code		:= 'LFA';
	par[12].unit		:= '0.01 mH';
	par[12].index		:= 16#2042;
	par[12].subindex	:= 16#3F;	
	par[12].type		:= DATATYPE_UINT;	
	par[12].access		:= ACCESS_RWS;
	par[12].visible		:= TRUE;

	par[13].name		:= 'Tuning Result: Rotor Time Constant';
	par[13].code		:= 'TRA';
	par[13].unit		:= '1 ms';
	par[13].index		:= 16#2042;
	par[13].subindex	:= 16#44;	
	par[13].type		:= DATATYPE_UINT;	
	par[13].access		:= ACCESS_RWS;
	par[13].visible		:= TRUE;
	
	
	
	par[20].name		:= 'Tuning: Speed Proportional Gain';
	par[20].code		:= 'SPG';
	par[20].unit		:= '%';
	par[20].index		:= 16#203D;
	par[20].subindex	:= 16#04;	
	par[20].type		:= DATATYPE_UINT;	
	par[20].access		:= ACCESS_RW;
	par[20].visible		:= TRUE;

	par[21].name		:= 'Tuning: Speed Time Integral';
	par[21].code		:= 'SIT';
	par[21].unit		:= '%';
	par[21].index		:= 16#203D;
	par[21].subindex	:= 16#05;	
	par[21].type		:= DATATYPE_UINT;	
	par[21].access		:= ACCESS_RW;
	par[21].visible		:= TRUE;

	par[22].name		:= 'Tuning: K Speed Loop Filter';
	par[22].code		:= 'SFC';
	par[22].unit		:= '1';
	par[22].index		:= 16#203D;
	par[22].subindex	:= 16#06;	
	par[22].type		:= DATATYPE_UINT;	
	par[22].access		:= ACCESS_RW;
	par[22].visible		:= TRUE;

	par[23].name		:= 'Tuning: Drive Switching Freq';
	par[23].code		:= 'SFR';
	par[23].unit		:= '0.1 kHz';
	par[23].index		:= 16#2001;
	par[23].subindex	:= 16#03;	
	par[23].type		:= DATATYPE_UINT;	
	par[23].access		:= ACCESS_RW;
	par[23].visible		:= TRUE;
	
	
	
	par[30].name		:= 'Accleration (P84 ONLY)';
	par[30].code		:= 'ACC';
	par[30].unit		:= '0.1 sec';
	par[30].index		:= 16#203C;
	par[30].subindex	:= 16#02;	
	par[30].type		:= DATATYPE_UINT;	
	par[30].access		:= ACCESS_RW;
	par[30].visible		:= TRUE;

	par[31].name		:= 'Deceleration (P84 ONLY)';
	par[31].code		:= 'DEC';
	par[31].unit		:= '0.1 sec';
	par[31].index		:= 16#203C;
	par[31].subindex	:= 16#03;	
	par[31].type		:= DATATYPE_UINT;	
	par[31].access		:= ACCESS_RW;
	par[31].visible		:= TRUE;

	par[32].name		:= 'Acceleration 2 (P84 ONLY)';
	par[32].code		:= 'AC2';
	par[32].unit		:= '0.1 sec';
	par[32].index		:= 16#203C;
	par[32].subindex	:= 16#0D;	
	par[32].type		:= DATATYPE_UINT;	
	par[32].access		:= ACCESS_RW;
	par[32].visible		:= TRUE;

	par[33].name		:= 'Deceleration 2 (P84 ONLY)';
	par[33].code		:= 'DE2';
	par[33].unit		:= '0.1 sec';
	par[33].index		:= 16#203C;
	par[33].subindex	:= 16#0E;	
	par[33].type		:= DATATYPE_UINT;	
	par[33].access		:= ACCESS_RW;
	par[33].visible		:= TRUE;
	
	
	
	par[40].name		:= 'Acceleration delta speed';
	par[40].code		:= 'SPAL';
	par[40].unit		:= 'RPM';
	par[40].index		:= 16#6048;
	par[40].subindex	:= 16#01;	
	par[40].type		:= DATATYPE_UINT;	
	par[40].access		:= ACCESS_RW;
	par[40].visible		:= TRUE;

	par[41].name		:= 'Acceleration delta time';
	par[41].code		:= 'SPAT';
	par[41].unit		:= 'sec';
	par[41].index		:= 16#6048;
	par[41].subindex	:= 16#02;	
	par[41].type		:= DATATYPE_UINT;	
	par[41].access		:= ACCESS_RW;
	par[41].visible		:= TRUE;

	par[42].name		:= 'Deceleration delta speed';
	par[42].code		:= 'SPDL';
	par[42].unit		:= 'RPM';
	par[42].index		:= 16#6049;
	par[42].subindex	:= 16#01;	
	par[42].type		:= DATATYPE_UINT;	
	par[42].access		:= ACCESS_RW;
	par[42].visible		:= TRUE;

	par[43].name		:= 'Deceleration delta time';
	par[43].code		:= 'SPDT';
	par[43].unit		:= 'sec';
	par[43].index		:= 16#6049;
	par[43].subindex	:= 16#02;	
	par[43].type		:= DATATYPE_UINT;	
	par[43].access		:= ACCESS_RW;
	par[43].visible		:= TRUE;
	
	// prepare list for vis
	FOR i := 0 TO 99 DO
		IF par[i].index <> 0 THEN
			parStrList[i] := par[i].name;
			
			IF NOT(par[i].visible) THEN
				parListOpt[i] := 2;
			END_IF
		ELSE
			parListOpt[i] := 2;
		END_IF
	END_FOR
	
END_ACTION
