
PROGRAM _INIT
	
	InitPars;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	pPar ACCESS ADR(par[selectedPar]);
	
	CASE step OF
		0: // wait to make sure there is a valid node
			IF (gMain.mchType > MCHTYP_UNDEF_1) THEN
				EplSDORead_0.pDevice 	:= ADR('IF1');
				EplSDORead_0.node 		:= UDINT_TO_USINT(gMain.mchType);
				
				EplSDOWrite_0.pDevice 	:= ADR('IF1');
				EplSDOWrite_0.node 		:= UDINT_TO_USINT(gMain.mchType);
				
				step := 1;
			END_IF
			
		1: // wait for command to read or write
			IF read THEN
				read := FALSE;
				step := 20;
			ELSIF parChanged THEN
				parChanged := FALSE;
				step := 20;
			ELSIF write THEN
				write := FALSE;
				IF (par[selectedPar].access = ACCESS_RW) OR (par[selectedPar].access = ACCESS_RWS) THEN
					step := 30;
				ELSE
					gAlarmInfo.EdgeAlarmFlags.WriteParFailed	:= TRUE;
					step := 99;
				END_IF
			ELSIF (gMotorSts.autotuneState <> TUNE_DONE) THEN
				step := 40;
			END_IF
			
		20: // Set READ pars
			EplSDORead_0.index		:= par[selectedPar].index;
			EplSDORead_0.subindex	:= par[selectedPar].subindex;
			
			CASE par[selectedPar].type OF
				DATATYPE_INT:
					EplSDORead_0.pData		:= ADR(data.valueINT);
					EplSDORead_0.datalen	:= 2;
				DATATYPE_UINT:
					EplSDORead_0.pData		:= ADR(data.valueUINT);
					EplSDORead_0.datalen	:= 2;
				DATATYPE_WORD:
					EplSDORead_0.pData		:= ADR(data.valueWORD);
					EplSDORead_0.datalen	:= 2;
			END_CASE
			
			EplSDORead_0.enable	:= TRUE;
			
			step := 21;
			
		21:
			IF EplSDORead_0.status = ERR_OK THEN
				EplSDORead_0.enable	:= FALSE;
				step := 1;
				
				CASE par[selectedPar].type OF
					DATATYPE_INT: parValueStr := INT_TO_STRING(data.valueINT);
					DATATYPE_UINT: parValueStr := UINT_TO_STRING(data.valueUINT);
					DATATYPE_WORD: parValueStr := WORD_TO_STRING(data.valueWORD);
				END_CASE
				
			ELSIF EplSDORead_0.status <> ERR_FUB_ENABLE_FALSE AND EplSDORead_0.status <> ERR_FUB_BUSY THEN
				status := EplSDORead_0.status;
				errorinfo := EplSDORead_0.errorinfo;
				EplSDORead_0.enable	:= FALSE;
				gAlarmInfo.EdgeAlarmFlags.WriteParFailed	:= TRUE;
				step := 99;
			END_IF				
		
		30: // Set WRITE pars
			EplSDOWrite_0.index		:= par[selectedPar].index;
			EplSDOWrite_0.subindex	:= par[selectedPar].subindex;
			
			CASE par[selectedPar].type OF
				DATATYPE_INT:
					data.valueINT := STRING_TO_INT(parValueStr);
					
					EplSDOWrite_0.pData		:= ADR(data.valueINT);
					EplSDOWrite_0.datalen	:= 2;
				DATATYPE_UINT:
					data.valueUINT := STRING_TO_UINT(parValueStr);
					
					EplSDOWrite_0.pData		:= ADR(data.valueUINT);
					EplSDOWrite_0.datalen	:= 2;
				DATATYPE_WORD:
					data.valueWORD := STRING_TO_WORD(parValueStr);
					
					EplSDOWrite_0.pData		:= ADR(data.valueWORD);
					EplSDOWrite_0.datalen	:= 2;
			END_CASE
			
			EplSDOWrite_0.enable	:= TRUE;
			
			step := 31;
			
		31:
			IF EplSDOWrite_0.status = ERR_OK THEN
				EplSDOWrite_0.enable	:= FALSE;
				gAlarmInfo.EdgeAlarmFlags.WriteParSuccess	:= TRUE;
				step := 1;
				read := TRUE; // always follow up with a read
			ELSIF EplSDOWrite_0.status <> ERR_FUB_ENABLE_FALSE AND EplSDOWrite_0.status <> ERR_FUB_BUSY THEN
				status := EplSDOWrite_0.status;
				EplSDOWrite_0.enable	:= FALSE;
				gAlarmInfo.EdgeAlarmFlags.WriteParFailed	:= TRUE;
				step := 99;
			END_IF	
			
		40: // Read the autotuning state
			EplSDORead_0.index		:= 16#2042;
			EplSDORead_0.subindex	:= 16#A;
			EplSDORead_0.pData		:= ADR(tempAutotuneState);
			EplSDORead_0.datalen	:= SIZEOF(tempAutotuneState);

			EplSDORead_0.enable	:= TRUE;
			
			step := 41;
			
		41:
			IF EplSDORead_0.status = ERR_OK THEN
				EplSDORead_0.enable	:= FALSE;
				gMotorSts.autotuneState := WORD_TO_DINT(tempAutotuneState);
				
				step := 1;			
				
			ELSIF EplSDORead_0.status <> ERR_FUB_ENABLE_FALSE AND EplSDORead_0.status <> ERR_FUB_BUSY THEN
				status := EplSDORead_0.status;
				EplSDORead_0.enable	:= FALSE;
				gAlarmInfo.EdgeAlarmFlags.WriteParFailed := TRUE;
				step := 99;
			END_IF				
			
		99: // some error handling
			IF gMain.cmd.reset THEN
				step := 1;
			END_IF
			
	END_CASE
	
	EplSDORead_0();
	EplSDOWrite_0();
	
	// autotuning check
	IF EDGEPOS(gMotorSts.autotuneState = TUNE_IN_PRG) THEN
		showTuningMsg := TRUE;
	ELSIF EDGEPOS(gMotorSts.autotuneState = TUNE_DONE) OR gMain.cmd.reset THEN
		showTuningMsg := FALSE;
	END_IF	
	gAlarmInfo.PersistentAlarmFlags.AutotuneActive := showTuningMsg;
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

