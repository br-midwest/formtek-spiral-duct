
TYPE
	drive_par_typ : 	STRUCT 
		name : STRING[80];
		code : STRING[10];
		unit : STRING[10];
		type : drive_datatype_enum;
		subindex : USINT;
		index : UINT;
		visible : BOOL;
		access : drive_access_enum;
	END_STRUCT;
	drive_value_typ : 	STRUCT 
		valueWORD : WORD;
		valueINT : INT;
		valueUINT : UINT;
	END_STRUCT;
	drive_access_enum : 
		(
		ACCESS_R := 0,
		ACCESS_RW := 1,
		ACCESS_RWS := 2
		);
	drive_datatype_enum : 
		(
		DATATYPE_WORD := 0,
		DATATYPE_INT := 1,
		DATATYPE_UINT := 2
		);
END_TYPE
