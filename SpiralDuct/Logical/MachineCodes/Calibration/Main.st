(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Author:		Brad Jones
 * Created:		2018/10:18 
*********************************************************
[PROGRAM INFORMATION]
Create a relationship between mm of tube formed and encoder count.
The change in mm (CalcMultiplier) is proportional TO the change in encoder count
Initial encoder count: gCal.latchedSnsCnts
Resulting encoder count: RawEncoderCount 
********************************************************)
PROGRAM _INIT

	IF gCal.calPartCnts = 0.0 THEN
		gCal.calPartCnts := 101000.0;
	END_IF
	
	IF gCal.calPartLng = 0.0 THEN
		gCal.calPartLng := 1651.0; // 65 in
	END_IF
	
	IF gCal.latchedSnsCnts = 0.0 THEN
		gCal.latchedSnsCnts := 100000.0;
	END_IF
	
END_PROGRAM
      
PROGRAM _CYCLIC

	// if a cut (and therefore an encoder zeroing) does not take place after a wr, encoderValue and gPart.CntsThisPart
	// will be different (encoder card is zeroed on wr, gPart.CntsThisPart remembers its value). Account for this shift.
	IF ABS(gIO.enc.Value - gPart.CntsThisPart) > 250 THEN
		wrEncOffset	:= ABS(gIO.enc.Value - gPart.CntsThisPart);
	ELSE
		wrEncOffset	:= 0;
	END_IF
	
	
	(****************************************************Calibration controls***************************************************************)
	// check for errors or stops while calibrating
	IF (gAlarmInfo.rxnControlOff OR gAlarmInfo.rxnStopMachine OR gMain.cmd.stop OR gMain.cmd.reqAbortCal) AND (step > CAL_IDLE) THEN
		gMain.cmd.reqAbortCal := FALSE;
		step := CAL_IDLE;
		gAlarmInfo.EdgeAlarmFlags.CalibrationAborted := TRUE;
	END_IF	
	
	(* In manual mode, calibration is signaled by an HMI button on the main screen *)
	(* To calibrate, push Production On pushbutton *)
	(* Calibration is finished when the measured length of the tube is entered into the HMI*)
	CASE step OF
		CAL_INIT:
			// from the measured calibration piece, back calculate the distance from the knife to the photoeye
			gCal.calcSnsDist := (gCal.latchedSnsCnts / gCal.calPartCnts) * gCal.calPartLng;
			MM_TO_ENCCNT	:= LREAL_TO_REAL(gCal.latchedSnsCnts / gCal.calcSnsDist);
			step := CAL_IDLE;
			
		CAL_IDLE:
			IF gMain.cmd.reqCal THEN
				gMain.cmd.reqCal := FALSE;
				gAlarmInfo.EdgeAlarmFlags.CalInstructions := TRUE;
				step := CAL_WAIT_START;
			END_IF
			
		CAL_WAIT_START:
			// waiting for the operator to push the prod on pb or to cancel
			IF EDGEPOS(gIO.di.ProdOnPressed) THEN
				step := CAL_PRODUCING;
				gCal.calPartLng := 0.0;
				staleLatch := TRUE;
			END_IF
			
		CAL_PRODUCING:
			IF gIO.di.LngSns THEN
				step := CAL_CHECK_DATA;
			END_IF
	
		CAL_CHECK_DATA:
			IF TON_StaleLatch.Q THEN
				gAlarmInfo.EdgeAlarmFlags.CalLatchFailed := TRUE;
			ELSE
				step := CAL_WAIT_INPUT;
			END_IF
			
		CAL_WAIT_INPUT:
			IF gCal.calPartLng <> 0.0 THEN
				gCal.calPartCnts := DINT_TO_LREAL(gPart.CntsThisPart);
				step := CAL_INIT;
			END_IF

	END_CASE
	
	IF MM_TO_ENCCNT <> 0 THEN
		ENCCNT_TO_MM	:= 1.0 / MM_TO_ENCCNT;
	END_IF	
	
	(**************************************************************************************************************)	

	gMain.sts.calInPrg	:= (step <> CAL_IDLE) AND (step <> CAL_ABORT);
	gMain.cmd.runCal	:= (step = CAL_PRODUCING);

	// keep a short log of previously latched encoder values, always use the most recent one
	gIO.enc.LatchEnable	:= (((gMain.step = MAIN_PRODUCING) OR (gMain.step = MAIN_FINISH_PROD)) AND (gPart.LngProduced > MIN_SNS_DIST_MM)) OR gMain.sts.calInPrg;

	IF EDGEPOS(gIO.enc.LatchCount > 0) THEN
		staleLatch := FALSE;
		
		FOR i := 9 TO 1 BY -1 DO
			gCal.EncLatchValBfr[i]	:= gCal.EncLatchValBfr[i-1];
		END_FOR
		gCal.EncLatchValBfr[0]	:= gIO.enc.LatchValue + wrEncOffset;
		
		// update scaling based on latest latch
		// V6.00 IMPORTANT: The calculated duct length is now heavily dependent on sensor position
		// If the sensor moves after calibration, the part will be skewed
		gCal.latchedSnsCnts 		:= DINT_TO_REAL(gIO.enc.LatchValue + wrEncOffset);
		MM_TO_ENCCNT			:= LREAL_TO_REAL(gCal.latchedSnsCnts / gCal.calcSnsDist);
	END_IF	

	TON_StaleLatch.IN := (step = CAL_CHECK_DATA) AND staleLatch;
	TON_StaleLatch.PT := T#1s;
	TON_StaleLatch();
	
END_PROGRAM
