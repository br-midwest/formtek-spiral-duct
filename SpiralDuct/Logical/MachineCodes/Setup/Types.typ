
TYPE
	setup_typ : 	STRUCT 
		stripThickness : REAL; (*Thickness of the raw stock material in mm*)
		FRUno : STRING[16]; (*Form Roll Unit number*)
		duty : STRING[16];
		foldingFinger : STRING[16];
		flangeRoller : STRING[16];
		stripWidth : REAL;
		flangeHt : REAL; (*Flange height in mm*)
		inlayBtwNo : STRING[16]; (*Inlay number for between guide plates*)
		inlayUnderNo : STRING[16]; (*Inlay number for under guide plates*)
		knifeGap : minmax_typ; (*Knife gap in mm*)
		prs : pressures_typ; (*Information about recommended pressures*)
		material : stockmaterial_enum;
	END_STRUCT;
	pressures_typ : 	STRUCT 
		clinchRoll : minmax_typ; (*Pressure information for clinch roller*)
		driveRoll : minmax_typ; (*Pressure information for drive roller*)
	END_STRUCT;
	stockmaterial_enum : 
		(
		MATERIAL_GALV,
		MATERIAL_SS,
		MATERIAL_ALUM
		);
	minmax_typ : 	STRUCT 
		max : REAL; (*maximum allowed value*)
		min : REAL; (*minimum allowed value*)
	END_STRUCT;
END_TYPE
