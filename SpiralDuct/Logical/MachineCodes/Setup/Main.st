
PROGRAM _CYCLIC

	(***** Determine roll bar pressures based on strip thickness and machine type *****)
	IF (gMain.par.stripThickness <> oldStripThickness) OR (Setup.material <> oldMaterial) THEN
		oldStripThickness := gMain.par.stripThickness;
		oldMaterial := Setup.material;
	
		// Set form roll unit number, inlay numbers and pressures based on strip thickness and material
		IF gMain.par.stripThickness >= 1.654 AND gMain.par.stripThickness < 2.000 THEN
			Setup.FRUno 		:= '6';
			Setup.flangeHt		:= 9.5;
			Setup.inlayBtwNo	:= '4+2';
			Setup.inlayUnderNo	:= 'None';
			Setup.knifeGap.min	:= 0.3048;
			Setup.knifeGap.max	:= 0.3302;
			Setup.duty			:= 'Heavy';
			Setup.foldingFinger	:= 'No';
			Setup.flangeRoller	:= 'No';
			Setup.stripWidth	:= 150.0;
			
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 60;
					Setup.prs.driveRoll.max 	:= 75;
			
					Setup.prs.clinchRoll.min 	:= 60;
					Setup.prs.clinchRoll.max 	:= 75;

				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		ELSIF gMain.par.stripThickness >= 1.313 AND gMain.par.stripThickness < 1.654 THEN
			Setup.FRUno 		:= '5';
			Setup.flangeHt		:= 8.5;
			Setup.inlayBtwNo	:= '5';
			Setup.inlayUnderNo	:= 'None';
			Setup.knifeGap.min	:= 0.2540;
			Setup.knifeGap.max	:= 0.2794;
			Setup.duty			:= 'Heavy';
			Setup.foldingFinger	:= 'No';
			Setup.flangeRoller	:= 'No';
			Setup.stripWidth	:= 142.88;
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 55;
					Setup.prs.driveRoll.max 	:= 65;
		
					Setup.prs.clinchRoll.min 	:= 50;
					Setup.prs.clinchRoll.max 	:= 60;
		
				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		ELSIF gMain.par.stripThickness >= 1.016 AND gMain.par.stripThickness < 1.313 THEN
			Setup.FRUno 		:= '4';
			Setup.flangeHt		:= 7.7;
			Setup.inlayBtwNo	:= '4';
			Setup.inlayUnderNo	:= '0';
			Setup.knifeGap.min	:= 0.2032;
			Setup.knifeGap.max	:= 0.2286;
			Setup.duty			:= 'Heavy';
			Setup.foldingFinger	:= 'Yes';
			Setup.flangeRoller	:= 'Yes';
			Setup.stripWidth	:= 139.7;
			
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 40;
					Setup.prs.driveRoll.max 	:= 55;
			
					Setup.prs.clinchRoll.min 	:= 35;
					Setup.prs.clinchRoll.max 	:= 50;
					
				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		ELSIF gMain.par.stripThickness >= 0.815 AND gMain.par.stripThickness < 1.016 THEN
			Setup.FRUno 		:= '3';
			Setup.flangeHt		:= 6.5;
			Setup.inlayBtwNo	:= '3';
			Setup.inlayUnderNo	:= '1';
			Setup.knifeGap.min	:= 0.1549;
			Setup.knifeGap.max	:= 0.1778;
			Setup.duty			:= 'Standard';
			Setup.foldingFinger	:= 'Yes';
			Setup.flangeRoller	:= 'Yes';
			Setup.stripWidth	:= 137.0;
			
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 30;
					Setup.prs.driveRoll.max 	:= 40;
			
					Setup.prs.clinchRoll.min 	:= 25;
					Setup.prs.clinchRoll.max 	:= 35;
					
				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		ELSIF gMain.par.stripThickness >= 0.554 AND gMain.par.stripThickness < 0.815 THEN
			Setup.FRUno			:= '2';
			Setup.flangeHt		:= 6.3;
			Setup.inlayBtwNo	:= '2';
			Setup.inlayUnderNo	:= '2';
			Setup.knifeGap.min	:= 0.1041;
			Setup.knifeGap.max	:= 0.1470;
			Setup.duty			:= 'Standard';
			Setup.foldingFinger	:= 'Yes';
			Setup.flangeRoller	:= 'Yes';
			Setup.stripWidth	:= 137.0;
			
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 25;
					Setup.prs.driveRoll.max 	:= 35;
			
					Setup.prs.clinchRoll.min 	:= 20;
					Setup.prs.clinchRoll.max 	:= 30;
					
				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		ELSIF gMain.par.stripThickness >= 0.380 AND gMain.par.stripThickness < 0.554 THEN
			Setup.FRUno			:= '1a';
			Setup.flangeHt		:= 6.3;
			Setup.inlayBtwNo	:= '1';
			Setup.inlayUnderNo	:= '3';
			Setup.knifeGap.min	:= 0.0762;
			Setup.knifeGap.max	:= 0.1016;
			Setup.duty			:= 'Standard';
			Setup.foldingFinger	:= 'Yes';
			Setup.flangeRoller	:= 'Yes';
			Setup.stripWidth	:= 137.0;
			
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 20;
					Setup.prs.driveRoll.max 	:= 30;

					Setup.prs.clinchRoll.min 	:= 15;
					Setup.prs.clinchRoll.max 	:= 25;
				
				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		ELSE
			Setup.FRUno			:= 'n/a';
			Setup.flangeHt		:= 0.0;
			Setup.inlayBtwNo	:= 'n/a';
			Setup.inlayUnderNo	:= 'n/a';
			Setup.knifeGap.min	:= 0.0;
			Setup.knifeGap.max	:= 0.0;
			Setup.duty			:= 'n/a';
			Setup.foldingFinger	:= 'n/a';
			Setup.flangeRoller	:= 'n/a';
			Setup.stripWidth	:= 0.0;
			
			
			CASE Setup.material OF
				MATERIAL_GALV:
					Setup.prs.driveRoll.min 	:= 0;
					Setup.prs.driveRoll.max 	:= 0;
			
					Setup.prs.clinchRoll.min 	:= 0;
					Setup.prs.clinchRoll.max 	:= 0;
				
				MATERIAL_ALUM:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
					
				MATERIAL_SS:
					Setup.prs.driveRoll.min := 0;
					Setup.prs.driveRoll.max := 0;
			
					Setup.prs.clinchRoll.min := 0;
					Setup.prs.clinchRoll.max := 0;
				
			END_CASE
			
		END_IF
	END_IF
	
	// check if gauge changed, then update the speed via case below
	IF gMain.par.materialGauge <> prevMaterialGaugeSelection THEN
		prevMaterialGaugeSelection := gMain.par.materialGauge;
		update := TRUE;
	END_IF
	
	// set limit speed and acceleration based on which machine type is active
	IF  gMain.mchType > 0 AND ( NOT(setFlag)  OR update ) THEN
		setFlag := TRUE; // only run this logic once per boot
		update := FALSE;
		
		CASE gMain.mchType OF
			// test setups
			MCHTYP_P66_TEST:
				gMain.sts.topSpeed := 1370;	
				gMain.sts.ACPiModel := brusACPi_P66;
				modelStr := 'P66 Test';
			MCHTYP_P86_TEST:
				gMain.sts.topSpeed:= 1370;
				gMain.sts.ACPiModel := brusACPi_P86;
				modelStr := 'P86 Test';
			MCHTYP_P84_TEST:
				gMain.sts.topSpeed:= 1370;
				gMain.sts.ACPiModel := brusACPi_P84;
				modelStr := 'P84 Test';
			MCHTYP_P74_TEST:
				gMain.sts.topSpeed:= 1370;
				gMain.sts.ACPiModel := brusACPi_P74;
				modelStr := 'P74 Test';
				
			// "old generation"
			MCHTYP_12E_230V_P84:
				gMain.sts.topSpeed:= 2640; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P84;
				modelStr := 'DMAX 1.2E 230V (P84)';
			MCHTYP_12E_460V_P84:
				gMain.sts.topSpeed:= 2640; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P84;
				modelStr := 'DMAX 1.2E 460V (P84)';
			MCHTYP_20E_460V_P84:
				CASE gMain.par.materialGauge OF
					GAUGE_HEAVY:
						gMain.sts.topSpeed:= 2720; // nameplate speed + 50%
					GAUGE_LIGHT:
						gMain.sts.topSpeed:= 3260; // nameplate speed + 50% + 15% for lighter gauge
				END_CASE
				gMain.sts.ACPiModel := brusACPi_P84;
				modelStr := 'DMAX 2.0E 460V (P84)';
			MCHTYP_16E_460V_P84:
				gMain.sts.topSpeed:= 2640; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P84;
				modelStr := 'DMAX 1.6E 460V (P84)';
			MCHTYP_20E_400V_P84:
				gMain.sts.topSpeed:= 2720; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P84;
				modelStr := 'DMAX 2.0E 400V (P84)';
			
			// "new generation"
			MCHTYP_12E_230V_P66:
				gMain.sts.topSpeed:= 2640; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P66;
				modelStr := 'DMAX 1.2E 230V (P66)';
			MCHTYP_12E_460V_P66:
				gMain.sts.topSpeed:= 2640; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P66;
				modelStr := 'DMAX 1.2E 460V (P66)';
			MCHTYP_20E_460V_P86:
				CASE gMain.par.materialGauge OF
					GAUGE_HEAVY:
						gMain.sts.topSpeed:= 2720; // nameplate speed + 50%
					GAUGE_LIGHT:
						gMain.sts.topSpeed:= 3260; // nameplate speed + 50% + 15% for lighter gauge
				END_CASE
				gMain.sts.ACPiModel := brusACPi_P86;
				modelStr := 'DMAX 2.0E 460V (P86)';
			MCHTYP_16E_460V_P86:
				gMain.sts.topSpeed:= 2640; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P86;
				modelStr := 'DMAX 1.6E 460V (P86)';
			MCHTYP_20E_400V_P86:
				gMain.sts.topSpeed:= 2720; // nameplate speed + 50%
				gMain.sts.ACPiModel := brusACPi_P86;
				modelStr := 'DMAX 2.0E 400V (P86)';
		END_CASE
		
		
		// if the node has changed, then set the update notification
		IF gMain.mchType <> prevNode THEN
			prevNode := gMain.mchType;
			gAlarmInfo.EdgeAlarmFlags.NodeUpdateNotification := TRUE;
		END_IF
	END_IF
	
END_PROGRAM