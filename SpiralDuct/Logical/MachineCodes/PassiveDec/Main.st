(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Author:		Brad Jones
 * Created:		2018/14:07 
*********************************************************
[PROGRAM INFORMATION]
Autoamatically identifies the relationship between potentiometer
set speed and counts required to decelerate

********************************************************)

PROGRAM _INIT
	
	z := 1;
	ti := 1;
	
	FOR i := 1 TO MAX_DATA_POINTS DO
		overridePts[i] := ((INT_TO_REAL(i) / INT_TO_REAL(MAX_DATA_POINTS)) * 100.0) - (1.0 / INT_TO_REAL(MAX_DATA_POINTS) * 50.0);
	END_FOR
	
	MTLookUpTable_0.Mode := mtLOOKUP_LINEAR_EXTRAPOLATION;
	MTLookUpTable_0.NumberOfNodes := MAX_DATA_POINTS + 1;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	vfdio ACCESS ADR(gVFDio[gMain.mchType]);
	
	edgeposVelocityReached := EDGEPOS(vfdio.DS402.SetVelocityReached);
	edgenegVelocityReached := EDGENEG(vfdio.DS402.SetVelocityReached);
	edgenegMoveVelocity := EDGENEG(vfdio.DS402.MoveVelocity);
	
	IF EDGEPOS(gMain.cmd.stop) THEN
		step := DEC_IDLE;
	END_IF
	
	// state machine for calibrating the decel for the machine
	CASE step OF
		DEC_IDLE: // wait until	
			IF edgeposVelocityReached THEN
				step := DEC_WAIT_FOR_HALT;
			END_IF
			
		DEC_WAIT_FOR_HALT: // if the move velocity command goes low, a normal dec ramp is initiated
			IF NOT(vfdio.DS402.SetVelocityReached) THEN
				step := DEC_IDLE;
			ELSIF edgenegMoveVelocity THEN
				latchedSpeed := vfdio.actVelocity;
				startDecCnts := gPart.CntsThisPart;
				step := DEC_WAIT_FOR_STANDSTILL;
			END_IF
			
		DEC_WAIT_FOR_STANDSTILL:
			IF NOT(vfdio.DS402.InMotion) THEN
				stopDecCnts := gPart.CntsThisPart;
				step := DEC_DATA_CHECKOUT;
			END_IF
			
		DEC_DATA_CHECKOUT:
			IF startDecCnts > stopDecCnts THEN
				step := DEC_IDLE; // data is bad, start over
			ELSE
				// put the data in the relevant slot
				z := LIMIT(1, REAL_TO_INT((INT_TO_REAL(latchedSpeed) / gMain.sts.topSpeed) * INT_TO_REAL(MAX_DATA_POINTS)) + 1, MAX_DATA_POINTS);
				
				data.decelCnts[z] := DINT_TO_REAL(stopDecCnts - startDecCnts);
				data.speed[z] := (latchedSpeed);
				
				// check to make sure the data is monotonic ascending
				FOR i := 1 TO MAX_DATA_POINTS DO
					IF (i <> z) AND data.decelCnts[i] = 0.0 THEN
						data.decelCnts[i] := data.decelCnts[z] * (INT_TO_REAL(i) / INT_TO_REAL(z));
					END_IF
					IF (i <> z) AND data.speed[i] = 0.0 THEN
						data.speed[i] := data.speed[z] * (INT_TO_REAL(i) / INT_TO_REAL(z));
					END_IF
				END_FOR
				
				MTLookUpTable_0.NodeVectorX := data.speed;
				MTLookUpTable_0.FcnValues := data.decelCnts;
				
				MTLookUpTable_0.Update := TRUE;
				step := DEC_IDLE;
			END_IF
	END_CASE
	
	// ============================= CALCULATING DUCT DECELERATION ================================
	// Adjust the y intercept to change when slow down begins. higher value means less slow time. 
	// The slope just defines how decel changes between speeds and should be left alone.
	// Currently, these decelerations are calculated with regressions that were made in Excel with
	// a few data point which were taken from traces. X value is set speed in motor rpm, Y value is 
	// decel in counts.
	MTLookUpTable_0.Enable := NOT(gMain.sts.calInPrg) AND NOT(gMain.cmd.reset);
	
	MTLookUpTable_0.InX := gMain.sts.reqSpd.cut;
	MTLookUpTable_0();
	gPart.CutDecelCnts := MAX(100.0, MTLookUpTable_0.Out);
	
	MTLookUpTable_0.InX := gMain.sts.reqSpd.prod;
	MTLookUpTable_0();
	gPart.ProdDecelCnts := MAX(100.0, MTLookUpTable_0.Out);
	
	MTLookUpTable_0.InX := gMain.sts.reqSpd.fine;
	MTLookUpTable_0();
	gPart.FineDecelCnts := MAX(100.0, MTLookUpTable_0.Out);
	
	IF MTLookUpTable_0.UpdateDone THEN
		MTLookUpTable_0.Update := FALSE;
	END_IF
	// ============================================================================================
	
	TeachSequence;
	
END_PROGRAM
