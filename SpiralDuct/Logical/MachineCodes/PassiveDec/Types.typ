
TYPE
	decel_data_typ : 	STRUCT 
		decelCnts : ARRAY[0..49]OF REAL;
		speed : ARRAY[0..49]OF REAL;
	END_STRUCT;
	decel_id_step_enum : 
		(
		DEC_IDLE := 0,
		DEC_WAIT_FOR_HALT := 1,
		DEC_WAIT_BEGIN_DEC := 2,
		DEC_WAIT_FOR_STANDSTILL := 3,
		DEC_DATA_CHECKOUT := 4,
		DEC_CALC := 5
		);
	teachStep_enum : 
		(
		TEACH_IDLE := 0,
		TEACH_ACC := 10,
		TEACH_HALT := 20,
		TEACH_W_BRAKE_OFF := 21,
		TEACH_EVAL := 30,
		TEACH_ABORTED := 99,
		NOT_ABORTED := 100
		);
END_TYPE
