
ACTION TeachSequence: 
	
	// make sure the sequence aborts properly
	IF EDGEPOS(gMain.cmd.stop) OR gAlarmInfo.rxnStopMachine THEN
		AbortTeachSeq	:= TRUE;
	END_IF
	
	// state machine for calibrating the decel for the machine
	CASE teachStep OF
		TEACH_IDLE:
			gMain.sts.overrideSpeedRef	:= 0;
			
			// wait for appropriate flag to begin dec identification
			IF gMain.sts.calInPrg AND gIO.di.ProdOnPressed AND gMain.par.useTeachSeqInCal THEN
				teachStep := TEACH_ACC;
				ti := 1;
				AbortTeachSeq := FALSE;
				teachStatusStr := 'Identifying deceleration...';
			END_IF
				
		TEACH_ACC:
			gMain.sts.overrideSpeedRef	:= overridePts[ti];
			
			// if acc==0 but vel<>0, machine is at speed, initiate stop
			IF step = DEC_WAIT_FOR_HALT THEN
				teachStep := TEACH_HALT;
			ELSIF TON_TOUT.Q OR AbortTeachSeq THEN
				abortedFrom	:= teachStep;
				teachStep := TEACH_ABORTED;
				teachStatusStr := 'Timed out in acceleration phase!';
			END_IF	
			
		TEACH_HALT:			
			// if acc==0 but vel<>0, machine is at speed, initiate stop
			IF step = DEC_DATA_CHECKOUT THEN
				teachStep := TEACH_W_BRAKE_OFF;
			ELSIF TON_TOUT.Q OR AbortTeachSeq THEN
				abortedFrom	:= teachStep;
				teachStep := TEACH_ABORTED;
				teachStatusStr := 'Timed out in speed identification';
			END_IF
			
		TEACH_W_BRAKE_OFF:
			// wait for the brake to release before continuing
			IF NOT(gIO.dout.DecoilerBrakeGoOn) THEN
				teachStep := TEACH_EVAL;
			ELSIF TON_TOUT.Q OR AbortTeachSeq THEN
				abortedFrom	:= teachStep;
				teachStep := TEACH_ABORTED;
				teachStatusStr := 'Timed out waiting for brake off';
			END_IF
			
		TEACH_EVAL:
			IF (step = DEC_IDLE) THEN
				IF ti < MAX_DATA_POINTS THEN
					ti	:= ti + 1;
					teachStep	:= TEACH_ACC;
				ELSE
					ti := 0;
					teachStep := TEACH_IDLE;
				END_IF
			ELSIF TON_TOUT.Q OR AbortTeachSeq THEN
				abortedFrom	:= teachStep;
				teachStep := TEACH_ABORTED;
				teachStatusStr := 'Timed out in cut speed evaluation!';
			END_IF
		
		TEACH_ABORTED:
			teachStep := TEACH_IDLE;
			AbortTeachSeq := FALSE;
		
	END_CASE
	
	// no step should take more than 5 seconds to execute on its own. If it does, abort the teach procedure
	teachTOTime := T#5s + REAL_TO_TIME(gMain.par.brakeOnTime * MS_TO_SEC);
	TON_TOUT(IN := (teachStep = oldStep) AND (teachStep <> TEACH_IDLE) AND (teachStep <> TEACH_ABORTED), PT := teachTOTime);
	oldStep	:= teachStep;
	
	// determine when to override each potentiometer
	gMain.sts.overrideInPrg := gMain.sts.controlOn AND NOT(gAlarmInfo.rxnStopMachine) AND (teachStep > TEACH_IDLE);
	gMain.cmd.runOverride	:= (teachStep = TEACH_ACC); 
	
END_ACTION
