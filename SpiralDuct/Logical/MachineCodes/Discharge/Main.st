
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC

		// discharge control - minimum required discharge cylinder time
	IF EDGEPOS(gMain.step = MAIN_DISCHARGE) THEN
		Discharge := TRUE;
	END_IF
	
	IF gMain.par.dischDelay < 0.5 THEN
		gMain.par.dischDelay := 0.5;
	END_IF
		
	TON_Discharge.IN := Discharge;
	TON_Discharge.PT := REAL_TO_TIME(gMain.par.dischDelay * 1000);
	
	IF (Discharge AND TON_Discharge.Q) OR NOT(gMain.sts.controlOn) OR ((gMain.step = MAIN_PRODUCING) AND (gMain.opMode <> OP_MODE_AUTO2)) THEN
		Discharge := FALSE;
	END_IF
	TON_Discharge();
	
	// set I/O
	gIO.dout.DischargeGoUp	:= (gMain.step = MAIN_DISCHARGE) OR Discharge;
	gIO.dout.DischargeLight	:= gIO.dout.DischargeGoUp;
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

