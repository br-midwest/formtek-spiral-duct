
PROGRAM _CYCLIC
	
	// somehow make sure that length to make is never 0?
	// to-do
	
	// index control for automatic operation
	IF gMain.sts.controlOn THEN		
		gMain.sts.partsReqInCurrSeq := gMain.par.seq[gMain.sts.seqIdx].Qty;
		
		IF gMain.sts.autoMode THEN
			// job continues through sequences
			IF (gMain.sts.cutsMadeInCurrSeq >= gMain.sts.partsReqInCurrSeq) AND (gMain.sts.partsReqInCurrSeq <> 0) THEN
				gMain.sts.cutsMadeInCurrSeq := 0;
				gMain.sts.seqIdx := gMain.sts.seqIdx + 1;
			END_IF
			
			// job has finished
			IF gMain.sts.seqIdx > gMain.par.totalSeq THEN
				gMain.sts.seqIdx := 1;
				gMain.sts.jobRunning := FALSE;
				gAlarmInfo.EdgeAlarmFlags.JobFinished := TRUE;
			END_IF
			
			// job restarted by operator
			IF (gMain.step = MAIN_READY) AND gMain.cmd.restartJob THEN
				gMain.sts.cutsMadeInCurrSeq := 0;
				gMain.sts.seqIdx := 1;
				gMain.sts.jobRunning := FALSE;
				gAlarmInfo.EdgeAlarmFlags.JobRestarted := TRUE;
			END_IF
			
		END_IF
	END_IF
	gMain.cmd.restartJob := FALSE;
	
	// set length to produce
	CASE gMain.opMode OF
		OP_MODE_AUTO1, OP_MODE_AUTO2:
			gPart.LngToMake := gMain.par.seq[gMain.sts.seqIdx].Lng;
		OP_MODE_MANUAL:
			gPart.LngToMake := gMain.par.manualLng;	
	END_CASE
	
	// convert length to make into counts
	gPart.LngToMakeCnts := gPart.LngToMake * MM_TO_ENCCNT; //Encoder Counts
	gPart.LngToMakeAtProdSpd	:= (gPart.LngToMake - gMain.par.lowSpeedFinishLng - (gPart.ProdDecelCnts * ENCCNT_TO_MM)); // set flag to slow from production speed to cut speed	
	
	gPart.LngProduced := DINT_TO_REAL(gPart.CntsThisPart) * ENCCNT_TO_MM;// calculate how much duct has been produced
	
	// check for maximum part length
	gMain.sts.partAtMaxLng := (gPart.LngProduced >= MAX_ALLOWED_PART_LNG);
	
	IF EDGEPOS(gMain.sts.partAtMaxLng) THEN
		gAlarmInfo.EdgeAlarmFlags.PartTooLong := TRUE;		
	END_IF
	
	// calculate usage
	Spiral_Usage_0.AmtProduced := gPart.LngProduced;
	Spiral_Usage_0.Width := gMain.par.materialWidth;
	Spiral_Usage_0.Diameter := gMain.par.fhDiam;
	Spiral_Usage_0.Reset := gMain.cmd.clearMaterialUsage;
	Spiral_Usage_0();
	gMain.sts.materialUsed := Spiral_Usage_0.Usage;
	
END_PROGRAM
