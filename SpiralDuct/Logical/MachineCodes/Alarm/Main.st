PROGRAM _INIT
	
	MpAlarmXCore_0.MpLink		:= ADR(mpAlarmXCore);
	MpAlarmXCore_0.Enable		:= TRUE;
	
	MpAlarmXListUI_0.MpLink		:= ADR(mpAlarmXCore);
	MpAlarmXListUI_0.UIConnect	:= ADR(alarmUIconnect);
	MpAlarmXListUI_0.UISetup.AlarmListSize	:= ALARM_LIST_SIZE;
	MpAlarmXListUI_0.Enable		:= TRUE;
	
	MpAlarmXHistory_0.MpLink	:= ADR(mpAlarmXHistory);
	MpAlarmXHistory_0.Enable	:= TRUE;
	
	MpAlarmXHistoryUI_0.MpLink	:= ADR(mpAlarmXHistory);
	MpAlarmXHistoryUI_0.UIConnect:= ADR(alarmhistUIconnect);
	MpAlarmXHistoryUI_0.UISetup.AlarmListSize	:= ALARM_LIST_SIZE;
	MpAlarmXHistoryUI_0.Enable	:= TRUE;
	
END_PROGRAM

PROGRAM _CYCLIC	

	IF MpAlarmXCore_0.Active THEN
	// ===================================================== SEVERITY 1 ALARMS ==========================================================
		// 1001 Estop
		IF gAlarmInfo.PersistentAlarmFlags.EstopActive AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'EstopActive', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'EstopActive');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.EstopActive) THEN
			MpAlarmXReset(mpAlarmXCore, 'EstopActive');		
		END_IF
		
		// 1002 No valid VFD nodes found
		IF gAlarmInfo.PersistentAlarmFlags.InvalidVFDNode AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'InvalidVFDNode', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'InvalidVFDNode');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.InvalidVFDNode) THEN
			MpAlarmXReset(mpAlarmXCore, 'InvalidVFDNode');		
		END_IF
		
		// 1003 VFD Fault
		IF gAlarmInfo.PersistentAlarmFlags.VFDfault AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'VFDfault', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'VFDfault');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.VFDfault) THEN
			MpAlarmXReset(mpAlarmXCore, 'VFDfault');		
		END_IF
		
		// 1004 Air Pressure Fault
		IF gAlarmInfo.PersistentAlarmFlags.AirPressureFault AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'AirPressureFault', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'AirPressureFault');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.AirPressureFault) THEN
			MpAlarmXReset(mpAlarmXCore, 'AirPressureFault');		
		END_IF
		
		// 1005 Pump Overload
		IF gAlarmInfo.PersistentAlarmFlags.PumpOverload AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'PumpOverload', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'PumpOverload');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.PumpOverload) THEN
			MpAlarmXReset(mpAlarmXCore, 'PumpOverload');		
		END_IF
		
		// 1006 Overtravel limit tripped
		IF gAlarmInfo.EdgeAlarmFlags.OvertravelLimit AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'OvertravelLimit', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.OvertravelLimit	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'OvertravelLimit');	
		END_IF
		
		// 1007 I/O alarm
		IF gAlarmInfo.PersistentAlarmFlags.IOAlarm AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'IOAlarm', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'IOAlarm');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.IOAlarm) THEN
			MpAlarmXReset(mpAlarmXCore, 'IOAlarm');		
		END_IF
		
		// 1008 Encoder is counting backwards
		IF gAlarmInfo.EdgeAlarmFlags.EncoderBackwards AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'EncoderBackwards', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.EncoderBackwards	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'EncoderBackwards');	
		END_IF
		
		// 1009 Communication to VFD lost!
		IF gAlarmInfo.PersistentAlarmFlags.VFDcommLost AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'VFDcommLost', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'VFDcommLost');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.VFDcommLost) THEN
			MpAlarmXReset(mpAlarmXCore, 'VFDcommLost');		
		END_IF
		
		// 1010 Encoder is not counting
		IF gAlarmInfo.EdgeAlarmFlags.EncoderStagnant AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'EncoderStagnant', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.EncoderStagnant	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'EncoderStagnant');	
		END_IF
		
		// 1011 No VFD nodes found
		IF gAlarmInfo.PersistentAlarmFlags.NoNodeDetected AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'NoNodeDetected', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'NoNodeDetected');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.NoNodeDetected) THEN
			MpAlarmXReset(mpAlarmXCore, 'NoNodeDetected');		
		END_IF
		
	// ==================================================================================================================================
	
	
	
	// ===================================================== SEVERITY 2 ALARMS ==========================================================
		// 2001 Cannot use existing part
		IF gAlarmInfo.EdgeAlarmFlags.CannotUsePart AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CannotUsePart', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CannotUsePart	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CannotUsePart');	
		END_IF
		
		// 2002 Cutting not allowed
		IF gAlarmInfo.EdgeAlarmFlags.CuttingNotAllowed AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CuttingNotAllowed', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CuttingNotAllowed	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CuttingNotAllowed');	
		END_IF
		
		// 2003 Discharge not allowed
		IF gAlarmInfo.EdgeAlarmFlags.DischargeNotAllowed AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'DischargeNotAllowed', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.DischargeNotAllowed	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'DischargeNotAllowed');	
		END_IF
		
		// 2004 Cannot Start
		IF gAlarmInfo.EdgeAlarmFlags.CannotStart AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CannotStart', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CannotStart	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CannotStart');	
		END_IF
		
		// 2005 Mode changed while running
		IF gAlarmInfo.EdgeAlarmFlags.ModeChangeWhileRunning AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'ModeChangeWhileRunning', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.ModeChangeWhileRunning	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'ModeChangeWhileRunning');	
		END_IF
		
		// 2006 Hydraulic pump is off, cannot run motor
		IF gAlarmInfo.EdgeAlarmFlags.HydraulicPumpOff AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'HydraulicPumpOff', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.HydraulicPumpOff	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'HydraulicPumpOff');	
		END_IF
		
		// 2007 Part is too long, not possible to continue running
		IF gAlarmInfo.EdgeAlarmFlags.PartTooLong AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'PartTooLong', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.PartTooLong	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'PartTooLong');	
		END_IF
		
		// 2008 New latch data not received from encoder card
		IF gAlarmInfo.EdgeAlarmFlags.CalLatchFailed AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CalLatchFailed', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CalLatchFailed	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CalLatchFailed');	
		END_IF
		
		// 2009 Machine left idle in active state
		IF gAlarmInfo.EdgeAlarmFlags.IdleInActiveState AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'IdleInActiveState', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.IdleInActiveState	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'IdleInActiveState');	
		END_IF
	
	// ==================================================================================================================================
	
	
	
	// ===================================================== SEVERITY 3 ALARMS ==========================================================
		// 3001 Failed To Reset Encoder
		IF gAlarmInfo.EdgeAlarmFlags.FailedToResetEncoder AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'FailedToResetEncoder', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.FailedToResetEncoder	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'FailedToResetEncoder');	
		END_IF
		
		// 3002 Failed To load job file
		IF gAlarmInfo.EdgeAlarmFlags.FileLoadFailed AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'FileLoadFailed', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.FileLoadFailed	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'FileLoadFailed');	
		END_IF
		
		// 3003 Failed to write par to drive
		IF gAlarmInfo.EdgeAlarmFlags.WriteParFailed AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'WriteParFailed', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.WriteParFailed	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'WriteParFailed');	
		END_IF
		
		// 3004 Parameter write successful
		IF gAlarmInfo.EdgeAlarmFlags.WriteParSuccess AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'WriteParSuccess', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.WriteParSuccess	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'WriteParSuccess');	
		END_IF
		
		// 3005 Calibration request denied
		IF gAlarmInfo.EdgeAlarmFlags.CannotCalibrate AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CannotCalibrate', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CannotCalibrate	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CannotCalibrate');	
		END_IF
		
		// 3006 Calibration aborted
		IF gAlarmInfo.EdgeAlarmFlags.CalibrationAborted AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CalibrationAborted', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CalibrationAborted	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CalibrationAborted');	
		END_IF
		
		// 3007 Calibration instructions
		IF gAlarmInfo.EdgeAlarmFlags.CalInstructions AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'CalInstructions', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.CalInstructions	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'CalInstructions');	
		END_IF
		// automatically ack the message if the operator hits production on first
		IF MpAlarmXCheckState(mpAlarmXCore, 'CalInstructions', mpALARMX_STATE_UNACKNOWLEDGED) AND EDGEPOS(gIO.di.ProdOnPressed) THEN
			MpAlarmXAcknowledge(mpAlarmXCore, 'CalInstructions');
		END_IF
		
		// 3008 Job has finished
		IF gAlarmInfo.EdgeAlarmFlags.JobFinished AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'JobFinished', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.JobFinished	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'JobFinished');	
		END_IF
		
		// 3009 Job was restarted manually
		IF gAlarmInfo.EdgeAlarmFlags.JobRestarted AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'JobRestarted', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.JobRestarted	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'JobRestarted');	
		END_IF
		
		// 3010 Job loaded successfully
		IF gAlarmInfo.EdgeAlarmFlags.FileLoadSuccess AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'FileLoadSuccess', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.FileLoadSuccess	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'FileLoadSuccess');	
		END_IF
		
		// 3011 Network error
		IF gAlarmInfo.EdgeAlarmFlags.NetworkError AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'NetworkError', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.NetworkError	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'NetworkError');	
		END_IF
		
		// 3012 No mode selected
		IF gAlarmInfo.EdgeAlarmFlags.NoModeSelected AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'NoModeSelected', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.NoModeSelected	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'NoModeSelected');	
		END_IF
		
		// 3013 Motor is currently autotuning
		IF gAlarmInfo.PersistentAlarmFlags.AutotuneActive AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'AutotuneActive', mpALARMX_STATE_ACTIVE)) THEN
			MpAlarmXSet(mpAlarmXCore, 'AutotuneActive');
		ELSIF NOT(gAlarmInfo.PersistentAlarmFlags.AutotuneActive) THEN
			MpAlarmXReset(mpAlarmXCore, 'AutotuneActive');		
		END_IF
		
		// 3014 Node update notification
		IF gAlarmInfo.EdgeAlarmFlags.NodeUpdateNotification AND NOT(MpAlarmXCheckState(mpAlarmXCore, 'NodeUpdateNotification', mpALARMX_STATE_ACTIVE)) THEN
			gAlarmInfo.EdgeAlarmFlags.NodeUpdateNotification	:= FALSE;
			MpAlarmXSet(mpAlarmXCore, 'NodeUpdateNotification');	
		END_IF
		
	// ==================================================================================================================================
	END_IF
	
	alarmUIconnect.AcknowledgeAll	:= gMain.cmd.reset;
	
	MpAlarmXCore_0(ErrorReset := gMain.cmd.reset);
	MpAlarmXListUI_0(ErrorReset := gMain.cmd.reset);
	MpAlarmXHistory_0(ErrorReset := gMain.cmd.reset);
	MpAlarmXHistoryUI_0(ErrorReset := gMain.cmd.reset);
	
	// get the alarm reactions
	gAlarmInfo.rxnStopMachine	:= MpAlarmXCheckReaction(mpAlarmXCore, 'STOP_MACHINE');
	gAlarmInfo.rxnControlOff	:= MpAlarmXCheckReaction(mpAlarmXCore, 'CONTROL_OFF');
	gAlarmInfo.rxnMessage		:= MpAlarmXCheckReaction(mpAlarmXCore, 'MESSAGE');	
	
	// get number of active and pending alarms
	gAlarmInfo.ActiveAlarms		:= MpAlarmXCore_0.ActiveAlarms;
	gAlarmInfo.PendingAlarms	:= MpAlarmXCore_0.PendingAlarms;
	
END_PROGRAM

PROGRAM _EXIT
	
	MpAlarmXCore_0(Enable := FALSE);
	MpAlarmXListUI_0(Enable := FALSE);
	MpAlarmXHistory_0(Enable := FALSE);
	MpAlarmXHistoryUI_0(Enable := FALSE);
	
END_PROGRAM