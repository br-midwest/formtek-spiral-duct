
TYPE
	JobLogStep_enum : 
		(
		JOBLOG_START, (*Check to see that mapp initialized properly*)
		JOBLOG_IDLE, (*Wait for request to save*)
		JOBLOG_LOAD_DATA, (*Load in the job log data from flash if it exists on startup*)
		JOBLOG_SAVE, (*Save the new data to the xml file. If the file does not exist, create it.*)
		JOBLOG_ERROR
		);
	JobData_typ : 	STRUCT 
		totalSeq : UINT; (*Total number of sequences programmed in the current job (as indicated by non-zero lengths)*)
		totalParts : UINT; (*Total number of ducts programmed in the current job (as calculated from valid sequences)*)
		jobTimeStr : STRING[80]; (*Job time displayed as a string*)
		jobTime : TIME; (*Job time in seconds*)
		endDTstr : STRING[80]; (*Date and time of job end, displayed as a string*)
		endDT : DATE_AND_TIME; (*Date and time of job end*)
		startDTstr : STRING[80]; (*Date and time of job start, displayed as a string*)
		startDT : DATE_AND_TIME; (*Date and time of job start*)
		seq : ARRAY[0..50]OF sequence_typ; (*Recipe sequence information*)
	END_STRUCT;
END_TYPE
