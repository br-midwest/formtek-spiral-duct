(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Author:		Brad Jones
 * Created:		Jan 24th 2018/08:20 
*********************************************************
[PROGRAM INFORMATION]
This task is responsible for recording information about
completed jobs.
	-Start time
	-End time
	-Total elapsed time of job
	-Total number of ducts produced
	-Number of sequences in the job
	
It also stores the last 20 jobs that were completed in
full in the array JobLog
********************************************************)

PROGRAM _INIT
	
	// mapp recipe register pv
	MpRecipeRegPar_JobLog.MpLink	:= ADR(mpRcpJobLog);
	MpRecipeRegPar_JobLog.Category	:= ADR('JobLog');
	MpRecipeRegPar_JobLog.PVName	:= ADR('JobTimes:JobLog');
	MpRecipeRegPar_JobLog.Enable	:= TRUE;
	
	// mapp reciep xml core
	MpRecipeXml_JobLog.Category		:= ADR('JobLog');
	MpRecipeXml_JobLog.FileName		:= ADR('JobLog');
	MpRecipeXml_JobLog.MpLink		:= ADR(mpRcpJobLog);
	MpRecipeXml_JobLog.Enable		:= TRUE;
	
	IF gSimulate THEN
		MpRecipeXml_JobLog.DeviceName	:= ADR(FILE_DEV_NAME_SIM);
	ELSE
		MpRecipeXml_JobLog.DeviceName	:= ADR(FILE_DEV_NAME_FPART);
	END_IF
	
	jobHistoryIndex		:= 0;
	jobHistorySeqIndex	:= 1;
	joblogStep			:= JOBLOG_START;

END_PROGRAM

PROGRAM _CYCLIC
	
	// get job times at start, end and during
	IF EDGEPOS(gMain.sts.jobRunning) THEN
		// clear current job time info
		Job.endDT	:= 0;
		Job.startDT	:= 0;
		Job.jobTime	:= 0;
		
		Job.startDTstr	:= '';
		Job.endDTstr	:= '';
		Job.jobTimeStr	:= '';
		
		// get the current job start time
		DTGetTime_0(enable := TRUE);
		Job.startDT	:= DTGetTime_0.DT1;
		ascDT(Job.startDT, ADR(Job.startDTstr), SIZEOF(Job.startDTstr));
		
	ELSIF EDGENEG(gMain.sts.jobRunning) THEN
		// get the current job end time
		DTGetTime_0(enable := TRUE);
		Job.endDT	:= DTGetTime_0.DT1;
		ascDT(Job.endDT, ADR(Job.endDTstr), SIZEOF(Job.endDTstr));
		
		// calculate the difference between start and end job times
		Job.jobTime	:= UDINT_TO_TIME(DiffDT(Job.endDT, Job.startDT) * 1000);
		ascTIME(Job.jobTime, ADR(Job.jobTimeStr), SIZEOF(Job.jobTimeStr));
		
		// retrieve job data
		FOR i := 1 TO gMain.par.totalSeq DO
			Job.seq[i].Lng	:= gMain.par.seq[i].Lng;
			Job.seq[i].Qty	:= gMain.par.seq[i].Qty;
		END_FOR	
		Job.totalParts := gMain.sts.totalParts;
		Job.totalSeq := gMain.par.totalSeq;
		
		// keep a log of the last 20 jobs completed
		FOR i := 19 TO 1 BY -1 DO
			brsmemmove(ADR(JobLog[i]), ADR(JobLog[i-1]), SIZEOF(JobLog[i]));
		END_FOR
		JobLog[0]	:= Job;
		joblogSave	:= TRUE;
		
	ELSIF gMain.sts.jobRunning THEN
		// get the current time to compare to start time
		DTGetTime_0(enable := TRUE);
		
		// calculate the difference between start time and current time
		Job.jobTime	:= UDINT_TO_TIME(DiffDT(DTGetTime_0.DT1, Job.startDT) * 1000);
		ascTIME(Job.jobTime, ADR(Job.jobTimeStr), SIZEOF(Job.jobTimeStr));
	END_IF
	
	// access the job requested from the log to view
	pjob ACCESS ADR(JobLog[jobHistoryIndex]);
	pseq ACCESS ADR(JobLog[jobHistoryIndex].seq[jobHistorySeqIndex]);


	// keep data from job log stored in flash
	CASE joblogStep OF
		JOBLOG_START:
			IF MpRecipeRegPar_JobLog.Active AND NOT(MpRecipeRegPar_JobLog.Error) AND MpRecipeXml_JobLog.Active AND NOT(MpRecipeXml_JobLog.Error) THEN
				joblogStep	:= JOBLOG_LOAD_DATA;	
			END_IF
		
		JOBLOG_IDLE:
			IF joblogSave THEN
				joblogSave	:= FALSE;
				joblogStep	:= JOBLOG_SAVE;
			END_IF
		
		JOBLOG_LOAD_DATA:
			MpRecipeXml_JobLog.Load	:= TRUE;
		
			IF MpRecipeXml_JobLog.Error AND (MpRecipeXml_JobLog.StatusID = mpRECIPE_ERR_INVALID_FILE_NAME OR MpRecipeXml_JobLog.StatusID = mpRECIPE_ERR_LOAD_DATA) THEN
				MpRecipeXml_JobLog.Load			:= FALSE;
				joblogErrAck					:= TRUE;
				joblogStep						:= JOBLOG_SAVE;
			ELSIF MpRecipeXml_JobLog.CommandDone THEN
				MpRecipeXml_JobLog.Load			:= FALSE;
				joblogStep						:= JOBLOG_IDLE;
			END_IF
		
		JOBLOG_SAVE:
			MpRecipeXml_JobLog.Save	:= TRUE;
		
			IF MpRecipeXml_JobLog.Error THEN
				MpRecipeXml_JobLog.Save			:= FALSE;
				joblogStep						:= JOBLOG_ERROR;
			ELSIF MpRecipeXml_JobLog.CommandDone THEN
				MpRecipeXml_JobLog.Save			:= FALSE;
				joblogStep						:= JOBLOG_IDLE;
			END_IF
		
		JOBLOG_ERROR:
			MpRecipeXml_JobLog.Load			:= FALSE;
			MpRecipeXml_JobLog.Save			:= FALSE;
			MpRecipeXml_JobLog.ErrorReset	:= FALSE;
			
			IF joblogErrAck THEN
				joblogErrAck	:= FALSE;
				joblogStep		:= JOBLOG_IDLE;
			END_IF
		
	END_CASE
	
	// clear job history if requested
	IF clearJobHist THEN
		clearJobHist	:= FALSE;
		brsmemset(ADR(JobLog), 0, SIZEOF(JobLog));
	END_IF
	
	MpRecipeXml_JobLog.ErrorReset := gMain.cmd.reset;
	MpRecipeRegPar_JobLog.ErrorReset := gMain.cmd.reset;
	
	MpRecipeRegPar_JobLog();
	MpRecipeXml_JobLog();
	
END_PROGRAM

PROGRAM _EXIT
  
	MpRecipeXml_JobLog.Enable		:= FALSE;
	MpRecipeRegPar_JobLog.Enable	:= FALSE;
	MpRecipeRegPar_JobLog();
	MpRecipeXml_JobLog();
   
END_PROGRAM
      