(********************************************************************************************************1
 * Copyright: B&R Industrial Automation
*********************************************************************************************************
 * Author:		Brad Jones
 * Created:		2018/10:43 
*********************************************************************************************************
[PROGRAM INFORMATION]

V4.00 - Sofware as-recieved pre-cleanup

V5.00 - Re-release of sw after major cleanups
		Renamed many variables to have useful names, added variables to structures to organize groups, added
		comments to as many variables as possible
		Consistent engineering units throughout (software uses mm). Any other display units are handled through
		visual components.
		Added performance features (see bitbucket list)
		Added automatic calibration (see bitbucket list)

V5.01 - BUGFIX: Breakout bit was set incorrectly if a new program was entered

V5.02 - BUGFIX: Resetting job timer while a job was not running would latch the "JobRunning" status low

V5.03 - BUGFIX: Job queue did not reset when changing modes or programming in new job

V6.00 - CHANGE: Machine references the photo eye each time it is passed. After calibration, the distance to
		the photo eye is set, so each subsequent pass of the eye assumes that it has not moved. This will 
		allow for mechanical slop to be covered up
		
		CHANGE: Deleted task CalibEye (nothing useful in there), renamed SetSnsLen to Calibration

		FEATURE: Added new screen to the Auto Menu to show calibration data.

V6.01 - BUGFIX: When a discharge table is used, sometimes the part would not get out of the way fast enough 
		and a false trigger would happen on the photoeye. A mask has been added if the calibration is valid
		to check for a minimum length produced before allowing new latches

V6.02 - FEATURE: Crimping detection and speed limiting. Schematic change is associated with this update!
		Added diCrimperIsClear and diCrimperActive to I/O list and mapping.
		CHANGE: Updated configuration name in the CPU configuration for each DMAX config
		CHANGE: In 1.2e 230V config, changed motor run mode to CUC, corrected NPR (was 1200, should have been 161)

V6.03 - BUGFIX: Changed motor run mode back to UUC, and turned fluxing FLU to continuous FCT
		BUGFIX: Properly limited the top speed of the 1.2e 230v

V6.04 - BUGFIX: Changed fluxing FLU to not-continuous FNC

V6.05 - CHANGE: adjusted max speeds and calibration acceleration for 1.2e models (measured with Jerry & tach)
		CHANGE: turned AUT to YES (automatic autotune). This was a setting adjusted by Chuck and Matt during
		and emergency service visit

V6.06 - CHANGE: small edits to the simulation configuration such that the VNC screens can be operated
		Updated the BRUSHelper library to include function isSim which check the target type

V6.07 - FEATURE: Added domain input to the devlink function conditional on the string being populated. Updated the
		visu page corresponding to this input.

V6.08 - FEATURE: Allowed VDF faults to be reset using the stop pb on the panel enclosure

V7.00 - Migrate project to AS4.7
		Remove SDC 
		One configuration to rule them all
		Check repo for rev notes

*********************************************************************************************************)

PROGRAM _INIT
	
	// get the config version
	ArProjectGetInfo_0(Execute := TRUE);
	SW_VERSION := ArProjectGetInfo_0.ConfigurationVersion; 
	SW_CONFIG_ID := ArProjectGetInfo_0.ConfigurationID;
	
	gMain.step := MAIN_IDLE;

	gSimulate := DiagCpuIsSimulated();
	
	gMain.par.stripThickness := 0;
	gMain.sts.seqIdx := 1;

END_PROGRAM

PROGRAM _CYCLIC

	(*Determine machine operating mode based on selector switch*)
	IF gIO.di.Auto1ModeSelected THEN
		gMain.opMode := OP_MODE_AUTO1;
	ELSIF gIO.di.Auto2ModeSelected THEN
		gMain.opMode := OP_MODE_AUTO2;
	ELSIF gIO.di.ManualModeSelected THEN
		gMain.opMode := OP_MODE_MANUAL;
	ELSIF gIO.di.SetupModeSelected THEN
		gMain.opMode := OP_MODE_SETUP;
	ELSE 
		gMain.opMode := OP_MODE_NONE;
	END_IF
	
	gMain.sts.autoMode := (gMain.opMode = OP_MODE_AUTO1) OR (gMain.opMode = OP_MODE_AUTO2);
	
	// check for control power on
	IF gIO.di.ControlOnPressed AND NOT(gAlarmInfo.rxnControlOff) THEN
		gMain.sts.controlOn	:= TRUE;
	END_IF
	
	IF gAlarmInfo.rxnControlOff OR NOT(gIO.di.CntrlOffReleased) THEN 
		gMain.sts.controlOn := FALSE;
	END_IF
	
	// mode changes, stop button and alarms can stop the machine
	stopBcModeChanged := (gMain.opMode <> prevOpMode) AND gMain.cmd.runMotor;
	prevOpMode := gMain.opMode;
	
	gMain.cmd.stop := stopBcModeChanged OR gAlarmInfo.rxnStopMachine OR NOT(gIO.di.StopBtnReleased);
	
	IF EDGEPOS(stopBcModeChanged) THEN
		gAlarmInfo.EdgeAlarmFlags.ModeChangeWhileRunning := TRUE;
	END_IF	

	// global state changes
	IF gAlarmInfo.rxnStopMachine OR gAlarmInfo.rxnControlOff THEN
		gMain.step := MAIN_ERROR;
	ELSIF NOT(gMain.sts.controlOn) AND (gAlarmInfo.ActiveAlarms = 0) AND (gMain.step <> MAIN_IDLE) THEN
		gMain.step := MAIN_IDLE;
	END_IF	
	
	// stop the active job if the stop button is pressed
	IF gMain.cmd.stop THEN
		gMain.sts.jobRunning := FALSE;
	END_IF

	// MAIN STATE MACHINE
	CASE gMain.step OF
		MAIN_IDLE: 
			gMain.sts.jobRunning := FALSE;
			
			IF gMain.sts.controlOn THEN
				gMain.step := MAIN_READY;
			END_IF			
			
		MAIN_READY:
			// allow use of the piece if it is shorter than the next requested length
			CASE gMain.opMode OF
				OP_MODE_AUTO1, OP_MODE_AUTO2:
					pieceOkToUse	:= (gPart.LngProduced < gMain.par.seq[gMain.sts.seqIdx].Lng);
				OP_MODE_MANUAL:
					pieceOkToUse	:= (gPart.LngProduced < gMain.par.manualLng);
				ELSE
					pieceOkToUse	:= FALSE;
			END_CASE				

			// check if the created piece is continuous/uncut up to the photoeye
			atLeastPELng := (gPart.LngProduced >= gCal.calcSnsDist);
			
			// Hyd pump needs to be on to move motor, and also duct can't exceed max lng
			okToMove := (gPart.LngProduced < MAX_ALLOWED_PART_LNG) AND gIO.di.HydPumpOnSelect;
	
			(***** State transitions *****)
			//Transition to calibration state
			IF gMain.sts.calInPrg THEN
				gMain.step := MAIN_CALIBRATING;
				
			// Transition to production (automatically)
			ELSIF gMain.sts.jobRunning AND (gMain.opMode = OP_MODE_AUTO2) THEN			
				gMain.step := MAIN_PRODUCING;
				
			// Transition to production
			ELSIF EDGEPOS(gIO.di.ProdOnPressed) THEN
				CASE gMain.opMode OF
					OP_MODE_MANUAL, OP_MODE_AUTO1, OP_MODE_AUTO2:
						IF okToMove AND pieceOkToUse AND (NOT(gIO.di.LngSns) OR (gIO.di.LngSns AND atLeastPELng)) THEN
							gMain.step := MAIN_PRODUCING;
							
							IF (gMain.opMode = OP_MODE_AUTO2) THEN
								gMain.sts.jobRunning := TRUE;
							END_IF
							
						ELSIF pieceOkToUse AND gIO.di.LngSns AND NOT(atLeastPELng) THEN
							gAlarmInfo.EdgeAlarmFlags.CannotStart := TRUE;
							
						ELSIF NOT(pieceOkToUse) THEN
							gAlarmInfo.EdgeAlarmFlags.CannotUsePart := TRUE;
							
						ELSIF NOT(gIO.di.HydPumpOnSelect) THEN
							gAlarmInfo.EdgeAlarmFlags.HydraulicPumpOff := TRUE;
							
						ELSIF (gPart.LngProduced > MAX_ALLOWED_PART_LNG) THEN
							gAlarmInfo.EdgeAlarmFlags.PartTooLong := TRUE;
							
						END_IF
					ELSE
						gAlarmInfo.EdgeAlarmFlags.NoModeSelected := TRUE;
				END_CASE
				
			// Transition to cutting
			ELSIF EDGEPOS(gIO.di.CutterOnPressed) THEN
				CASE gMain.opMode OF
					OP_MODE_MANUAL:
						IF gIO.di.HydPumpOnSelect THEN
							gMain.step := MAIN_START_CUT;
							
						ELSIF NOT(gIO.di.HydPumpOnSelect) THEN
							gAlarmInfo.EdgeAlarmFlags.HydraulicPumpOff := TRUE;
							
						END_IF
					ELSE
						gAlarmInfo.EdgeAlarmFlags.CuttingNotAllowed := TRUE;
				END_CASE
				
			// Transition to discharge
			ELSIF EDGEPOS(gIO.di.DischargeOnPressed) THEN
				CASE gMain.opMode OF
					OP_MODE_AUTO1,OP_MODE_MANUAL:
						gMain.step := MAIN_DISCHARGE;
					ELSE
						gAlarmInfo.EdgeAlarmFlags.DischargeNotAllowed := TRUE;
				END_CASE
			END_IF
			
		MAIN_CALIBRATING:
			
			IF gMain.cmd.stop OR NOT(gMain.sts.calInPrg) THEN
				gMain.step := MAIN_READY;
			END_IF					

		(*********** #21 PRODUCING DUCT STATE ACTION **********)
		(***** State description: Feed sheet metal until the encoder position
		* reaches the anticipation value - the encoder value that will
		* trigger a slow down in order TO reach the correct part length.
		* In Auto Mode #2, the discharge ramp is retracted in this state. *****)
		//	Auto Mode #1 and #2:
		//		a.	Motor feeds material from the spool of sheet metal at uniform speed.
		//		b.	Clincher is enabled, forming the duct shape.
		//		c.	When the encoder exceeds the anticipation length, proceed TO FINISH FEED.
		MAIN_PRODUCING: 
			(***** State transitions *****)
			IF (gMain.cmd.stop) THEN 
				gMain.step := MAIN_READY;
			END_IF
	
			// produce until slow down required
			lookForSlowDown := (DINT_TO_LREAL(gPart.CntsThisPart) > ((gPart.LngToMakeCnts - gPart.ProdDecelCnts) * 0.75));
			
			(*** When the target encoder value is reached, trigger transition to tube finishing state ***)
			IF (gPart.CntsThisPart >= 0) AND (lookForSlowDown OR slowToCutSpd) THEN
				gMain.step := MAIN_FINISH_PROD;
				
			END_IF


		(*********** #22 FINISH PRODUCING DUCT STATE ACTION **********)
		(***** State description: Continue feeding tube until the target encoder
		* count is reached FOR park length. 
		* trigger a slow down in order TO reach the correct part length.
		* In Auto Mode #2, the discharge ramp is retracted in this state. *****)
		//	Auto Mode #1 and #2:
		//		a.	Slow down TO finishing speed near end OF part production.
		//		b.	If the encoder value is greater than the target length, proceed TO the CUT state.
		MAIN_FINISH_PROD: 
			IF gMain.cmd.stop THEN
				gMain.step := MAIN_READY;
			END_IF
			
			// TODO: possibly come up with some factor for stopping ahead of the target so it's always just a bit short
			//stopCalcFOS := (1 + (gIO.ai.ProdSpeedPot.scaledValue / 1000.0));
			
			// HOW TO MORE ACCURATELY DETERMINE HOW MANY COUNTS ARE NEEDED TO STOP? LOOK AT ACTUAL SPEED MAYBE?
			IF gMain.cmd.runProdSpd THEN
				targetCntReached := (gPart.CntsThisPart >= (gPart.LngToMakeCnts - gPart.ProdDecelCnts - (gMain.par.userOvershoot * MM_TO_ENCCNT)));
			ELSE
				targetCntReached := (gPart.CntsThisPart >= (gPart.LngToMakeCnts - gPart.CutDecelCnts - (gMain.par.userOvershoot * MM_TO_ENCCNT)));
			END_IF
			
			IF targetCntReached THEN
				gMain.step := MAIN_CHECK_LNG;
			END_IF
			
			
		MAIN_CHECK_LNG:
			IF gMain.cmd.stop THEN
				gMain.step := MAIN_READY;
			END_IF
			
			stopProducing := gPart.CntsThisPart >= (gPart.LngToMakeCnts - gPart.FineDecelCnts - (gMain.par.userOvershoot * MM_TO_ENCCNT));
			
			CASE gMain.opMode OF
				OP_MODE_AUTO1, OP_MODE_AUTO2:
					IF stopProducing THEN
						gMain.step := MAIN_START_CUT;
					END_IF
					
				OP_MODE_MANUAL:				
					IF stopProducing THEN
						gMain.step := MAIN_READY;
					END_IF
			END_CASE

		(*********** #23 CUT TUBE ACTION **********)
		(***** State description: Engage the cutter hydraulics and begin the
		* cutting move. A wait is programmed TO allow FOR drive TO stop
		* before the cutter is engaged. *****)
		MAIN_START_CUT:
			(* Reset raw encoder value *)
			IF readyToCut THEN
				gPart.CntsThisPart	:= 0;
				gIO.enc.Reset := TRUE;
			END_IF

			(*** State transitions ***)
			IF gMain.cmd.stop THEN 
				gMain.step := MAIN_READY;
			END_IF
	
			(* Once cutter is engaged, trigger transition to waiting for limit switch state *)
			IF TON_CutDwell.Q AND TON_WaitEncReset.Q THEN
				IF gIO.enc.Value > ENC_RESET_OK_TOL THEN
					gMain.step := MAIN_READY;				
					gAlarmInfo.EdgeAlarmFlags.FailedToResetEncoder	:= TRUE;
					
				ELSIF gIO.enc.Value <= ENC_RESET_OK_TOL THEN
					gMain.step := MAIN_CUTTING;
					
					// advance the job
					CASE gMain.opMode OF
						OP_MODE_AUTO1, OP_MODE_AUTO2:
							gMain.sts.cutsMadeInCurrSeq := gMain.sts.cutsMadeInCurrSeq + 1;(* Add to cut count *)
					END_CASE
				END_IF
				// reset the command to clear the encoder value
				gIO.enc.Reset := FALSE;
			END_IF

		(*********** #24 WAIT FOR CUTTER LIMIT SWITCH STATE ACTION **********)
		(***** State description: Tube is fed forward at a lower speed so a
		* circular cut can be made. The cut is complete when the cutter
		* reaches the LIMIT switch. *****)
		MAIN_CUTTING: 
			(*** State transition ***)
			(* Back to init state if mode changes or not in auto mode #2 when limit switch is reached *)
			IF (gMain.cmd.stop OR ((gMain.opMode <> OP_MODE_AUTO2) AND gIO.di.CutterSlideLimit)) THEN 
				gMain.step := MAIN_READY;
			END_IF
	
			(* LS is reached in auto mode #2, triggering transition to the tube discharge state *)
			IF (gMain.opMode = OP_MODE_AUTO2) AND gIO.di.CutterSlideLimit AND NOT gMain.cmd.stop AND gMain.sts.controlOn THEN
				gMain.step := MAIN_DISCHARGE;
			END_IF


		(*********** #25 TUBE DISCHARGE STATE ACTION **********)
		(***** State description: Slide the tube down the discharge ramp, which
		* is raised hydraulically. *****)
		MAIN_DISCHARGE: 
			(*** State transitions ***)
			IF gMain.cmd.stop THEN
				gMain.step := MAIN_READY;
			END_IF

			CASE gMain.opMode OF 
				OP_MODE_AUTO1, OP_MODE_MANUAL:
					IF TON_FixedDisch.Q THEN
						gMain.step := MAIN_READY;
					END_IF
					
				OP_MODE_AUTO2:
					(* Once discharge is completed, continue with next part *)
					IF TON_FixedDisch.Q THEN
						IF gMain.sts.jobRunning THEN
							gMain.step := MAIN_PRODUCING;	
						ELSE
							gMain.step := MAIN_READY;
						END_IF
					END_IF
			END_CASE
		
		MAIN_ERROR:
			gMain.sts.jobRunning := FALSE;
			
			IF NOT(gAlarmInfo.rxnControlOff) AND NOT(gAlarmInfo.rxnStopMachine) THEN
				gMain.step := MAIN_RESETTING;
			END_IF
		
		MAIN_RESETTING:
			IF gMain.sts.controlOn THEN
				gMain.step := MAIN_READY;
			ELSE
				gMain.step := MAIN_IDLE;
			END_IF
	
	END_CASE
	
	// check for hanging active states (due to operator)
	TON_IdleCheck.IN := ( (gMain.step = prevStep) AND ( gMotorSts.standstill ) ) AND (gMain.step <> MAIN_ERROR) AND (gMain.step <> MAIN_READY) AND (gMain.step <> MAIN_IDLE);
	TON_IdleCheck.PT := T#5m;
	TON_IdleCheck();
	prevStep := gMain.step;
	
	IF EDGEPOS(TON_IdleCheck.Q) THEN
		gAlarmInfo.EdgeAlarmFlags.IdleInActiveState := TRUE;
	END_IF
	
	// Retract discharge ramp 
	// This controls how long the system will wait once it hits st21 before it starts producing duct
	TON_DischRetract.IN := (gMain.step = MAIN_PRODUCING) AND (gMain.opMode = OP_MODE_AUTO2);
	TON_DischRetract.PT := REAL_TO_TIME(gMain.par.dischRetractDelay * 1000); //Convert seconds to ms
	TON_DischRetract();
	
	(* Cut delay timer *)
	TON_CutDelay.IN	:= (gMain.step = MAIN_START_CUT) AND gMain.sts.autoMode;
	TON_CutDelay.PT	:= REAL_TO_TIME(gMain.par.autoCutDelay * 1000); //Convert seconds to ms
	TON_CutDelay();
	
	(* Programmed wait time for drive to stop *)
	TON_CutDwell.IN	:= (gMain.step = MAIN_START_CUT) AND readyToCut;
	TON_CutDwell.PT	:= REAL_TO_TIME(gMain.par.cutDwell * 1000); //Convert seconds to ms
	TON_CutDwell();
	
	TON_WaitEncReset.IN	:= (gIO.enc.Reset);
	TON_WaitEncReset.PT	:= T#500ms;
	TON_WaitEncReset();
	
	(* Wait for discharge to occur *)
	TON_FixedDisch.IN := (gMain.step = MAIN_DISCHARGE);
	TON_FixedDisch.PT := T#500ms;
	TON_FixedDisch();
	
	TON_AutoSlowStart.PT := REAL_TO_TIME(gMain.par.autoSlowStartDelay * 1000);
	TON_AutoSlowStart.IN := gMain.sts.dischClear AND (gMain.step = MAIN_PRODUCING) AND gMain.sts.autoMode;
	TON_AutoSlowStart();
	
	TON_HydRunning.IN := NOT(gIO.di.HydPressureIsLow);
	TON_HydRunning.PT := T#10s;
	TON_HydRunning();
	
	(*Continuous flasher function, use as needed*)
	TPC_Flasher.IN := TRUE;
	TPC_Flasher.PT := T#1s;
	TPC_Flasher();
	
	// set status
	gMain.sts.dischClear	:= (gMain.step = MAIN_PRODUCING) AND ((gMain.opMode <> OP_MODE_AUTO2) OR TON_DischRetract.Q);
	readyToCut		:= (gMain.step = MAIN_START_CUT) AND (NOT(gMain.sts.autoMode) OR TON_CutDelay.Q); // no delay in manual mode
	slowToCutSpd	:= ((gMain.step = MAIN_FINISH_PROD) OR (gMain.step = MAIN_FINISH_PROD)) AND (gPart.LngProduced > gPart.LngToMakeAtProdSpd);
	
	// set I/O
	gMain.cmd.runProdSpd 	:= ((gMain.step = MAIN_PRODUCING) AND gMain.sts.dischClear AND (NOT(gMain.sts.autoMode) OR (gMain.sts.autoMode AND TON_AutoSlowStart.Q)))
							OR ((gMain.step = MAIN_FINISH_PROD) AND NOT(slowToCutSpd))
							OR (gMain.cmd.runCal);
	
	gMain.cmd.runCutSpd 	:= (gMain.step = MAIN_CUTTING)
							OR ((gMain.step = MAIN_PRODUCING) AND gMain.sts.dischClear AND gMain.sts.autoMode AND NOT(TON_AutoSlowStart.Q))
							OR ((gMain.step = MAIN_FINISH_PROD) AND slowToCutSpd);
	
	gMain.cmd.runFineSpd	:= (gMain.step = MAIN_CHECK_LNG);

	gMain.cmd.runMotor 	:= ((gMain.cmd.runProdSpd OR gMain.cmd.runCutSpd OR gMain.cmd.runFineSpd) AND NOT(gMain.sts.overrideInPrg)) OR gMain.cmd.runOverride;
	
	gIO.dout.ProdOnLight		:= ( (((gMain.sts.dischClear AND (gMain.step = MAIN_PRODUCING)) OR (gMain.step = MAIN_FINISH_PROD) OR (gMain.step = MAIN_CUTTING)) OR gMain.sts.jobRunning) AND (gMotorSts.autotuneState <> TUNE_IN_PRG) ) OR
									( (TPC_Flasher.Q) AND (gMotorSts.autotuneState = TUNE_IN_PRG) );
	gIO.dout.CutterOnLight		:= (readyToCut AND (gMain.step = MAIN_START_CUT)) OR (gMain.step = MAIN_CUTTING);
	gIO.dout.CutterGoUp			:= ((readyToCut AND (gMain.step = MAIN_START_CUT)) OR (gMain.step = MAIN_CUTTING));
	gIO.dout.UnlockCutterSlide	:= (gMain.step = MAIN_CUTTING);
	gIO.dout.ControlOnLight		:= gMain.sts.controlOn;
	gIO.dout.LubePumpGoOn		:= NOT(gIO.di.PumpOverloadTrip) AND gMain.sts.controlOn AND gIO.di.LubePumpOnSelect;
	gIO.dout.DriveEnable 		:= NOT(gIO.di.PumpOverloadTrip) AND gMain.sts.controlOn AND gIO.di.HydPumpOnSelect;	
	gIO.dout.ClinchRollGoUp		:= gIO.di.ClinchUpSelect;
	
	IF(gMain.mchType = MCHTYP_12E_230V_P84) OR (gMain.mchType = MCHTYP_12E_230V_P66) THEN
		gIO.dout.HydPumpGoOn := gIO.di.HydPumpOnSelect AND (gIO.di.HydPressureIsLow OR NOT TON_HydRunning.Q) AND gMain.sts.controlOn;
	ELSE
		gIO.dout.HydPumpGoOn	:= gIO.di.HydPumpOnSelect AND (gIO.di.HydPressureIsLow OR NOT TON_HydRunning.Q);
	END_IF
	
END_PROGRAM
