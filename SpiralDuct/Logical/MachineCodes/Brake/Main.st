

PROGRAM _INIT

	 
END_PROGRAM

PROGRAM _CYCLIC
	
	(*** Brake activation logic ***)
	
	// activate brake if decelerating (+ hold time)
	TOF_DecelHold.IN := gMotorSts.decelerating;
	TOF_DecelHold.PT := REAL_TO_TIME(gMain.par.brakeOnTime * MS_TO_SEC);
	TOF_DecelHold();
	
	decelerating := ( TOF_DecelHold.Q );
	
	// activate brake if production nears a slow down
	//prodNearSlowDown := (gMain.step = MAIN_FINISH_PROD);
	
	// activate brake while stopped
	motorNotRunning := ( gMotorSts.standstill ) AND NOT( gMain.sts.calInPrg );
	
	// activate the brake when there is an error?
	errorStop := gAlarmInfo.rxnStopMachine;
	
	
	(* Turn on brake *)
	gIO.dout.DecoilerBrakeGoOn := gMain.sts.controlOn AND ( decelerating OR prodNearSlowDown OR motorNotRunning OR errorStop);

	
END_PROGRAM
