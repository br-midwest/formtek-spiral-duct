
PROGRAM _CYCLIC

	// chose the sequence to edit
	IF LineDn THEN
		editIdx := LIMIT(1, editIdx + 1, MAX((gMain.par.totalSeq - LIST_SIZE_IDX), 1));
	ELSIF LineUp THEN
		editIdx := LIMIT(1, editIdx - 1, MAX((gMain.par.totalSeq - LIST_SIZE_IDX), 1));
	ELSIF PageDn THEN
		editIdx := LIMIT(1, editIdx + LIST_SIZE, MAX((gMain.par.totalSeq - LIST_SIZE_IDX), 1));
	ELSIF PageUp THEN
		editIdx := LIMIT(1, editIdx - LIST_SIZE, MAX((gMain.par.totalSeq - LIST_SIZE_IDX), 1));
	END_IF
	
	LineUp := FALSE;
	LineDn := FALSE;
	PageUp := FALSE;
	PageDn := FALSE;
	
	// if the seq num changes outside of editor, check the index
	editIdx := LIMIT(1, editIdx, MAX((gMain.par.totalSeq - LIST_SIZE_IDX), 1));
	
	FOR i := 0 TO LIST_SIZE_IDX DO
		seqNo[i] := INT_TO_USINT(editIdx + i);
	END_FOR
	
	// point to the edited element
	pSeq ACCESS ADR(gMain.par.seq[editIdx]);
	
	// calculate how many ducts are configured currently
	gMain.sts.totalParts	:= 0;
	FOR i := 1 TO gMain.par.totalSeq DO
		// make sure all new recipe entries have valid values
		IF gMain.par.seq[i].Lng < MIN_ALLOWED_PART_LNG THEN
			gMain.par.seq[i].Lng := MIN_ALLOWED_PART_LNG;
		END_IF
		IF gMain.par.seq[i].Qty = 0 THEN
			gMain.par.seq[i].Qty := 1;
		END_IF
		
		// count current amount of total parts
		IF gMain.par.seq[i].Lng > 0.0 THEN
			gMain.sts.totalParts	:= gMain.sts.totalParts + gMain.par.seq[i].Qty;
		END_IF
	END_FOR	
	
	// enter new program
	IF gMain.cmd.clearCurrentRcp OR (gMain.par.seq[1].Lng = 0.0) THEN
		gMain.cmd.clearCurrentRcp := FALSE;
		
		memset(ADR(gMain.par.seq), 0, SIZEOF(gMain.par.seq));
		
		gMain.par.totalSeq	:= 1;
		editIdx				:= 1;
		
		gMain.par.seq[1].Lng	:= 1524.0; // 60in in mm
		gMain.par.seq[1].Qty	:= 1;
		
		// reset the currently active job if you go to enter a new program
		gMain.sts.cutsMadeInCurrSeq	:= 0;
		gMain.sts.seqIdx			:= 1;
		gMain.sts.jobRunning		:= FALSE;
	END_IF
	
END_PROGRAM
