
ACTION TestPanel: 

	// some logic for testing
	IF gIO.enc.Value >= 825500 THEN
		testPanelIn[0] := TRUE; // length sensor trigger
	END_IF
	IF gMain.step = MAIN_DISCHARGE THEN
		testPanelIn[0] := FALSE;
	END_IF
	
	TP_CutterSlide.IN := (gMain.step = MAIN_CUTTING) AND (gPart.LngProduced > 250);
	TP_CutterSlide.PT := T#1s;
	TP_CutterSlide();
	
	testPanelIn[13] := TP_CutterSlide.Q;
	
	// digital inputs
	gIO.di.LngSns					:= testPanelIn[0];
	gIO.di.CutterOnPressed			:= testPanelIn[1];
	gIO.di.DischargeOnPressed		:= testPanelIn[2];
	gIO.di.SetupModeSelected		:= testPanelIn[3];
	gIO.di.ManualModeSelected		:= testPanelIn[4];
	gIO.di.Auto1ModeSelected		:= testPanelIn[5];
	gIO.di.Auto2ModeSelected		:= testPanelIn[6];
	gIO.di.TravelLimIsClear			:= testPanelIn[7];
	gIO.di.LubePumpOnSelect			:= testPanelIn[8];
	gIO.di.PumpOverloadTrip			:= testPanelIn[9];
	gIO.di.HydPressureIsLow			:= testPanelIn[10];
	gIO.di.AirPressureOk			:= testPanelIn[11];
	gIO.di.CrimperIsClear			:= testPanelIn[12];
	gIO.di.CutterSlideLimit			:= testPanelIn[13];
	gIO.di.ClinchUpSelect			:= testPanelIn[14];
	gIO.di.HydPumpOnSelect			:= testPanelIn[15];
	gIO.di.CntrlOffReleased			:= testPanelIn[16];
	gIO.di.ControlOnPressed			:= testPanelIn[17];
	gIO.di.StopBtnReleased			:= testPanelIn[18];
	gIO.di.ProdOnPressed			:= testPanelIn[19];
	
	// analog inputs
	gIO.ai.CutSpeedPot.scaledValue	:= testPanelPot[0];
	gIO.ai.CutSpeedPot.status		:= 0;
	
	gIO.ai.ProdSpeedPot.scaledValue	:= testPanelPot[1];
	gIO.ai.ProdSpeedPot.status		:= 0;
	
	// test panel estop logic
	estopped := FALSE; // todo - make this something?
	
	// todo: for sim, simulate motor/encoder movement
	
END_ACTION
