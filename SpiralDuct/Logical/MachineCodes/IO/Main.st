
PROGRAM _INIT

END_PROGRAM

PROGRAM _CYCLIC

	// ======================= check IO status bits =======================
	gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= FALSE;
	
		FOR i := 0 TO IO_SIZE DO
			IF ModuleOK[i] THEN
				IOAlarmStr	:= 'Module unplugged or missing!';
				gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= TRUE;			
				EXIT;
			END_IF
		END_FOR	
		
		FOR i := 0 TO IO_SIZE DO
			IF DigitalOutputStatus[i] THEN
				IOAlarmStr	:= 'Digital Output status problem!';
				gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= TRUE;			
				EXIT;
			END_IF
		END_FOR	
		
		FOR i := 0 TO IO_SIZE DO
			IF PSUBusPwrFault[i] THEN
				IOAlarmStr	:= 'PSU bus power problem!';
				gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= TRUE;			
				EXIT;
			END_IF
		END_FOR	
		
		FOR i := 0 TO IO_SIZE DO
			IF PSUIOPwrFault[i] THEN
				IOAlarmStr	:= 'IO bus power problem!';
				gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= TRUE;
				EXIT;
			END_IF
		END_FOR	
		
		IF gIO.ai.CutSpeedPot.status = AI_STS_OPEN_LINE THEN
			IOAlarmStr	:= 'Cut speed pot wire break!';
			gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= TRUE;
		END_IF
		
		IF gIO.ai.ProdSpeedPot.status = AI_STS_OPEN_LINE THEN
			IOAlarmStr	:= 'Production speed pot wire break!';
			gAlarmInfo.PersistentAlarmFlags.IOAlarm	:= TRUE;
		END_IF
	// =====================================================================
	
	// for forcing, use the actual value until I/O is forced
	IF NOT(gMain.cmd.enableIOForcing) THEN
		FOR i := 0 TO IO_SIZE DO
			ForceDigitalInput[i]	:= DigitalInput[i];
			ForceDigitalOutput[i]	:= DigitalOutput[i];
		END_FOR
	END_IF
	
	// digital inputs
	gIO.di.LngSns					:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[0]) OR ((DigitalInput[0]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.CutterOnPressed			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[1]) OR ((DigitalInput[1]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.DischargeOnPressed		:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[2]) OR ((DigitalInput[2]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.SetupModeSelected		:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[3]) OR ((DigitalInput[3]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.ManualModeSelected		:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[4]) OR ((DigitalInput[4]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.Auto1ModeSelected		:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[5]) OR ((DigitalInput[5]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.Auto2ModeSelected		:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[6]) OR ((DigitalInput[6]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.TravelLimIsClear			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[7]) OR ((DigitalInput[7]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.LubePumpOnSelect			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[8]) OR ((DigitalInput[8]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.PumpOverloadTrip			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[9]) OR ((DigitalInput[9]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.HydPressureIsLow			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[10]) OR ((DigitalInput[10]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.AirPressureOk			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[11]) OR ((DigitalInput[11]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.CrimperIsClear			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[12]) OR ((DigitalInput[12]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.CutterSlideLimit			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[13]) OR ((DigitalInput[13]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.ClinchUpSelect			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[14]) OR ((DigitalInput[14]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.HydPumpOnSelect			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[15]) OR ((DigitalInput[15]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.CntrlOffReleased			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[16]) OR ((DigitalInput[16]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.ControlOnPressed			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[17]) OR ((DigitalInput[17]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.StopBtnReleased			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[18]) OR ((DigitalInput[18]) AND NOT(gMain.cmd.enableIOForcing));
	gIO.di.ProdOnPressed			:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[19]) OR ((DigitalInput[19]) AND NOT(gMain.cmd.enableIOForcing));
	unusedDI						:= (gMain.cmd.enableIOForcing AND ForceDigitalInput[20]) OR ((DigitalInput[20]) AND NOT(gMain.cmd.enableIOForcing));
		
	// digital outputs
	DigitalOutput[0]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[0]) OR (gIO.dout.ControlOnLight AND NOT(gMain.cmd.enableIOForcing));
	DigitalOutput[1]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[1]) OR (gIO.dout.ProdOnLight AND NOT(gMain.cmd.enableIOForcing));
	DigitalOutput[2]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[2]) OR (gIO.dout.CutterOnLight AND NOT(gMain.cmd.enableIOForcing));
	DigitalOutput[3]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[3]) OR (gIO.dout.DischargeLight AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[4]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[4]) OR (gIO.dout.ClinchRollGoUp AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[5]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[5]) OR (gIO.dout.DecoilerBrakeGoOn AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[6]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[6]) OR (gIO.dout.LubePumpGoOn AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[7]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[7]) OR (gIO.dout.UnlockCutterSlide AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[8]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[8]) OR (gIO.dout.CutterGoUp AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[9]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[9]) OR (gIO.dout.HydPumpGoOn AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[10]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[10]) OR (gIO.dout.DischargeGoUp AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[11]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[11]) OR (gIO.dout.VFDContactor AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[12]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[12]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[13]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[13]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[14]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[14]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[15]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[15]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[16]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[16]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[17]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[17]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[18]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[18]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	
	DigitalOutput[19]	:= (gMain.cmd.enableIOForcing AND ForceDigitalOutput[19]) OR (unusedDO AND NOT(gMain.cmd.enableIOForcing));	

	// analog inputs
	gIO.ai.CutSpeedPot.scaledValue	:= INT_TO_REAL(AnalogInput[0]) * ANALOG_PCT_MAX / ANALOG_RAW_MAX;
	gIO.ai.CutSpeedPot.status		:= AnalogStatus[0];
	
	gIO.ai.ProdSpeedPot.scaledValue	:= INT_TO_REAL(AnalogInput[1]) * ANALOG_PCT_MAX / ANALOG_RAW_MAX;
	gIO.ai.ProdSpeedPot.status		:= AnalogStatus[1];
	
	// analog outputs
	AnalogOutput[0]	:= 0;
	
	// encoder - no logic atm, mapped directly to card	
	
	
	// overwrite inputs in case of a test panel
	CASE gMain.mchType OF
		MCHTYP_SIM, MCHTYP_P66_TEST, MCHTYP_P84_TEST, MCHTYP_P86_TEST, MCHTYP_P74_TEST:
			TestPanel;
			
		ELSE
			// estop drops out ALL I/O, so if no DI are present, then allow ESTOP condition through
			estopped := TRUE;
			FOR i := 0 TO IO_SIZE - 1 DO
				IF DigitalInput[i] THEN
					estopped := FALSE;
				END_IF
			END_FOR		
	END_CASE
	
	// wait for all of the I/O to drop out before differentiating estop from other alarms
	TON_DebounceTravelLimit.IN := NOT(gIO.di.TravelLimIsClear);
	TON_DebounceTravelLimit.PT := T#50ms;
	TON_DebounceTravelLimit();
	
	TON_DebounceAirPressure.IN := NOT(gIO.di.AirPressureOk);
	TON_DebounceAirPressure.PT := T#50ms;
	TON_DebounceAirPressure();
	
	// I/O based alarms, set as fast as possible
	gAlarmInfo.PersistentAlarmFlags.EstopActive			:= estopped;
	gAlarmInfo.PersistentAlarmFlags.AirPressureFault	:= NOT(gIO.di.AirPressureOk) AND TON_DebounceAirPressure.Q AND NOT(estopped);
	gAlarmInfo.PersistentAlarmFlags.PumpOverload		:= gIO.di.PumpOverloadTrip;
	
	IF (TON_DebounceTravelLimit.Q AND NOT(estopped)) OR (NOT(gIO.di.TravelLimIsClear) AND EDGEPOS(gIO.di.ProdOnPressed)) THEN
		gAlarmInfo.EdgeAlarmFlags.OvertravelLimit := TRUE;
	END_IF
	

	
END_PROGRAM

