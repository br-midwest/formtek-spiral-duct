
TYPE
	recipe_init_enum : 
		(
		RCP_INIT_START := 0,
		RCP_INIT_LOAD := 10,
		RCP_INIT_DEFAULTS := 20,
		RCP_INIT_SAVE := 21,
		RCP_INIT_DONE := 99
		);
END_TYPE
