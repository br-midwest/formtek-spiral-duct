
PROGRAM _INIT
	
	MpRecipeXml_0.Enable		:= TRUE;	
	MpRecipeXml_0.FileName		:= ADR(filename);
	MpRecipeXml_0.MpLink		:= ADR(mpRcpPars);
	MpRecipeXml_0.Category		:= ADR(category);
	
	IF gSimulate THEN
		MpRecipeXml_0.DeviceName	:= ADR(FILE_DEV_NAME_SIM);
	ELSE
		MpRecipeXml_0.DeviceName	:= ADR(FILE_DEV_NAME_FPART);
	END_IF
	
	MpRecipeRegPar_0.Enable		:= TRUE;
	MpRecipeRegPar_0.MpLink		:= ADR(mpRcpPars);
	MpRecipeRegPar_0.Category	:= ADR(category);
	MpRecipeRegPar_0.PVName		:= ADR('gMain.par');
	
	MpRecipeRegPar_1.Enable		:= TRUE;
	MpRecipeRegPar_1.MpLink		:= ADR(mpRcpPars);
	MpRecipeRegPar_1.Category	:= ADR(category);
	MpRecipeRegPar_1.PVName		:= ADR('gDev.par');
	
	REPEAT
		MpRecipeRegPar_0();
		MpRecipeRegPar_1();
		MpRecipeXml_0();
		
		CASE step OF			
			RCP_INIT_START: // wait for active
				IF MpRecipeXml_0.Error OR MpRecipeRegPar_0.Error OR MpRecipeRegPar_1.Error THEN
					step	:= RCP_INIT_DONE;
				ELSIF MpRecipeXml_0.Active AND MpRecipeRegPar_0.Active AND MpRecipeRegPar_1.Active THEN 
					step	:= RCP_INIT_LOAD;
				END_IF
			
			RCP_INIT_LOAD: // try to load
				MpRecipeXml_0.Load	:= TRUE;
			
				// if the load fails because the file doesn't exits, create new file
				IF MpRecipeXml_0.Info.Diag.StatusID.ID = mpRECIPE_ERR_LOAD_DATA THEN
					MpRecipeXml_0.Load	:= FALSE;
					MpRecipeXml_0.ErrorReset	:= TRUE;
					step	:= RCP_INIT_DEFAULTS;
				ELSIF MpRecipeXml_0.Error THEN
					MpRecipeXml_0.Load	:= FALSE;
					step	:= RCP_INIT_DEFAULTS;
				ELSIF MpRecipeXml_0.CommandDone THEN
					MpRecipeXml_0.Load	:= FALSE;
					step	:= RCP_INIT_DONE;
				END_IF
			
			RCP_INIT_DEFAULTS: // set default values
				gMain.par.autoCutDelay			:= 1.0;
				gMain.par.autoSlowStartDelay	:= 1.5;
				gMain.par.cutDwell				:= 1.0;
				gMain.par.dischDelay			:= 1.0;
				gMain.par.dischRetractDelay		:= 2.0;
				gMain.par.fhDiam				:= 203.2; // 8 inches
				gMain.par.brakeOnTime			:= 4.0;
				gMain.par.lowSpeedFinishLng		:= 127.0; // 5 inches
				gMain.par.manualLng				:= 2032.0; // 80 inches
				gMain.par.materialGauge			:= GAUGE_HEAVY;
				gMain.par.materialWidth			:= 119.0625;
				
				gMain.par.pref.lengthUnit		:= 2; // imperial
				gMain.par.pref.pressureUnit		:= 1; // psi
				gMain.par.pref.language			:= 0; // english
				
				gDev.par.cifs.ServerDomain		:= 'WORKGROUP';
				gDev.par.cifs.ServerIP			:= '10.9.9.5';
				gDev.par.cifs.ShareName			:= 'LockFormerFiles';
				gDev.par.cifs.ServerUser		:= 'admin';
				gDev.par.cifs.ServerPw			:= '123';
				gDev.par.cifs.DeviceName		:= FILE_DEV_NAME_WINDOWS;
				
				gDev.par.usb.AutoLink			:= FALSE;
				gDev.par.usb.AutoLinkInterval	:= T#3s;
				gDev.par.usb.DeviceName			:= FILE_DEV_NAME_USB;				
				
				step	:= RCP_INIT_SAVE;

			RCP_INIT_SAVE: // save a new copy of the recipe file
				MpRecipeXml_0.Save	:= TRUE;
			
				IF MpRecipeXml_0.Error THEN
					MpRecipeXml_0.Save	:= FALSE;
					step	:= RCP_INIT_DONE;
				ELSIF MpRecipeXml_0.CommandDone THEN
					MpRecipeXml_0.Save	:= FALSE;
					step	:= RCP_INIT_DONE;
				END_IF
				
		 	ELSE
		END_CASE
		UNTIL step = RCP_INIT_DONE
	END_REPEAT

	TON_ParChange.PT	:= T#5s;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	//check whether op has changed any pars. If pars are static for 5 seconds, save the recipe
	par0diff	:= (brsmemcmp(ADR(gMain.par), ADR(oldPars0), SIZEOF(oldPars0)) <> 0);
	par1diff	:= (brsmemcmp(ADR(gDev.par), ADR(oldPars1), SIZEOF(oldPars1)) <> 0);
	
	TON_ParChange.IN	:= (par0diff OR par1diff);
	
	// process commands
	IF EDGEPOS(TON_ParChange.Q) THEN
		MpRecipeXml_0.Save	:= TRUE;
		brsmemcpy(ADR(oldPars0), ADR(gMain.par), SIZEOF(oldPars0)); // update old memory
		brsmemcpy(ADR(oldPars1), ADR(gDev.par), SIZEOF(oldPars1)); // update old memory
		
	ELSIF MpRecipeXml_0.CommandDone OR MpRecipeXml_0.Error THEN
		MpRecipeXml_0.Save	:= FALSE;
		MpRecipeXml_0.Load	:= FALSE;

	END_IF
	
	// if desired, add routine to "load defaults" here
	//IF loadDefaults THEN
	//END_IF
	
	MpRecipeRegPar_0.ErrorReset	:= gMain.cmd.reset;
	MpRecipeRegPar_1.ErrorReset	:= gMain.cmd.reset;
	MpRecipeXml_0.ErrorReset	:= gMain.cmd.reset;
	
	TON_ParChange();
	MpRecipeRegPar_0();
	MpRecipeRegPar_1();
	MpRecipeXml_0();
	 
END_PROGRAM

PROGRAM _EXIT
	
	MpRecipeRegPar_0(Enable := FALSE);
	MpRecipeRegPar_1(Enable := FALSE);
	MpRecipeXml_0(Enable := FALSE);
	 
END_PROGRAM