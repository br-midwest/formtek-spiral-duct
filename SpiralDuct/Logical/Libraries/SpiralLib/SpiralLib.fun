
{REDUND_ERROR} FUNCTION_BLOCK Spiral_Usage (*Calculates material usage for spiral duct production*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		AmtProduced : LREAL;
		Width : REAL;
		Diameter : REAL;
		Reset : BOOL;
	END_VAR
	VAR_OUTPUT
		Usage : LREAL;
		Active : BOOL;
	END_VAR
	VAR
		oldAmtProduced : LREAL;
		change : LREAL;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK
