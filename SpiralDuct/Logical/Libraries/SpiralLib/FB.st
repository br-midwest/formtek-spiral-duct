
(* Calculates material usage for spiral duct production *)
FUNCTION_BLOCK Spiral_Usage
	
	change	:= AmtProduced - oldAmtProduced;
	oldAmtProduced := AmtProduced;
	
	IF Width <> 0.0 AND change > 0.0 THEN
		Usage := ABS((Usage + (change * (spirallibCONST_PI * REAL_TO_LREAL(Diameter) / REAL_TO_LREAL(Width)))));	
		Active := TRUE;
	ELSE
		Active := FALSE;		
	END_IF

	// reset used material log
	IF EDGEPOS(Reset) THEN
		Usage := 0.0;		
	END_IF
	
END_FUNCTION_BLOCK
