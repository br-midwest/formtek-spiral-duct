#define TRUE 1
#define FALSE 0
#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "DS402.h"
#ifdef __cplusplus
	};
#endif

/*Structure type declaration*/
DS402BasicControlnputType ControlIN;
DS402BasicControlOutputType ControlOUT;
DS402BasicStateEnum	State;

void DS402Basic(struct DS402Basic* inst)
{
	/***************************************************************INPUT HANDLING PART*******************************************************************/
	/**********In the logic part of a FB, it are not used directly inputs/outputs but variables located in the ControlIN / ControlOUT structures**********/
	/****************************************************Assign inputs of FB to ControlIN structure*******************************************************/
	ControlIN.Command.MoveVelocity			=inst->MoveVelocity;
	ControlIN.Command.Stop					=inst->Stop;
	ControlIN.Command.SwitchOn				=inst->SwitchOn;
	ControlIN.Command.ErrorAcknowledge		=inst->ErrorAcknowledge;
	ControlIN.Command.Enable				=inst->Enable;
	ControlIN.Status.ModuleOk				=inst->ModuleOk;
	ControlIN.Status.StatusWord				=inst->StatusWord;
	ControlIN.Command.Update				=inst->Update;
	ControlIN.Parameters.Direction 			=inst->Direction;
	ControlIN.Parameters.Velocity 			=inst->Velocity;
	/********If is "MoveVelocity" or "Update" command, assigning ControlIN parameters to ControlOUT parameters (only parameters that need an update)*************/
	if ((ControlIN.Command.Update > ControlIN.Command.UpdateOld) || (ControlIN.Command.MoveVelocity > ControlIN.Command.MoveVelocityOld  ))
	{
		ControlOUT.Status.SetVelocity		=ControlIN.Parameters.Velocity;
		ControlOUT.Status.SetDirection		=ControlIN.Parameters.Direction;
	}
	/***************************************************************STATE MACHINE PART*******************************************************************/
	/**********for more details regarding state machine(states number, command numbers),please check the DS 402 library documentation********************/
	switch (State)
	{
		case	ds402BASIC_ERROR:		
			ControlOUT.Status.Error						=TRUE;
	/*********************To acknowledge the error, the rising edge is required (fault acknowledge command in CiA 402- 128)**************************/
			if(ControlIN.Command.ErrorAcknowledge>ControlIN.Command.ErrorAcknowledgeOld)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ERROR_ACK;	
			}
			else
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
	/******************if the error disappears then the state changes depending on the state of drive********************************/
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_SWITCHED_ON_STATUS) && ControlIN.Status.ModuleOk) 
			{
				ControlOUT.Status.Error					=FALSE;
				State									=ds402BASIC_SWITCHED_ON;
			}
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS) && ControlIN.Status.ModuleOk)
			{
				ControlOUT.Status.Error					=FALSE;
				State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS)  && ControlIN.Status.ModuleOk)
			{
				ControlOUT.Status.Error					=FALSE;
				State									=ds402BASIC_WAIT_HW;
			}	
			break;
		
		case ds402BASIC_WAIT_HW:
		/***********************************The "Active" output is TRUE only if the FB is enabled and ready to switch on**********************************/
			ControlOUT.Status.Active					=FALSE;	
		/**************Check if the drive is in the "SWITCH_ON_DISABLED" state, then command ds402BASIC_CMD_ENABLE_VOLTAGE(6) can be used******************/	
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS) && ControlIN.Command.Enable) 
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ENABLE_VOLTAGE;	
			}
		/****************Check if after command ds402BASIC_CMD_ENABLE_VOLTAGE(6),drive changed to "READY_TO_SWITCH_ON" state ******************************/
			if ( ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS) && ControlIN.Command.Enable && ControlIN.Status.ModuleOk )
			{
				State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
			break;
			
		case ds402BASIC_READY_TO_SWITCH_ON:
		/***************In this state, the drive is ready for operation and FB is enabled - "Active"(FB output) flag is set to TRUE.**********************/	
			ControlOUT.Status.Active					=TRUE;
		/***************If the user wants to disable the FB, a negative edge is required (FB "EnableE input) then the "Disable" command is given**********/	
			if(	ControlIN.Command.Enable<ControlIN.Command.EnableOld )
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
		/*****************Check if after command ds402BASIC_CMD_DISABLE(0),drive changed to "ds402BASIC_WAIT_HW_STATUS" state ******************************/
			if (ControlIN.Command.Enable==0 && (inst->StatusWord | ds402BASIC_BIT_MASK_ONE)	== ds402BASIC_WAIT_HW_STATUS)
			{
				State									= ds402BASIC_WAIT_HW;		
			}
		/**********************To switch on, the rising egde is required, then ds402BASIC_CMD_SWITCH_ON(7) command is given*********************************/
			if(ControlIN.Command.SwitchOn > ControlIN.Command.SwitchOnOld )
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_SWITCH_ON;	
			}
		/***************Check if after command ds402BASIC_CMD_SWITCH_ON(7),drive changed to "ds402BASIC_SWITCHED_ON_STATUS" state ***************************/
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_SWITCHED_ON_STATUS)
			{
				State									= ds402BASIC_SWITCHED_ON;
			}
			break;
		
		case ds402BASIC_SWITCHED_ON:
		/***************If the user wants to disable the FB, a negative edge is required (FB "EnableE input) then the "Disable" command is given**********/	
			if(	ControlIN.Command.Enable<ControlIN.Command.EnableOld)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
		/****************Check if after command Enable =FALSE,drive changed to ds402BASIC_WAIT_HW_STATUS state-and then a change of state *******************/	
			if (ControlIN.Command.Enable==0 && (inst->StatusWord | ds402BASIC_BIT_MASK_ONE)	== ds402BASIC_WAIT_HW_STATUS)
			{
				State									=ds402BASIC_WAIT_HW;
			}
		/**********************To switch off, the negative egde is required, then ds402BASIC_CMD_SWITCH_ON(7) command is given*********************************/
			if(ControlIN.Command.SwitchOn < ControlIN.Command.SwitchOnOld)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ENABLE_VOLTAGE;
			}
		/******Check if after command ds402BASIC_CMD_ENABLE_VOLTAGE(6),drive changed to ds402BASIC_READY_TO_SWITCH_ON state-and then a change of state *******/
			if ( (inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS)
			{
				State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
		
		/***If there is a rising edge at the "MoveVelocity" command, then the direction of motion and speed is assigned to output (no update needed in this case) and MoveVelocity command is given***/
			if(ControlIN.Command.MoveVelocity > ControlIN.Command.MoveVelocityOld)
			{	
				if (ControlIN.Parameters.Direction ==FALSE)
				{
					ControlOUT.Status.ControlWord		=ds402BASIC_CMD_RUN_POSITIVE;
				}
				if (ControlIN.Parameters.Direction ==TRUE)
				{
					ControlOUT.Status.ControlWord		= ds402BASIC_CMD_RUN_NEGATIVE;
				}
			}
		/***Check if after MoveVelocity command (ds402BASIC_CMD_RUN_POSITIVE (15) or ds402BASIC_CMD_RUN_NEGATIVE(2063)),drive changed to "ds402BASIC_ENABLED_STATUS" state ***/
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_ENABLED_STATUS )
			{
				State									=ds402BASIC_OPERATION_ENABLED;
			}		
			break;
		
		case ds402BASIC_OPERATION_ENABLED:
			
		/*************************Change of command depending on selected direction of movement(rising edge on the "update" command is required)*******************************/	
			if ((ControlIN.Command.Update > ControlIN.Command.UpdateOld) && ControlIN.Parameters.Direction==0)
			{
				ControlOUT.Status.ControlWord			=ds402BASIC_CMD_RUN_POSITIVE;
			}
			if ((ControlIN.Command.Update > ControlIN.Command.UpdateOld) && ControlIN.Parameters.Direction==1)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_RUN_NEGATIVE;
			}
		/*********************Check if after command "Enable" =FALSE,drive changed to ds402BASIC_WAIT_HW_STATUS state-and then a change of state ******************************/			
			if(	ControlIN.Command.Enable<ControlIN.Command.EnableOld )
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS)
			{
				State									=ds402BASIC_WAIT_HW;
			}
		/************Check if after command ds402BASIC_CMD_ENABLE_VOLTAGE(6),drive changed to ds402BASIC_READY_TO_SWITCH_ON state-and then a change of state ******************/			
			if(ControlIN.Command.SwitchOn < ControlIN.Command.SwitchOnOld)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ENABLE_VOLTAGE;
			}
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS)
			{
				State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
		/************Check if after command ds402BASIC_CMD_SWITCH_ON (7),drive changed to ds402BASIC_SWITCHED_ON state-and then a change of state ******************************/				
			if(ControlIN.Command.MoveVelocity<ControlIN.Command.MoveVelocityOld)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_SWITCH_ON;
			}
			if ((inst->StatusWord |  ds402BASIC_BIT_MASK_TWO) == ds402BASIC_SWITCHED_ON_STATUS)
			{
				State									= ds402BASIC_SWITCHED_ON;
			}
		/************Check if after command QUICK STOP COMMAND (2),drive changed to ds402BASIC_QUICK_STOP_ACTIVE state-and then a change of state ******************************/	
			if (ControlIN.Command.Stop>ControlIN.Command.StopOld)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_QUICK_STOP;
			}
			if ((inst->StatusWord |  ds402BASIC_BIT_MASK_TWO) == ds402BASIC_QUICK_STOP_STATUS)
			{
				ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;
				State									=ds402BASIC_QUICK_STOP_ACTIVE;
			}
			break;
		
		case ds402BASIC_QUICK_STOP_ACTIVE:
		/*********************************************Check if the drive is stopped - then return to the ENABLE status*************************************************************/
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS) 
			{
				State									=ds402BASIC_WAIT_HW;	
			}	
			break;
	}
	/***************************************************************OUTPUT HANDLING PART**************************************************************/
	/************************************************************Check if drive is in error state*****************************************************/
	if (((inst->StatusWord | ds402BASIC_BIT_MASK_ONE)	== ds402BASIC_ERROR_STATUS) || (ControlIN.Status.ModuleOk==FALSE))
	{
		State											=ds402BASIC_ERROR;	
	}	
	/***********************************Check if drive is ReadyToSwitchOn and SwitchedOn -then set outputs*************************************************/
	if (((inst->StatusWord | ds402BASIC_BIT_MASK_TWO)== ds402BASIC_READY_SWITCH_STATUS) && ControlIN.Command.Enable)
	{
		ControlOUT.Status.SwitchedOn					=FALSE;
		ControlOUT.Status.ReadyToSwitchOn				=TRUE;
	}
	else if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO)== ds402BASIC_SWITCHED_ON_STATUS)
	{
		ControlOUT.Status.SwitchedOn					=TRUE;
		ControlOUT.Status.ReadyToSwitchOn				=FALSE;
	}
	else if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO)== ds402BASIC_ENABLED_STATUS )
	{
		ControlOUT.Status.ReadyToSwitchOn				=FALSE;
		ControlOUT.Status.SwitchedOn					=TRUE;
	}
	else
	{
		ControlOUT.Status.ReadyToSwitchOn				=FALSE;
		ControlOUT.Status.SwitchedOn					=FALSE;
	}
	/******************************Set of FB status outputs depending on the ACOPOSInverter status**********************************/
	/******************************************Check if set point reached************************************************************/
	if ((inst->StatusWord | ds402BASIC_BIT_MASK_SET_POINT)	== ds402BASIC_ENABLED_STATUS )
	{
		ControlOUT.Status.SetVelocityReached			=TRUE;
	}
	else
	{
		ControlOUT.Status.SetVelocityReached			=FALSE;
	}
	/*******************************************Check if motor is moving**************************************************************/
	if (State==ds402BASIC_OPERATION_ENABLED)
	{
		ControlOUT.Status.InMotion						=TRUE;
	}
	else
	{
		ControlOUT.Status.InMotion						=FALSE;
	}
	/*************************************Assign ControlOUT structure to FB output **************************************************/
	inst->Active										=ControlOUT.Status.Active;
	inst->InMotion										=ControlOUT.Status.InMotion;
	inst->ControlWord									=ControlOUT.Status.ControlWord;
	inst->Error											=ControlOUT.Status.Error;
	inst->SetVelocityReached							=ControlOUT.Status.SetVelocityReached;
	inst->ReadyToSwitchOn								=ControlOUT.Status.ReadyToSwitchOn;
	inst->SwitchedOn									=ControlOUT.Status.SwitchedOn;
	inst->StatusID										=State;
	inst->SetVelocity									=ControlOUT.Status.SetVelocity;	
	inst->SetDirection									=ControlOUT.Status.SetDirection;
	/******************* If Enable = FALSE then all FB outputs are set to FALSE (Library guideline)******************/
	if (ControlOUT.Status.Active==FALSE)
	{

		ControlOUT.Status.InMotion						=FALSE;
		ControlOUT.Status.Error							=FALSE;
		ControlOUT.Status.SetVelocityReached			=FALSE;
		ControlOUT.Status.ReadyToSwitchOn				=FALSE;
		ControlOUT.Status.SwitchedOn					=FALSE;
		ControlOUT.Status.SetVelocity					=FALSE;	
		ControlOUT.Status.SetDirection					=FALSE;
	}
	
	/********************************************Edge detection*******************************************************/
	ControlIN.Command.EnableOld							=ControlIN.Command.Enable;
	ControlIN.Command.ErrorAcknowledge					=ControlIN.Command.ErrorAcknowledgeOld;
	ControlIN.Command.MoveVelocityOld					=ControlIN.Command.MoveVelocity;
	ControlIN.Command.StopOld							=ControlIN.Command.Stop;
	ControlIN.Command.SwitchOnOld						=ControlIN.Command.SwitchOn;
	ControlIN.Parameters.DirectionOld					=ControlIN.Parameters.Direction;
	ControlIN.Command.UpdateOld 						=ControlIN.Command.Update;
	
	
	
}
