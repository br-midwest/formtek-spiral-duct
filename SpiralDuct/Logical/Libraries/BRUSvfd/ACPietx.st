
(* Returns string with ACPi error text *)
FUNCTION ACPietx
	
	IF pErrStr = 0 THEN
		ACPietx := FALSE;
		RETURN;
	ELSE		
		errStr ACCESS pErrStr;
		CASE ACPiModel OF
			brusACPi_S44:
				// S44 faults list
				CASE LFT_Input OF
					0:	errStr := 'nOF - No Fault Present'; 
					1:	errStr := 'InF - Calibration Error';	
					3:	errStr := 'CFF - Incorrect Configuration';	
					4:	errStr := 'CFI - Invalid Configuration';	
					5:	errStr := 'SLF1 - ModBus Communication Fault';	
					9:	errStr := 'OCF - Over Current Fault';	
					10: errStr := 'CrF1 - Precharge Fault';	
					16: errStr := 'OHF - Drive Overheat';	
					17: errStr := 'OLF - Motor Overload';	
					18: errStr := 'ObF - Overbraking';	
					19: errStr := 'OSF - Mains Overvoltage';	
					20: errStr := 'OPF1 - 1 Output Phase Loss';	
					21: errStr := 'PHF - Input Phase Loss';	
					22: errStr := 'USF - Undervoltage';	
					23: errStr := 'SCF1 - Motor Short Circuit';	
					24: errStr := 'SOF - Overspeed';	
					25: errStr := 'tnF - Auto-tuning Fault';	
					26: errStr := 'InF1 - Rating Error';	
					27: errStr := 'InF2 - Power Calibration';	
					28: errStr := 'InF3 - Internal Serial Link';	
					29: errStr := 'InF4 - Internal Mfg area';	
					32: errStr := 'SCF3 - Ground Short Circuit';	
					33: errStr := 'OPF2 - 3 Output Phase Loss';	
					42: errStr := 'SLF2 - PowerSuite Communication';	
					45: errStr := 'SLF3 - HMI Communication Fault';	
					51: errStr := 'InF9 - Internal I measure';	
					53: errStr := 'InFb - Internal th. sensor';	
					54: errStr := 'tJF - IGBT Overheat';	
					55: errStr := 'SCF4 - IGBT Short Circuit';	
					56: errStr := 'SCF5 - Motor Short Circuit';	
					69: errStr := 'InFE - Internal CPU Fault';	
					77: errStr := 'CFI2';	
					100: errStr := 'ULF - Underload Fault';	
					101: errStr := 'OLC - Overload Fault';	
					106: errStr := 'LFF1';	
					253: errStr := 'XXXX';
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
		
			brusACPi_X64:
				// X64 faults list
				CASE LFT_Input OF
					0: errStr := 'nOF - No Fault Code';	
					3: errStr := 'CFF - Incorrect Configuration';	
					4: errStr := 'CFI - Invalid Configuration';	
					5: errStr := 'SLF - Communication Fault';	
					6: errStr := 'ILF - Internal Communication Interuption';	
					7: errStr := 'CnF - Communication Option Card';	
					8: errStr := 'EPF - External Fault';	
					9: errStr := 'OCF - Overcurrent';	
					10: errStr := 'CrF - Capacitor pre-charge';	
					13: errStr := 'LFF - 4 - 20 mA loss';	
					16: errStr := 'OHF - Drive Overheating';	
					17: errStr := 'OLF - Motor Overload';	
					18: errStr := 'ObF - DC Bus Overvoltage';	
					19: errStr := 'OSF - Line Supply Overvoltage';	
					20: errStr := 'OPF - Motor Phase Lose';	
					21: errStr := 'PHF - Line Phase Loss';	
					22: errStr := 'USF - Line Supply Undervoltage';	
					23: errStr := 'OCF - Motor Short Circuit (phase to phase)';	
					24: errStr := 'SOF - Motor Overspeed';	
					25: errStr := 'tnF - Auto-tuning was unsuccessful';	
					26: errStr := 'IF1 - Unknow rating';	
					27: errStr := 'IF2 - MMI card';	
					28: errStr := 'IF3 - MMI communication';	
					29: errStr := 'IF4 - Industrial EEPROM memory';	
					30: errStr := 'EEF - EEPROM memory';	
					31: errStr := 'OCF - Impeding Short Circuit';	
					32: errStr := 'SCF - Motor Short Circuit (to ground)';	
					33: errStr := 'OPF - Motor Phase Loss - 3 phases';	
					34: errStr := 'COF - Communication Interuption';	
					35: errStr := 'bLF - Brake Control';	
					36: errStr := 'OCF - Power Module on 15 kW drive';	
					55: errStr := 'SCF - Power Module or Motor Short Circuit';
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
		
			brusACPi_P74:
				// P74 faults list
				CASE LFT_Input OF
					0 : errStr := 'nOF - No Fault';	
					1 : errStr := 'InF - Calibration Error';	
					2 : errStr := 'EEF1 - Control EEPROM';	
					3 : errStr := 'CFF - Incorrect Configuration';	
					4 : errStr := 'CFI - Invalid Configuration';	
					5 : errStr := 'SLF1 - Communication Error';	
					6 : errStr := 'ILF - Internal Communication Link';	
					7 : errStr := 'CnF - Communication Network';	
					8 : errStr := 'EPF1 - External Fault';	
					9 : errStr := 'OCF - Overcurrent';	
					10 : errStr := 'CrF - Precharge';	
					11 : errStr := 'SPF - Speed Feedback loss';
					16 : errStr := 'OHF - Drive Overheat';	
					17 : errStr := 'OLF - Motor Overload';	
					18 : errStr := 'ObF - Overbraking';	
					19 : errStr := 'OSF - Mains Overvoltage';	
					20 : errStr := 'OPF1 - 1 Output Phase Loss';	
					21 : errStr := 'PHF - Input Phase Loss';	
					22 : errStr := 'USF - Undervoltage';	
					23 : errStr := 'SCF1 - Motor Short Circuit';	
					24 : errStr := 'SOF - Overspeed';	
					25 : errStr := 'tnF - Auto-tuning Unsuccessful';	
					26 : errStr := 'InF1 - Rating Error';
					27 : errStr := 'InF2 - Power Calibration';	
					28 : errStr := 'InF3 - Internal Serial Link';	
					29 : errStr := 'InF4 - Internal Mfg Area';	
					30 : errStr := 'EEF2 - Power EEPROM';	
					31 : errStr := 'SCF2 - Impedant Short Circuit';	
					32 : errStr := 'SCF3 - Ground Short Circuit';	
					33 : errStr := 'OPF2 - 3 Phase Output Loss';	
					34 : errStr := 'COF - Powerlink Communication';	
					35 : errStr := 'bLF - Brake Control';
					38 : errStr := 'EPF2 - External Fault Com.';
					41 : errStr := 'brF - Brake Feedback';	
					42 : errStr := 'SLF2 - PowerSuite Communication.';	
					43 : errStr := 'ECF - Encoder Coupling';	
					44 : errStr := 'SSF - Torque Current Limit';	
					45 : errStr := 'SLF3 - HMI com.';	
					46 : errStr := 'PrF - Power Removal';
					49 : errStr := 'PtFL - LI6=PTC Probe';	
					50 : errStr := 'OtFL - PTC Fault';	
					51 : errStr := 'InF9 - Internal I measure';	
					52 : errStr := 'InFA - Internal Mains Circuit';	
					53 : errStr := 'InFb - Internal th. Sensor';	
					54 : errStr := 'tJF - IGBT Overheat';	
					55 : errStr := 'SCF4 - IGBT Short Circuit';	
					56 : errStr := 'SCF5 - Motor Short Circuit';	
					57 : errStr := 'SrF - Torque Time-out';	
					58 : errStr := 'FCF1 - Output Contact Stuck';	
					59 : errStr := 'FCF2 - Output Contact Open';	
					61 : errStr := 'AI2F - AI2 Input';	
					64 : errStr := 'LCF - Input Contactor';	
					66 : errStr := 'dCF - Iff. I Fault';	
					67 : errStr := 'HdF - IGBT  Desaturation';	
					68 : errStr := 'InF6 - Internal Option';	
					69 : errStr := 'InFE - Internal CPU';	
					71 : errStr := 'LFF3 - AI3 4-20 mA Loss';	
					73 : errStr := 'HCF - Cards Pairing';	
					76 : errStr := 'dLF - Load Fault';	
					77 : errStr := 'CFI2 - Bad Configuration';	
					99 : errStr := 'CSF - Ch. SW. Fault';	
					100 : errStr := 'ULF - Pr. Underload Fault';	
					101 : errStr := 'OLC - Proc. Overload Fault';	
					105 : errStr := 'ASF - Angle Error';	
					107 : errStr := 'SAFF - Safety Fault';	
					108 : errStr := 'FbE - FB Fault';	
					109 : errStr := 'FbES - FB Stop Fault';
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
		
			brusACPi_P84:
				// P84 faults list
				CASE LFT_Input OF
					0: errStr := 'nOF - No Fault';	
					1: errStr := 'InF - Calibration Error';	
					2: errStr := 'EEF1 - Control EEPROM';	
					3: errStr := 'CFF - Incorrect Configuration';	
					4: errStr := 'CFI - Invalid Configuration';	
					5: errStr := 'SLF1 - Communication Error';	
					6: errStr := 'ILF - Internal Communication Link';	
					7: errStr := 'CnF - Communication Network';	
					8: errStr := 'EPF1 - External Fault';	
					9: errStr := 'OCF - Overcurrent';	
					10: errStr := 'CrF - Precharge';	
					11: errStr := 'SPF - Speed Feedback loss';	
					12: errStr := 'AnF - Load Slipping';	
					13: errStr := 'LFF2 - AI2 4-20 mA Loss';	
					14: errStr := 'PtF1 - PTC1 Probe';	
					15: errStr := 'OtF1 - PTC1 Overheat';	
					16: errStr := 'OHF - Drive Overheat';	
					17: errStr := 'OLF - Motor Overload';	
					18: errStr := 'ObF - Overbraking';	
					19: errStr := 'OSF - Mains Overvoltage';	
					20: errStr := 'OPF1 - 1 Output Phase Loss';	
					21: errStr := 'PHF - Input Phase Loss';	
					22: errStr := 'USF - Undervoltage';	
					23: errStr := 'SCF1 - Motor Short Circuit';	
					24: errStr := 'SOF - Overspeed';	
					25: errStr := 'tnF - Auto-tuning Unsuccessful';	
					26: errStr := 'InF1 - Rating Error';	
					27: errStr := 'InF2 - Power Calibration';	
					28: errStr := 'InF3 - Internal Serial Link';	
					29: errStr := 'InF4 - Internal Mfg Area';	
					30: errStr := 'EEF2 - Power EEPROM';
					31: errStr := 'SCF2 - Impedant Short Circuit';	
					32: errStr := 'SCF3 - Ground Short Circuit';	
					33: errStr := 'OPF2 - 3 Phase Output Loss';	
					34: errStr := 'COF - Powerlink Communication';	
					35: errStr := 'bLF - Brake Control';	
					37: errStr := 'InF7 - Internal Hard Init';	
					38: errStr := 'EPF2 - External Fault Com.';	
					39: errStr := 'APF - Application Fault';	
					40: errStr := 'InF8 - Internal Ctrl Supply';	
					41: errStr := 'brF - Brake Feedback';	
					42: errStr := 'SLF2 - PowerSuite Communication.';	
					43: errStr := 'ECF - Encoder Coupling';	
					44: errStr := 'SSF - Torque Current Limit';	
					45: errStr := 'SLF3 - HMI com.';	
					46: errStr := 'PrF - Power Removal';	
					47: errStr := 'PtF2 - PTC2 Probe';	
					48: errStr := 'OtF2 - PTC2 Overheat';	
					49: errStr := 'PtFL - LI6=PTC Probe';	
					50: errStr := 'OtFL - PTC Fault';	
					51: errStr := 'InF9 - Internal I measure';	
					52: errStr := 'InFA - Internal Mains Circuit';	
					53: errStr := 'InFb - Internal th. Sensor';	
					54: errStr := 'tJF - IGBT Overheat';	
					55: errStr := 'SCF4 - IGBT Short Circuit';	
					56: errStr := 'SCF5 - Motor Short Circuit';	
					57: errStr := 'SrF - Torque Time-out';	
					58: errStr := 'FCF1 - Output Contact Stuck';	
					59: errStr := 'FCF2 - Output Contact Open';	
					60: errStr := 'InFC - Int. T Measurement';	
					61: errStr := 'AI2F - AI2 Input';	
					62: errStr := 'EnF - Encoder';	
					63: errStr := 'CrF2 - Thyr. Soft Charge';	
					64: errStr := 'LCF - Input Contactor';	
					65: errStr := 'bUF - DB Unit Short Circuit';	
					66: errStr := 'dCF - Iff. I Fault';
					67: errStr := 'HdF - IGBT  Desaturation';	
					68: errStr := 'InF6 - Internal Option';	
					69: errStr := 'InFE - Internal CPU';	
					70: errStr := 'bOF - BR Overload';	
					71: errStr := 'LFF3 - AI3 4-20 mA Loss';	
					72: errStr := 'LEF4 - AI4 4-20 mA Loss';	
					73: errStr := 'HCF - Cards Pairing';
					76: errStr := 'dLF - Load Fault';	
					99: errStr := 'CSF - Ch. SW. Fault';
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
		
			brusACPi_P66:
				// P66 faults list
				CASE LFT_Input OF
					0: errStr := 'No Error Saved';
					2: errStr := 'EEPROM error';
					3: errStr := 'Invalid configuration during startup';
					4: errStr := 'Incorrect parameter configuration';
					5: errStr := 'Local serial Modbus error';
					6: errStr := 'Option internal comms error';
					7: errStr := 'Option NET internal comms error';
					8: errStr := 'External error via LI';
					9: errStr := 'Overcurrent error';
					10: errStr := 'Charging relay error';
					11: errStr := 'Speed encoder response loss';
					12: errStr := 'Direction error';
					16: errStr := 'Frequency inverter overheat';
					17: errStr := 'Motor overload error';
					18: errStr := 'Over braking error';
					19: errStr := 'Over supply error';
					20: errStr := 'Motor 1 phase loss';
					21: errStr := 'Main input 1 phase loss';
					22: errStr := 'Undervoltage error';
					23: errStr := 'Motor short-circuit error';
					24: errStr := 'Overspeed error';
					25: errStr := 'Tuning fault';
					26: errStr := 'Unspecified frequency inverter assessment';
					27: errStr := 'Unspecified or incompatible power board';
					28: errStr := 'Internal serial communications error';
					29: errStr := 'Invalid industrialization zone';
					30: errStr := 'EEPROM power error';
					32: errStr := 'Direct ground short circuit error';
					33: errStr := 'Motor 3 phase loss';
					34: errStr := 'CANopen comms error';
					35: errStr := 'Braking motor 3-phase loss';
					38: errStr := 'External error from comms board';
					41: errStr := 'Braking contactor error';
					42: errStr := 'Power suite comms error';
					44: errStr := 'Torque current limiting error';
					45: errStr := 'Remote control panel comms error';
					49: errStr := 'PtLC error';
					50: errStr := 'Motor overheat';
					51: errStr := 'Current measurement loop error';
					52: errStr := 'Input phase failture error';
					53: errStr := 'Thermosensor error';
					54: errStr := 'IGBT overheat';
					55: errStr := 'IGBT short-circuit error';
					56: errStr := 'Load short-circuit error';
					58: errStr := 'Output contactor engaged contactor';
					59: errStr := 'Output contactor Open-ended contactor';
					64: errStr := 'Line contactor failure';
					67: errStr := 'Hardware error';
					68: errStr := 'Unspecified or incompatible option board';
					69: errStr := 'CPU error';
					71: errStr := 'AI3 4-20mA error';
					73: errStr := 'Hardware configuration error';
					76: errStr := 'Dynamic load error';
					77: errStr := 'Configuration transmission error';
					99: errStr := 'Channel charge error';
					100: errStr := 'Torque underload error';
					101: errStr := 'Torque overload error';
					105: errStr := 'Angle setting error';
					107: errStr := 'Safety functions error';
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
		
			brusACPi_P76:	
				// P76 faults list
				CASE LFT_Input OF
					
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
				
			brusACPi_P86:
				// P86 faults list
				CASE LFT_Input OF
					
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
				
			brusACPi_P96:
			// P96 faults list
				CASE LFT_Input OF
					
					ELSE
						errStr := 'Unrecognized error number';
				END_CASE
			
		END_CASE
		
		ACPietx := TRUE;
	END_IF
	
END_FUNCTION
