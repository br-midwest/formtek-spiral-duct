
{REDUND_ERROR} FUNCTION ACPietx : BOOL (*Returns string with ACPi error text*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		LFT_Input : INT; (*LFT_Input in the ACPi I/O mapping*)
		ACPiModel : ACPi_Model_enum;
		pErrStr : UDINT; (*Address of a *)
	END_VAR
	VAR
		errStr : REFERENCE TO STRING[80];
	END_VAR
END_FUNCTION
