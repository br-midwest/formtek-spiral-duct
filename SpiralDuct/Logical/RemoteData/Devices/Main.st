
PROGRAM _INIT
	
	CIFSDevChk_0.Enable	:= TRUE;
	USBDevChk_0.Enable	:= TRUE;

END_PROGRAM

PROGRAM _CYCLIC
	
	// local file device always available
	gDev.sts.devicesStatus[FILE_DEVICE_LOCAL]	:= DEV_OK;
	
	// check for USB sticks
	USBDevChk_0.Parameters	:= gDev.par.usb;
	USBDevChk_0.Link		:= gDev.cmd.reqRemoteDevice AND NOT(gDev.cmd.refresh);
	USBDevChk_0.Eject		:= NOT(gDev.cmd.reqRemoteDevice) AND NOT(gDev.sts.operationInProgress);
	USBDevChk_0.ErrorReset	:= gMain.cmd.reset OR gDev.cmd.refresh OR NOT(gDev.cmd.reqRemoteDevice);
	USBDevChk_0();
	
	IF USBDevChk_0.Error OR NOT(USBDevChk_0.Ready) THEN
		gDev.sts.devicesStatus[FILE_DEVICE_USB]	:= DEV_NOT_OK;
	ELSE
		gDev.sts.devicesStatus[FILE_DEVICE_USB]	:= DEV_OK;
	END_IF
	
	// check for the Windows share
	CIFSDevChk_0.Parameters	:= gDev.par.cifs;
	CIFSDevChk_0.Link		:= gDev.cmd.reqRemoteDevice AND NOT(gDev.cmd.refresh);
	CIFSDevChk_0.Eject		:= NOT(gDev.cmd.reqRemoteDevice) AND NOT(gDev.sts.operationInProgress);
	CIFSDevChk_0.ErrorReset	:= gMain.cmd.reset OR gDev.cmd.refresh OR NOT(gDev.cmd.reqRemoteDevice);
	CIFSDevChk_0();

	IF CIFSDevChk_0.Error OR NOT(CIFSDevChk_0.Ready) THEN
		gDev.sts.devicesStatus[FILE_DEVICE_WINDOWS]	:= DEV_NOT_OK;
	ELSE
		gDev.sts.devicesStatus[FILE_DEVICE_WINDOWS]	:= DEV_OK;
	END_IF
	
	// select device
	CASE gDev.sts.selectedDevice OF
		FILE_DEVICE_LOCAL:
			IF gSimulate THEN
				gDev.sts.deviceName	:= FILE_DEV_NAME_SIM;
			ELSE
				gDev.sts.deviceName	:= FILE_DEV_NAME_FPART;
			END_IF
			
		FILE_DEVICE_WINDOWS:
			gDev.sts.deviceName	:= FILE_DEV_NAME_WINDOWS;
			
			IF gDev.sts.devicesStatus[FILE_DEVICE_WINDOWS] = DEV_NOT_OK THEN
				gDev.sts.selectedDevice	:= FILE_DEVICE_LOCAL;
			END_IF	
		
		FILE_DEVICE_USB:
			gDev.sts.deviceName	:= FILE_DEV_NAME_USB;
		
			IF gDev.sts.devicesStatus[FILE_DEVICE_USB] = DEV_NOT_OK THEN
				gDev.sts.selectedDevice	:= FILE_DEVICE_LOCAL;
			END_IF
		
	END_CASE
	
END_PROGRAM

PROGRAM _EXIT
  

   
END_PROGRAM
      