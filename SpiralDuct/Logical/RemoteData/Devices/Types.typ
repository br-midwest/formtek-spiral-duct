
TYPE
	device_step_enum : 
		(
		DEVICE_IDLE,
		DEVICE_SET_PARS,
		DEVICE_LINK,
		DEVICE_READY,
		DEVICE_UNLINK,
		DEVICE_ERROR
		);
END_TYPE
