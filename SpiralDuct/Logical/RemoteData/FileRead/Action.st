
ACTION ParseFile:
	
	CASE parseStep OF 
		FILE_PARSE_CONVERT:
			//convert the string to an array of ASCII chars					
			memcpy(ADR(TempUsint), ADR(TempString), UINT_TO_UDINT(strlen(ADR(TempString))));
					
			newLineCount := 0;
			parseStep := FILE_PARSE_PARSING;
				
		FILE_PARSE_PARSING:
			FOR char := 0 TO strlen(ADR(TempString)) DO
				currentChar := TempUsint[char];
						
				IF TempUsint[char] = ASCII_010_NEWLINE THEN
					newLineCount := newLineCount + 1;
				END_IF
						
				IF (newLineCount >= 2) AND TempUsint[char] <> ASCII_010_NEWLINE THEN
					IF TempUsint[char] <> ASCII_044_COMMA AND TempUsint[char] <> ASCII_032_SPACE THEN;
						usintArrayVari[i] := TempUsint[char];
						i := i + 1;
					ELSIF TempUsint[char] = ASCII_044_COMMA OR (TempUsint[char] = ASCII_010_NEWLINE AND commaCount >= 1) THEN
						memcpy(ADR(stringVari), ADR(usintArrayVari), 10);
						TempRecipe[(newLineCount-2)].TempSequence[commaCount] := STRING_TO_REAL(stringVari);
						FOR j := 0 TO 10 DO 
							usintArrayVari[j] := 0;
						END_FOR
								
						commaCount := commaCount + 1;
						i := 0;
								
					END_IF
				END_IF
						
				IF (newLineCount >= 2) AND commaCount >= 1 AND TempUsint[char] = ASCII_010_NEWLINE THEN
					memcpy(ADR(stringVari), ADR(usintArrayVari), 10);
					TempRecipe[(newLineCount-3)].TempSequence[commaCount] := STRING_TO_REAL(stringVari);
					FOR j := 0 TO 10 DO 
						usintArrayVari[j] := 0;
					END_FOR
								
					commaCount := 0;
					i := 0;
				END_IF
						
				IF char = strlen(ADR(TempString)) THEN
					memcpy(ADR(stringVari), ADR(usintArrayVari), 10);
					TempRecipe[(newLineCount-2)].TempSequence[commaCount] := STRING_TO_REAL(stringVari);
					FOR j := 0 TO 10 DO 
						usintArrayVari[j] := 0;
					END_FOR
								
					commaCount := 0;
					i := 0;
				END_IF
						
			END_FOR						
					
			IF char >= strlen(ADR(TempString)) THEN
				newLineCount := 0;
				commaCount := 0;
				i := 0;
				parseStep := FILE_PARSE_SUCCESS;
			END_IF
	END_CASE
	
END_ACTION
