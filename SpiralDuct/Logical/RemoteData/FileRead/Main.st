(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Author:	Brad Jones
 * Created:	2018/11:17 
*********************************************************
[PROGRAM INFORMATION]
This program is responsible for loading in job information
from a formatted (csv) text file.
Based on the shareMode, it will connect to either a USB 
flash drive plugged into the controller, or a Windows
shared folder on the same network as the PLC.

The following is an example of the text file that is received from the third party software: 

Spiral Test
981723987
1, 26.000, 16.00, 0.00, 60.00
1, 28.000, 14.00, 0.00, 60.00 

There are two lines that precede the tube measurements, with the project name (eg Spiral Test) AND account number) 
The numbers correspond TO the following physical quantities: Quantity, Gauge, Diameter, Width, Length 
The width value is used in oval spirals, otherwise it is 0.00. This allows FOR the computation FOR the oval round 
This file needs TO be parsed AND passed into the corresponding variabales/arrays 
NOTE: File's new line contains a carriage return ($r) and a line feed ($n) 
********************************************************)

PROGRAM _INIT
	
END_PROGRAM

PROGRAM _CYCLIC

	// Read in the job data from the shared text file
	CASE file.step OF
		FILE_WAIT:
			IF gMain.cmd.loadRemote THEN
				gMain.cmd.loadRemote:= FALSE;
				file.step 			:= FILE_OPEN;				
				file.fileDevice 	:= gDev.sts.deviceName;
				file.fileName 		:= gDev.par.selectedFile;
				file.stat.msgStr	:= 'Processing request...';
			END_IF		

		FILE_OPEN:
			file.fb.FileOpen.enable		:= TRUE;
			file.fb.FileOpen.pDevice	:= ADR(file.fileDevice);
			file.fb.FileOpen.pFile		:= ADR(file.fileName);
			file.fb.FileOpen.mode		:= fiREAD_WRITE;
			file.fb.FileOpen();
	
			IF( file.fb.FileOpen.status = ERR_OK ) THEN
				FileHandle	:= file.fb.FileOpen.ident;
				FileLength	:= file.fb.FileOpen.filelen;
				file.step	:= FILE_READ;
				// clear the old recipe information out
				brsmemset(ADR(TempRecipe[0]), 0, SIZEOF(TempRecipe));
				brsmemset(ADR(TempString), 0, SIZEOF(TempString));
				brsmemset(ADR(TempUsint), 0, SIZEOF(TempUsint));
				
			ELSIF (file.fb.FileOpen.status = ERR_FUB_BUSY) THEN
				file.step	:= FILE_OPEN;
			ELSIF file.fb.FileOpen.status = fiERR_FILE_NOT_FOUND THEN
				file.stat.errStep 		:= file.step;
				file.step				:= FILE_ERROR;
				file.stat.msgStr		:= 'File not found!';
			ELSE
				file.stat.fileIOerrNum 	:= file.fb.FileOpen.status;
				file.stat.errStep 		:= file.step;
				file.step				:= FILE_ERROR;
				file.stat.msgStr		:= 'Failed to open file!';
			END_IF
		
		FILE_READ:
			file.fb.FileRead.enable		:= TRUE;
			file.fb.FileRead.ident		:= FileHandle;
			file.fb.FileRead.offset		:= 0;
			file.fb.FileRead.pDest		:= ADR(TempString);
			file.fb.FileRead.len		:= FileLength;
			file.fb.FileRead();
		
			IF( file.fb.FileRead.status <> ERR_OK) THEN
				IF( file.fb.FileRead.status = ERR_FUB_BUSY ) THEN
					file.step	:= FILE_READ;
				ELSE
					file.stat.fileIOerrNum 	:= file.fb.FileRead.status;
					file.stat.errStep 		:= file.step;
					file.step				:= FILE_ERROR;
					file.stat.msgStr		:= 'Failed to read file!';
				END_IF
			ELSE
				file.step		:= FILE_PARSE;
				parseStep		:= FILE_PARSE_CONVERT;
			END_IF
		
		FILE_PARSE:
			ParseFile;
			
			IF parseStep = FILE_PARSE_SUCCESS THEN
				parseStep := FILE_PARSE_IDLE;
				file.step := FILE_COPYRECIPE;
			ELSIF parseStep = FILE_PARSE_ERROR THEN
				//gAlarmInfo.EdgeAlarmFlags FAILED TO PARSE FILE
				parseStep := FILE_PARSE_IDLE;
				file.step := FILE_CLOSE;
			END_IF
			
		FILE_COPYRECIPE:			
			// move the recipe data out of temp into job recipe tags
			FOR sq := 1 TO NUM_MAX_SEQ DO
				gMain.par.seq[sq].Qty := REAL_TO_INT(TempRecipe[sq-1].TempSequence[0]);
				
				// Check for which units the job was supposed to be received in
				CASE remoteJobUnits OF
					JOBFILE_UNIT_IN: tmpSeqLng := (TempRecipe[sq-1].TempSequence[4]* IN_TO_MM); //(INCH --> MM)
					JOBFILE_UNIT_MM: tmpSeqLng := TempRecipe[sq-1].TempSequence[4]; //(STAY IN MM)
				END_CASE
				
				gMain.par.seq[sq].Lng := LIMIT(MIN_ALLOWED_PART_LNG, tmpSeqLng, MAX_ALLOWED_PART_LNG); 
			
				// get job size
				IF tmpSeqLng = 0.0 THEN
					gMain.par.totalSeq := sq - 1;
					EXIT;
				END_IF
			END_FOR
			
			// clear temp data
			brsmemset(ADR(TempUsint), 0, SIZEOF(TempUsint));
			
			TempString := '';
			char := 0;
			j := 0;
			sq := 0;
			file.step := FILE_CLOSE;
				
		FILE_CLOSE:
			file.fb.FileClose.enable:= TRUE;
			file.fb.FileClose.ident	:= FileHandle;
			file.fb.FileClose();
			
			IF file.fb.FileClose.status = ERR_OK THEN  (* FileClose successful *)
				file.step := FILE_WAIT;  (* next Step*)
				// message the operator
				gAlarmInfo.EdgeAlarmFlags.FileLoadSuccess := TRUE;
			ELSIF file.fb.FileClose.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				file.stat.fileIOerrNum := file.fb.FileClose.status;
				file.stat.errStep 	:= file.step;
				file.step 			:= FILE_ERROR;
				file.stat.msgStr	:= 'Error closing file from network share!';
			END_IF	
			
		FILE_ERROR:
			
			file.fb.FileOpen.enable		:= FALSE;
			file.fb.FileOpen();
			
			file.fb.FileRead.enable		:= FALSE;
			file.fb.FileRead();
			
			file.fb.FileClose.enable 	:= FALSE;
			file.fb.FileClose();
			
			file.stat.fileSysErrNum := FileIoGetSysError();

			// message the operator
			gAlarmInfo.EdgeAlarmFlags.FileLoadFailed := TRUE;
			
			file.stat.errStep	:= FILE_WAIT;
			file.step 			:= FILE_WAIT;
		
	END_CASE

	// set status
	gMain.sts.loadingJob := (file.step <> FILE_WAIT); 
	
END_PROGRAM

