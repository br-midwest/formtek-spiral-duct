
TYPE
	fileStep_enum : 
		(
		FILE_WAIT := 0,
		FILE_OPEN := 20,
		FILE_READ := 30,
		FILE_PARSE := 40,
		FILE_WRITE := 50,
		FILE_CLOSE := 60,
		FILE_COPYRECIPE := 90, (*Copy recipe to the recipe location where it is actually executed*)
		FILE_ERROR := 99
		);
	fileParseStep_enum : 
		(
		FILE_PARSE_IDLE := 0,
		FILE_PARSE_CONVERT := 1,
		FILE_PARSE_PARSING := 2,
		FILE_PARSE_ADD_SPACE := 3,
		FILE_PARSE_SUCCESS := 4,
		FILE_PARSE_ERROR := 5
		);
	jobFileLngUnit_enum : 
		(
		JOBFILE_UNIT_IN := 0,
		JOBFILE_UNIT_MM := 1
		);
	shareMode_enum : 
		(
		SHAREMODE_WINSHARE := 0,
		SHAREMODE_USB := 1
		);
	TempSequence_Type : 	STRUCT 
		TempSequence : ARRAY[0..8]OF REAL;
	END_STRUCT;
	file_typ : 	STRUCT 
		fileName : STRING[50]; (*file name, no extention, entered by OP on HMI*)
		fileDevice : STRING[80]; (*file device name*)
		step : fileStep_enum;
		stat : file_stat_typ;
		fb : file_fb_typ;
	END_STRUCT;
	file_stat_typ : 	STRUCT 
		fileSysErrNum : UINT;
		fileIOerrNum : UINT;
		msgStr : STRING[80];
		errStep : fileStep_enum;
	END_STRUCT;
	file_fb_typ : 	STRUCT 
		FileClose : FileClose;
		FileRead : FileRead;
		FileOpen : FileOpen;
	END_STRUCT;
END_TYPE
