(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Author:		Brad Jones
 * Created:		Jan 2 2018/16:23 
*********************************************************
[PROGRAM INFORMATION]
Network management program
Allows for the setting of IP address and setting of IF
mode on PLC ethernet port
********************************************************)

PROGRAM _INIT

	(* init values *)
	strcpy(ADR(Network.Data.Device), ADR('IF2'));  (* X20 CPU Ethernet device *)
	
	IF gSimulate THEN
		Network.Data.Step := 254;  (* deactivate this task by going to undefined state *)
	ELSE
		Network.Data.Step := 10;  (* Initialize all parameters *)
	END_IF
	
END_PROGRAM


PROGRAM _CYCLIC
	
	// execute network commands within the AsARCfg sample
	CASE Network.Data.Step OF
 	
		0:	IF Network.Command.bGetNetworkInfo THEN
				Network.Data.Step := 10;
			ELSIF Network.Command.bSetInaNodeNum THEN
				Network.Data.Step := 110;
			ELSIF Network.Command.bSetIpAddress THEN
				Network.Data.Step := 120;
			ELSIF Network.Command.bSetSubnetMask THEN
				Network.Data.Step := 130;
			ELSIF Network.Command.bSetBroadcastAddress THEN
				Network.Data.Step := 140;
			ELSIF Network.Command.bSetDefaultGateway THEN
				Network.Data.Step := 150;
			ELSIF Network.Command.bSetEthBaudrate THEN
				Network.Data.Step := 160;
			ELSIF Network.Command.bSetEthConfigMode THEN
				Network.Data.Step := 170;
			ELSIF Network.Command.bSetHostName THEN
				Network.Data.Step := 180;
			ELSIF Network.Command.bCfgGetDhcpsData THEN
				Network.Data.Step := 190;
			ELSIF Network.Command.bCfgSetDhcpsData THEN
				Network.Data.Step := 200;
			ELSIF Network.Command.bSetSDMStatus THEN
				Network.Data.Step := 220;
			END_IF
					
		10:	 (* Get the INA node number of the configured Ethernet device *)
			Network.Functionblocks.CfgGetInaNode_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetInaNode_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			
			Network.Functionblocks.CfgGetInaNode_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetInaNode_0.status = 0 THEN  (* CfgGetInaNode successfull *)
				Network.Data.InaNodeNum := Network.Functionblocks.CfgGetInaNode_0.InaNode;
   				Network.Data.Step := 20;
			ELSIF Network.Functionblocks.CfgGetInaNode_0.status = ERR_FUB_BUSY THEN  (* CfgGetInaNode not finished -> redo *)
   				(* Busy *)		
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		20:	 (* Get the IP address of the configured Ethernet device *)
			Network.Functionblocks.CfgGetIPAddr_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetIPAddr_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgGetIPAddr_0.pIPAddr := ADR(Network.Data.IpAddressAct);  (* String which contains the readout IP address <------------------------------------------------------------------------ *)
   			Network.Functionblocks.CfgGetIPAddr_0.Len := SIZEOF(Network.Data.IpAddressAct);  (* Length of the IP address string (ideally 16) *)
			
			Network.Functionblocks.CfgGetIPAddr_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetIPAddr_0.status = 0 THEN  (* CfgGetIPAddr successfull *)
   				Network.Data.Step := 30;		
			ELSIF Network.Functionblocks.CfgGetIPAddr_0.status = ERR_FUB_BUSY THEN  (* CfgGetIPAddr not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		30:	 (* Get the Subnet Mask of the configured Ethernet device *)
			Network.Functionblocks.CfgGetSubnetMask_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetSubnetMask_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgGetSubnetMask_0.pSubnetMask := ADR(Network.Data.SubnetMaskAct);  (* String which contains the readout SubnetMask <------------------------------------------------------------------------ *)	
   			Network.Functionblocks.CfgGetSubnetMask_0.Len := SIZEOF(Network.Data.SubnetMaskAct);  (* Length of the SubnetMask string (ideally 16) *)
			
			Network.Functionblocks.CfgGetSubnetMask_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetSubnetMask_0.status = 0 THEN  (* CfgGetSubnetMask successfull *)
				Network.Data.Step := 0;
				Network.Command.bGetNetworkInfo := 0;
				// if there is no information in the user input for the network settings, initialize them
				IF strcmp(ADR(Network.Data.IpAddressReq), ADR(emptyString)) = 0 OR strcmp(ADR(Network.Data.SubnetMaskReq), ADR(emptyString)) = 0 THEN
					Network.Data.IpAddressReq	:= Network.Data.IpAddressAct;
					Network.Data.SubnetMaskReq	:= Network.Data.SubnetMaskAct;
				END_IF
			ELSIF Network.Functionblocks.CfgGetSubnetMask_0.status = ERR_FUB_BUSY THEN  (* CfgGetSubnetMask not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		40:	 (* Get the broadcast address from the AR registry for a configured Ethernet device *)
			Network.Functionblocks.CfgGetBroadcastAddr_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetBroadcastAddr_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgGetBroadcastAddr_0.pBroadcastAddr := ADR(Network.Data.BroadcastAddr);  (* String which contains the readout Broadcast address <------------------------------------------------------------------------ *)
			Network.Functionblocks.CfgGetBroadcastAddr_0.Len := SIZEOF(Network.Data.BroadcastAddr);  (* Length of the Broadcast address string (ideally 16) *)
		
			Network.Functionblocks.CfgGetBroadcastAddr_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetBroadcastAddr_0.status = 0 THEN  (* CfgGetBroadcastAddr successfull *)
   				Network.Data.Step := 50;
			ELSIF Network.Functionblocks.CfgGetBroadcastAddr_0.status = ERR_FUB_BUSY THEN  (* CfgGetBroadcastAddr not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		50:	 (* Get the default gateway address from the AR registry for a configured Ethernet device *)
			Network.Functionblocks.CfgGetDefaultGateway_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetDefaultGateway_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgGetDefaultGateway_0.pGateway := ADR(Network.Data.Gateway);  (* String which contains the readout Default Gateway <------------------------------------------------------------------------ *)
			Network.Functionblocks.CfgGetDefaultGateway_0.Len := SIZEOF(Network.Data.Gateway);  (* Length of the Default Gateway string (ideally 16) *)
			
			Network.Functionblocks.CfgGetDefaultGateway_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetDefaultGateway_0.status = 0 THEN  (* CfgGetDefaultGateway successfull *)
   				Network.Data.Step := 60;
			ELSIF Network.Functionblocks.CfgGetDefaultGateway_0.status = ERR_FUB_BUSY THEN  (* CfgGetDefaultGateway not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
		
		60:	 (* Get the baud rate from the AR registry for a configured Ethernet device *)
			Network.Functionblocks.CfgGetEthBaudrate_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetEthBaudrate_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
				
			Network.Functionblocks.CfgGetEthBaudrate_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetEthBaudrate_0.status = 0 THEN  (* CfgGetEthBaudrate successfull *)
				Network.Data.EthBaudrate := Network.Functionblocks.CfgGetEthBaudrate_0.Baudrate;
   				Network.Data.Step := 70;
			ELSIF Network.Functionblocks.CfgGetEthBaudrate_0.status = ERR_FUB_BUSY THEN  (* CfgGetEthBaudrate not finished -> redo *)
   				(* Busy *)		
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		70:	 (* Get the configuration mode currently being used for a configured Ethernet device *)
			Network.Functionblocks.CfgGetEthConfigMode_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetEthConfigMode_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			
			Network.Functionblocks.CfgGetEthConfigMode_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetEthConfigMode_0.status = 0 THEN  (* CfgGetEthConfigMode successfull *)
				Network.Data.EthConfigMode := Network.Functionblocks.CfgGetEthConfigMode_0.ConfigMode;
   				Network.Data.Step := 80;
			ELSIF Network.Functionblocks.CfgGetEthConfigMode_0.status = ERR_FUB_BUSY THEN  (* CfgGetEthConfigMode not finished -> redo *)
   				(* Busy *)		
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		80:	 (* Get the host name of your target *)
			Network.Functionblocks.CfgGetHostName_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetHostName_0.pHostName := ADR(Network.Data.HostName);  (* String which contains the readout Default Gateway <------------------------------------------------------------------------ *)
			Network.Functionblocks.CfgGetHostName_0.Len := SIZEOF(Network.Data.HostName);  (* Length of the HostName string *)
			
			Network.Functionblocks.CfgGetHostName_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgGetHostName_0.status = 0 THEN  (* CfgGetHostName successfull *)
   				Network.Data.Step := 90;
			ELSIF Network.Functionblocks.CfgGetHostName_0.status = ERR_FUB_BUSY THEN  (* CfgGetHostName not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		90:	 (* Get the MAC address of an Ethernet device *)
			Network.Functionblocks.CfgGetMacAddr_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetMacAddr_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgGetMacAddr_0.pMacAddr := ADR(Network.Data.MacAddress);  (* Memory area which contains the readout MAC address <------------------------------------------------------------------------ *)
			Network.Functionblocks.CfgGetMacAddr_0.Len := SIZEOF(Network.Data.MacAddress);  (* Length of the MAC address (ideally 6 bytes) *)
			
			Network.Functionblocks.CfgGetMacAddr_0; (* Function Call *)
			
			IF Network.Functionblocks.CfgGetMacAddr_0.status = 0 THEN  (* CfgGetMacAddr successfull *)
				Network.Command.bGetNetworkInfo := 0;
   				Network.Data.Step := 100;
			ELSIF Network.Functionblocks.CfgGetMacAddr_0.status = ERR_FUB_BUSY THEN  (* CfgGetMacAddr not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		100:  (* Get the current status of the system diagnostics *)
			Network.Functionblocks.CfgGetSdmStatus_0.enable := 1;  (* Enable the function block *)
		
			Network.Functionblocks.CfgGetSdmStatus_0;  (* Function Call *)
		
			IF Network.Functionblocks.CfgGetSdmStatus_0.status = 0 THEN  (* CfgGetSdmStatus successfull *)
				Network.Data.SDMStatus := Network.Functionblocks.CfgGetSdmStatus_0.run;
				Network.Data.Step := 0;				
			ELSIF Network.Functionblocks.CfgGetSdmStatus_0.status = ERR_FUB_BUSY THEN  (* CfgGetSdmStatus not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		110:  (* Set the INA node number of the configured Ethernet device *) 
			Network.Functionblocks.CfgSetInaNode_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetInaNode_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetInaNode_0.InaNode := Network.Data.InaNodeNum;  (* Desired INA Node number *)
			Network.Functionblocks.CfgSetInaNode_0.Option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetInaNode_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetInaNode_0.status = 0 THEN  (* CfgSetInaNode successfull *)
				Network.Command.bSetInaNodeNum := 0;
   				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetInaNode_0.status = ERR_FUB_BUSY THEN  (* CfgSetInaNode not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF			
			
		120:  (* Set the IP address of the configured Ethernet device *)
			Network.Functionblocks.CfgSetIPAddr_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetIPAddr_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetIPAddr_0.pIPAddr := ADR(Network.Data.IpAddressReq);  (* Desired IP address *)
			Network.Functionblocks.CfgSetIPAddr_0.Option := cfgOPTION_NON_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetIPAddr_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetIPAddr_0.status = 0 THEN  (* CfgSetIPAddr successfull *)
				Network.Command.bSetIpAddress := 0;
				Network.Data.Step := 0;
				Network.Command.bGetNetworkInfo := 1; // loop back and update the current information
			ELSIF Network.Functionblocks.CfgSetIPAddr_0.status = ERR_FUB_BUSY THEN  (* CfgSetIPAddr not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		130:  (* Set the Subnet Mask of the configured Ethernet device *)
			Network.Functionblocks.CfgSetSubnetMask_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetSubnetMask_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetSubnetMask_0.pSubnetMask := ADR(Network.Data.SubnetMaskReq);  (* Desired SubnetMask *)
			Network.Functionblocks.CfgSetSubnetMask_0.Option := cfgOPTION_NON_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetSubnetMask_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetSubnetMask_0.status = 0 THEN  (* CfgSetSubnetMask successfull *)
				Network.Command.bSetSubnetMask := 0;
				Network.Data.Step := 0;
				Network.Command.bGetNetworkInfo := 1; // loop back and update the current information
			ELSIF Network.Functionblocks.CfgSetSubnetMask_0.status = ERR_FUB_BUSY THEN  (* CfgSetSubnetMask not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		140:  (* Set the broadcast address into the AR registry for a configured Ethernet device *)
			Network.Functionblocks.CfgSetBroadcastAddr_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetBroadcastAddr_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetBroadcastAddr_0.pBroadcastAddr := ADR(Network.Data.BroadcastAddr);  (* Desired broadcast address *)
			Network.Functionblocks.CfgSetBroadcastAddr_0.Option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetBroadcastAddr_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetBroadcastAddr_0.status = 0 THEN  (* CfgSetBroadcastAddr successfull *)
				Network.Command.bSetBroadcastAddress := 0;
   				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetBroadcastAddr_0.status = ERR_FUB_BUSY THEN  (* CfgSetBroadcastAddr not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		150:  (* Set the default gateway address into the AR registry for a configured Ethernet device *)
			Network.Functionblocks.CfgSetDefaultGateway_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetDefaultGateway_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetDefaultGateway_0.pGateway := ADR(Network.Data.Gateway);  (* Desired Default Gateway *)
			Network.Functionblocks.CfgSetDefaultGateway_0.Option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetDefaultGateway_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetDefaultGateway_0.status = 0 THEN  (* CfgSetDefaultGateway successfull *)
				Network.Command.bSetDefaultGateway := 0;
   				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetDefaultGateway_0.status = ERR_FUB_BUSY THEN  (* CfgSetDefaultGateway not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		160:  (* Set the baud rate into the AR registry for a configured Ethernet device *)
			Network.Functionblocks.CfgSetEthBaudrate_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetEthBaudrate_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetEthBaudrate_0.Baudrate := Network.Data.EthBaudrate;  (* Desired Baud rate *)
			Network.Functionblocks.CfgSetEthBaudrate_0.Option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetEthBaudrate_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetEthBaudrate_0.status = 0 THEN  (* CfgSetEthBaudrate successfull *)
				Network.Command.bSetEthBaudrate := 0;
   				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetEthBaudrate_0.status = ERR_FUB_BUSY THEN  (* CfgSetEthBaudrate not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		170:  (* Set the configuration mode for a configured Ethernet device *)
			Network.Functionblocks.CfgSetEthConfigMode_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetEthConfigMode_0.pDevice := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetEthConfigMode_0.ConfigMode := Network.Data.EthConfigMode;  (* Desired ethernet config mode *)
			Network.Functionblocks.CfgSetEthConfigMode_0.Option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetEthConfigMode_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetEthConfigMode_0.status = 0 THEN  (* CfgSetEthConfigMode successfull *)
				Network.Command.bSetEthConfigMode := 0;
   				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetEthConfigMode_0.status = ERR_FUB_BUSY THEN  (* CfgSetEthConfigMode not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		180:  (* Set the host name of your target *)
			Network.Functionblocks.CfgSetHostName_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetHostName_0.pHostName := ADR(Network.Data.HostName);  (* Desired host name *)
			Network.Functionblocks.CfgSetHostName_0.Option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
			
			Network.Functionblocks.CfgSetHostName_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetHostName_0.status = 0 THEN  (* CfgSetHostName successfull *)
				Network.Command.bSetHostName := 0;
   				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetHostName_0.status = ERR_FUB_BUSY THEN  (* CfgSetHostName not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		190:  (* Read the DHCP server configuration *)
			Network.Functionblocks.CfgGetDhcpsData_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgGetDhcpsData_0.pInterfaceList := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgGetDhcpsData_0.len := SIZEOF(Network.Data.Device);  (* Length of the device string *)
  			Network.Functionblocks.CfgGetDhcpsData_0.pLeases := ADR(Network.Data.Lease);  (* cfgLease_typ array *)
			Network.Functionblocks.CfgGetDhcpsData_0.numLeases := 1;  (* Number of cfgLease_typ array elements *)		
		
			Network.Functionblocks.CfgGetDhcpsData_0;  (* Function Call *)
		
			IF Network.Functionblocks.CfgGetDhcpsData_0.status = 0 THEN  (* CfgGetDhcpsData successfull *)
				Network.Command.bCfgGetDhcpsData := 0;
				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgGetDhcpsData_0.status = ERR_FUB_BUSY THEN  (* CfgGetDhcpsData not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF

		200:  (* Configure the DHCP server *)
			Network.Functionblocks.CfgSetDhcpsData_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetDhcpsData_0.pInterfaceList := ADR(Network.Data.Device);  (* Name of the device *)
			Network.Functionblocks.CfgSetDhcpsData_0.pLeases := ADR(Network.Data.Lease);  (* cfgLease_typ array *)
			Network.Functionblocks.CfgSetDhcpsData_0.numLeases := 1;  (* Number of cfgLease_typ array elements *)
			Network.Functionblocks.CfgSetDhcpsData_0.option := cfgOPTION_VOLATILE;  (*  Temporarily set the parameter *)
		
			Network.Functionblocks.CfgSetDhcpsData_0;  (* Function Call *)
		
			IF Network.Functionblocks.CfgSetDhcpsData_0.status = 0 THEN  (* CfgSetDhcpsData successfull *)
				Network.Command.bCfgSetDhcpsData := 0;
				Network.Data.Step := 210;
			ELSIF Network.Functionblocks.CfgSetDhcpsData_0.status = ERR_FUB_BUSY THEN  (* CfgSetDhcpsData not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
		
		210:  (* Start the DHCP server *)
			Network.Functionblocks.CfgSetDhcpServer_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetDhcpServer_0.start := 1;  (* Start the DHCP server *)
			Network.Functionblocks.CfgSetDhcpServer_0.option := cfgOPTION_NON_VOLATILE;  (*  Temporarily set the parameter *)
		
			Network.Functionblocks.CfgSetDhcpServer_0;  (* Function Call *)
		
			IF Network.Functionblocks.CfgSetDhcpServer_0.status = 0 THEN  (* CfgSetDhcpServer successfull *)
				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetDhcpServer_0.status = ERR_FUB_BUSY THEN  (* CfgSetDhcpServer not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF	
			
		220:  (* Start or stop the system diagnostic function (SDM) *)
			Network.Functionblocks.CfgSetSdmStatus_0.enable := 1;  (* Enable the function block *)
			Network.Functionblocks.CfgSetSdmStatus_0.start := Network.Data.SDMStatus;  (* Start or stop SMD *)
			
			Network.Functionblocks.CfgSetSdmStatus_0;  (* Function Call *)
			
			IF Network.Functionblocks.CfgSetSdmStatus_0.status = 0 THEN  (* CfgSetSdmStatus successfull *)
				Network.Command.bSetSDMStatus := 0;
				Network.Data.Step := 0;
			ELSIF Network.Functionblocks.CfgSetSdmStatus_0.status = ERR_FUB_BUSY THEN  (* CfgSetSdmStatus not finished -> redo *)
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Network.Data.Step := 255;
				gAlarmInfo.EdgeAlarmFlags.NetworkError := TRUE;
			END_IF
			
		255:  (*Error handling*)
			Network.Command.bGetNetworkInfo		:= FALSE;
			Network.Command.bSetEthConfigMode	:= FALSE;
			Network.Command.bSetIpAddress		:= FALSE;
			Network.Command.bSetSubnetMask		:= FALSE;
			Network.Command.bCfgGetDhcpsData	:= FALSE;
			Network.Command.bCfgSetDhcpsData	:= FALSE;
			Network.Command.bSetBroadcastAddress:= FALSE;
			Network.Command.bSetDefaultGateway	:= FALSE;
			Network.Command.bSetEthBaudrate		:= FALSE;
			Network.Command.bSetEthConfigMode	:= FALSE;
			Network.Command.bSetHostName		:= FALSE;
			Network.Command.bSetInaNodeNum		:= FALSE;
			Network.Command.bSetSDMStatus		:= FALSE;
		
			IF NOT(gAlarmInfo.rxnMessage) THEN
				Network.Data.Step := 0;
			END_IF
		
	END_CASE
	
END_PROGRAM
