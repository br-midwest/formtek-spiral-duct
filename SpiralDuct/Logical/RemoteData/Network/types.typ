(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: NwManagement
 * File: NwManagement.typ
 * Author: Bernecker + Rainer
 * Created: April 21, 2009
 ********************************************************************
 * Local data types of program NwManagement
 ********************************************************************)

TYPE
	Network_typ : 	STRUCT 
		Command : Network_Command_typ; (*Structure Command (contains all command elements)*)
		Data : Network_Data_typ; (*Structure Data (contains all data informations)*)
		Functionblocks : Network_Functionblocks_typ; (*Structure Functionblocks (contains all FuBs)*)
	END_STRUCT;
	Network_Command_typ : 	STRUCT 
		bGetNetworkInfo : BOOL; (*Command: Get several network informations*)
		bSetInaNodeNum : BOOL; (*Command: Set the configured ina node number*)
		bSetIpAddress : BOOL; (*Command: Set the configured IP address*)
		bSetSubnetMask : BOOL; (*Command: Set the configured SubnetMask*)
		bSetBroadcastAddress : BOOL; (*Command: Set the configured broadcast address*)
		bSetDefaultGateway : BOOL; (*Command: Set the configured default gateway*)
		bSetEthBaudrate : BOOL; (*Command: Set the configured ethernet baudrate*)
		bSetEthConfigMode : BOOL; (*Command: Set the configured config mode*)
		bSetHostName : BOOL; (*Command: Set the configured host name*)
		bCfgGetDhcpsData : BOOL; (*Command: Get the configured DHCP server data*)
		bCfgSetDhcpsData : BOOL; (*Command: Set the configured DHCP server data*)
		bSetSDMStatus : BOOL; (*Command: Start SDM*)
	END_STRUCT;
	Network_Data_typ : 	STRUCT 
		InaNodeNum : USINT; (*Data variable for CfgGetInaNode() and CfgSetInaNode()*)
		IpAddressReq : STRING[16]; (*Data variable for CfgGetIPAddr() and CfgSetIPAddr()*)
		SubnetMaskReq : STRING[16]; (*Data variable for CfgGetSubnetMask() and CfgSetSubnetMask()*)
		IpAddressAct : STRING[16]; (*Data variable for CfgGetIPAddr() and CfgSetIPAddr()*)
		SubnetMaskAct : STRING[16]; (*Data variable for CfgGetSubnetMask() and CfgSetSubnetMask()*)
		BroadcastAddr : STRING[16]; (*Data variable for CfgGetBroadcastAddr() and CfgSetBroadcastAddr()*)
		Gateway : STRING[16]; (*Data variable for CfgGetDefaultGateway() and CfgSetDefaultGateway()*)
		EthBaudrate : UDINT; (*Data variable for CfgGetEthBaudrate() and CfgSetEthBaudrate()*)
		EthConfigMode : UDINT; (*Data variable for CfgGetEthConfigMode() and CfgSetEthConfigMode()*)
		HostName : STRING[80]; (*Data variable for CfgGetHostName() and CfgSetHostName()*)
		MacAddress : ARRAY[0..5]OF USINT; (*Data variable for CfgGetMacAddr()*)
		Lease : cfgLease_typ; (*Data structure for CfgGetDhcpsData() and CfgSetDhcpsData()*)
		SDMStatus : UDINT; (*Data variable for CfgGetSdmStatus() and CfgSetSdmStatus()*)
		Device : STRING[80]; (*Device string (contains the device path)*)
		Step : USINT; (*Step variable*)
	END_STRUCT;
	Network_Functionblocks_typ : 	STRUCT 
		CfgGetInaNode_0 : CfgGetInaNode; (*Functionblock CfgGetInaNode()*)
		CfgGetIPAddr_0 : CfgGetIPAddr; (*Functionblock CfgGetIPAddr()*)
		CfgGetSubnetMask_0 : CfgGetSubnetMask; (*Functionblock CfgGetSubnetMask()*)
		CfgGetBroadcastAddr_0 : CfgGetBroadcastAddr; (*Functionblock CfgGetBroadcastAddr()*)
		CfgGetDefaultGateway_0 : CfgGetDefaultGateway; (*Functionblock CfgGetDefaultGateway()*)
		CfgGetEthBaudrate_0 : CfgGetEthBaudrate; (*Functionblock CfgGetEthBaudrate()*)
		CfgGetEthConfigMode_0 : CfgGetEthConfigMode; (*Functionblock CfgGetEthConfigMode()*)
		CfgGetHostName_0 : CfgGetHostName; (*Functionblock CfgGetHostName()*)
		CfgGetMacAddr_0 : CfgGetMacAddr; (*Functionblock CfgGetMacAddr()*)
		CfgGetSdmStatus_0 : CfgGetSdmStatus; (*Functionblock CfgGetSdmStatus()*)
		CfgSetInaNode_0 : CfgSetInaNode; (*Functionblock CfgSetInaNode()*)
		CfgSetIPAddr_0 : CfgSetIPAddr; (*Functionblock CfgSetIPAddr()*)
		CfgSetSubnetMask_0 : CfgSetSubnetMask; (*Functionblock CfgSetSubnetMask()*)
		CfgSetBroadcastAddr_0 : CfgSetBroadcastAddr; (*Functionblock CfgSetBroadcastAddr()*)
		CfgSetDefaultGateway_0 : CfgSetDefaultGateway; (*Functionblock CfgSetDefaultGateway()*)
		CfgSetEthBaudrate_0 : CfgSetEthBaudrate; (*Functionblock CfgSetEthBaudrate()*)
		CfgSetEthConfigMode_0 : CfgSetEthConfigMode; (*Functionblock CfgSetEthConfigMode()*)
		CfgSetHostName_0 : CfgSetHostName; (*Functionblock CfgSetHostName()*)
		CfgGetDhcpsData_0 : CfgGetDhcpsData; (*Functionblock CfgGetDhcpsData()*)
		CfgSetDhcpsData_0 : CfgSetDhcpsData; (*Functionblock CfgSetDhcpsData()*)
		CfgSetDhcpServer_0 : CfgSetDhcpServer; (*Functionblock CfgSetDhcpServer()*)
		CfgSetSdmStatus_0 : CfgSetSdmStatus; (*Functionblock CfgSetSdmStatus()*)
	END_STRUCT;
END_TYPE
