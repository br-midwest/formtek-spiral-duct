
TYPE
	browser_list_typ : 	STRUCT 
		date : ARRAY[0..49]OF DATE_AND_TIME;
		size : ARRAY[0..49]OF UDINT;
		name : ARRAY[0..49]OF STRING[255];
	END_STRUCT;
END_TYPE
