
PROGRAM _INIT
	
	MpFileManagerUI_0.Enable 	:= TRUE;
	MpFileManagerUI_0.MpLink	:= ADR(mpFileManagerUI);
	MpFileManagerUI_0.UIConnect	:= ADR(uiconnect);
	MpFileManagerUI_0.UISetup.FileListScrollWindow	:= 1;
	MpFileManagerUI_0.UISetup.FileListSize			:= 5;
	
	IF gSimulate THEN
		uiconnect.DeviceList.DeviceNames[0]	:= FILE_DEV_NAME_SIM;
	ELSE
		uiconnect.DeviceList.DeviceNames[0]	:= FILE_DEV_NAME_FPART;
	END_IF
	uiconnect.DeviceList.DeviceNames[1]	:= FILE_DEV_NAME_WINDOWS;
	uiconnect.DeviceList.DeviceNames[2]	:= FILE_DEV_NAME_USB;
	
	extension := '.txt';
	
END_PROGRAM

PROGRAM _CYCLIC

	// set selected file device source for browser
	uiconnect.DeviceList.SelectedIndex := UDINT_TO_UINT(gDev.sts.selectedDevice);
	
	// extract list of files from the file ui
	IF EDGENEG(uiconnect.Status = mpFILE_UI_STATUS_CHANGE_DEVICE) OR EDGE(gDev.cmd.reqRemoteDevice) THEN
		tempRefresh := FALSE;
		brsmemset(ADR(list), 0, SIZEOF(list));
		
		j := 0;
		FOR i := 0 TO 49 DO
			IF (uiconnect.File.List.Items[i].ItemType = mpFILE_ITEM_TYPE_TXT) THEN
				list.name[j] := uiconnect.File.List.Items[i].Name;
				list.date[j] := uiconnect.File.List.Items[i].LastModified;
				list.size[j] := uiconnect.File.List.Items[i].Size;
				j := j + 1;
			END_IF
		END_FOR
		
		listSelectedIndex := 0;
	END_IF
		
	IF listSelectionMade THEN
		listSelectionMade := FALSE;
		checkExt := TRUE;
		gDev.par.selectedFile := list.name[listSelectedIndex];
	END_IF
	
	// check that op entered values have extension
	IF manualEntry OR checkExt THEN
		manualEntry := FALSE;
		checkExt := FALSE;
		
		IF FIND(gDev.par.selectedFile, extension) = 0 THEN
			gDev.par.selectedFile := CONCAT(gDev.par.selectedFile, extension);
		END_IF
	END_IF

	MpFileManagerUI_0.ErrorReset := gMain.cmd.reset;

	// call fb
	MpFileManagerUI_0();
	 
END_PROGRAM

PROGRAM _EXIT

	MpFileManagerUI_0(Enable := FALSE);
	
END_PROGRAM

