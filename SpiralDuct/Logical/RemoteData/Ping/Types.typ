(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Icmp_Ping
 * File: Icmp_Ping.typ
 * Author: Bernecker + Rainer
 * Created: April 21, 2009
 ********************************************************************
 * Local data types of program Icmp_Ping
 ********************************************************************)

TYPE
	Ping_typ : 	STRUCT 
		step : ping_step_enum; (*Step variable*)
		Host : STRING[80]; (*Host name*)
	END_STRUCT;
	ping_step_enum : 
		(
		PING_IDLE,
		PING_PINGING,
		PING_RESET
		);
END_TYPE
