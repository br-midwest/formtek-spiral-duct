(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Icmp_Ping
 * File: Icmp_Ping.st
 * Author: Brad Jones
 * Created: JUNE 1 2020
 ********************************************************************
 * Pings a station and returns result status
 ********************************************************************)

PROGRAM _INIT
	
	strcpy(ADR(Ping.Host),ADR('192.168.0.1'));
	lastStatus := ERR_FUB_ENABLE_FALSE;
	
END_PROGRAM

PROGRAM _CYCLIC

	CASE Ping.step OF
		PING_IDLE:
			IF action THEN
				action := FALSE;
				Ping.step := PING_PINGING;
			END_IF
			
		PING_PINGING:	(*Ping station*)
			IcmpPing_0.enable := TRUE;  (*Enables the FBK*)
			IcmpPing_0.pHost := ADR(Ping.Host);  (*IP address or name (SG4 only) of the station (host) that should be pinged*)
			IcmpPing_0.timeout := 100;  (*Response timeout. Specified in 10ms steps (1 = 10ms, 2 = 20ms, etc.). If 0 is specified, the default timeout (10ms) is used.*)
			
			IcmpPing_0;  (*Call FBK*)
			lastStatus := IcmpPing_0.status;
			
			IF IcmpPing_0.status = ERR_FUB_BUSY THEN  (*FBK still busy, call again*)	
				(*wait for fb to produce a status*)
			ELSE 
				Ping.step := PING_RESET;
			END_IF
		
		PING_RESET:
			IcmpPing_0.enable := FALSE;  (*Disnables the FBK*)
			IcmpPing_0;  (*Call FBK*)
		
			Ping.step := PING_IDLE;
	END_CASE

END_PROGRAM
