
PROGRAM _INIT

	(* Always enable the drive *)
	gIO.dout.VFDContactor	:= TRUE;
	
	// make sure there is no null pointer
	vfdio ACCESS ADR(gVFDio[0]);
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// pick the configured hw by available node
	GetNode;

	(* Reset commands when they are completed *)
	vfdio.DS402.ErrorAcknowledge	:= gMain.cmd.reset;
	
	//If there is no error reset the error reset command
	IF NOT(vfdio.DS402.Error) THEN
		vfdio.DS402.ErrorAcknowledge	:= FALSE;
	END_IF

	//When the axis is stopped reset all the movment commands
	IF NOT(vfdio.DS402.InMotion) THEN
		vfdio.DS402.Stop			:= FALSE;
	END_IF
	
	vfdio.DS402.SwitchOn		:= gIO.dout.DriveEnable AND NOT(vfdio.DS402.Error) AND vfdio.DS402.Active;
	vfdio.DS402.MoveVelocity	:= vfdio.DS402.SwitchedOn AND gMain.cmd.runMotor;
	

	vfdio.DS402.Stop	:= gMain.cmd.stop;
//	IF EDGENEG(gMain.cmd.runMotor) AND gIO.dout.DriveEnable THEN
//		vfdio.DS402.Stop	:= TRUE;
//	END_IF
	
	(* Cancel all commands when the axis is not powered *)
	IF EDGENEG(vfdio.DS402.SwitchedOn) THEN
		vfdio.DS402.MoveVelocity	:= FALSE;
		vfdio.DS402.Stop			:= FALSE;
	END_IF
	
	
	//Crimp Assembly functionality
//	ProdSpeedStatusOld := ProdSpeedStatusNew;
//	ProdSpeedStatusNew := ProdSpeedPotStatus;
//	ProdSpeedRef := gIO.aiProdSpeedPot;
//	
//	IF ProdSpeedStatusNew = 3 AND ProdSpeedStatusOld = 2 THEN
//		CrimpLimitActive := TRUE;
//	END_IF
//	
//	IF CrimpLimitActive AND ProdSpeedPotStatus = 0 THEN
//		CrimpLimitActive := FALSE;
//	END_IF
	
	// Determine source of prod/cut speed signals
	IF gMain.sts.overrideInPrg THEN
		ProdSpeedRef := gMain.sts.overrideSpeedRef;
	ELSIF gIO.ai.ProdSpeedPot.status = AI_STS_OPEN_LINE OR gIO.ai.ProdSpeedPot.status = AI_STS_HI_LIM THEN
		ProdSpeedRef := gIO.ai.CutSpeedPot.scaledValue;
	ELSIF (*gIO.di.CrimperActive AND*) NOT(gIO.di.CrimperIsClear) THEN
		ProdSpeedRef := gIO.ai.CutSpeedPot.scaledValue; 
	ELSE
		ProdSpeedRef := gIO.ai.ProdSpeedPot.scaledValue;
	END_IF
	
	IF gMain.sts.overrideInPrg THEN
		CutSpeedRef	:= gMain.sts.overrideSpeedRef;
	ELSIF gIO.ai.CutSpeedPot.status = AI_STS_OPEN_LINE OR gIO.ai.CutSpeedPot.status = AI_STS_HI_LIM THEN
		CutSpeedRef	:= 0;
	ELSE
		CutSpeedRef := gIO.ai.CutSpeedPot.scaledValue;
	END_IF
	
	// make sure that used cut speed is never more than production speed, except for when cutting
	// operator didn't like this because they often run the machine from the knobs instead of the screen or buttons
	//CutSpeedRef := LIMIT(0, CutSpeedRef, ProdSpeedRef);
	
	(* Scale the speed to the reading Production speed and cut speed potentiometers are giving *)
	LCRLimScal_ProdPot.x	:= ProdSpeedRef;
	LCRLimScal_ProdPot.x1	:= 0.0;
	LCRLimScal_ProdPot.x2	:= 100.0;
	LCRLimScal_ProdPot.y1	:= 0;
	LCRLimScal_ProdPot.y2	:= gMain.sts.topSpeed; // motor speed in rpm
	//Cut speed potentiometer to speed scaling
	LCRLimScal_CutPot.x		:= CutSpeedRef;
	LCRLimScal_CutPot.x1	:= 0.0;
	LCRLimScal_CutPot.x2	:= 100.0;
	LCRLimScal_CutPot.y1	:= 0;
	LCRLimScal_CutPot.y2	:= gMain.sts.topSpeed; // motor speed in rpm
	
	// set speed requests
	gMain.sts.reqSpd.cut := LCRLimScal_CutPot.y;
	gMain.sts.reqSpd.prod := LCRLimScal_ProdPot.y;
	gMain.sts.reqSpd.fine := MIN((gMain.sts.topSpeed * 0.1), gMain.sts.reqSpd.cut);
	
	(* Update speed based on Prod/Cut speeds *)
	IF NOT(gSimulate) THEN
		IF(gMain.cmd.runProdSpd) OR gMain.sts.overrideInPrg THEN	
			PotVelocity	:= REAL_TO_INT(gMain.sts.reqSpd.prod);
		ELSIF(gMain.cmd.runCutSpd) THEN
			PotVelocity	:= REAL_TO_INT(gMain.sts.reqSpd.cut);
		ELSIF(gMain.cmd.runFineSpd) THEN
			PotVelocity := gMain.sts.reqSpd.fine;
		END_IF
	
		IF (ABS(PotVelocity - OldVelocity) >= MIN_VELOCITY_CHANGE) OR (PotVelocity = 0.0) THEN
			vfdio.setVelocity	:= REAL_TO_INT(PotVelocity);
			OldVelocity			:= PotVelocity;
			
			vfdio.DS402.Update	:= TRUE;
		END_IF
		
		LCRLimScal_ProdPot();
		LCRLimScal_CutPot();
		
	//	gIO.dout.DecoilerBrakeGoOn		:= NOT gAxisCntrl.Out.PowerOn OR NOT gAxisCntrl.In.MoveVelocity;
	END_IF
	
	// update the used encoder value, check for errors
	IF (gIO.enc.Value > oldEncoderValue) THEN
		gPart.CntsThisPart	:= gPart.CntsThisPart + (gIO.enc.Value - oldEncoderValue);
		backwardsMoves	:= 0; // reset backwards move counter on forwards movement
	ELSIF (gIO.enc.Value < oldEncoderValue) THEN
		backwardsMoves := backwardsMoves + 1;
	END_IF
	
	IF backwardsMoves > BACKWARDS_MOVES_THRESH THEN
		backwardsMoves	:= 0;
		gAlarmInfo.EdgeAlarmFlags.EncoderBackwards := TRUE;
	END_IF
	
	TON_EncChk.IN := (vfdio.actVelocity > 0) AND (oldEncoderValue = gIO.enc.Value);
	TON_EncChk.PT := T#1s;
	TON_EncChk();
	
	IF EDGEPOS(TON_EncChk.Q) THEN
		gAlarmInfo.EdgeAlarmFlags.EncoderStagnant := TRUE;
	END_IF
	
	oldEncoderValue	:= gIO.enc.Value;
	
	// set vfd alarms
	IF node > 0 THEN
		(* Raise and display VFD error *)
		gVFDio[node].errorNumber := (gVFDio[node].LFT_Input_INT OR gVFDio[node].LFT_Input_UINT);
		ACPietx(gVFDio[node].errorNumber, gMain.sts.ACPiModel, ADR(ErrorTextVFD));
		gAlarmInfo.PersistentAlarmFlags.VFDfault := vfdio.DS402.Error;
		
		gAlarmInfo.PersistentAlarmFlags.VFDcommLost := NOT(vfdio.DS402.ModuleOk);
	END_IF

	// call fbs
	vfdio.DS402();
	
	// get status of motor
	GetStatus;
	
	// reset update cmd
	vfdio.DS402.Update	:= FALSE;
	
END_PROGRAM