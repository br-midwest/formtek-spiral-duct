
ACTION GetNode: 

	IF gSimulate THEN
		// node is always 0 in sim
		node := 0;
		
	ELSE
		// when not simulating, 0 is invalid node
		IF node = 0 THEN
			FOR i := 0 TO (SIZEOF(gVFDio)/SIZEOF(gVFDio[0])) - 1 DO
				IF gVFDio[i].DS402.ModuleOk THEN
					
					CASE i OF
						MCHTYP_12E_230V_P84, 
						MCHTYP_12E_460V_P84, 
						MCHTYP_20E_460V_P84, 
						MCHTYP_20E_400V_P84,
						MCHTYP_16E_460V_P84,
						
						MCHTYP_12E_230V_P66, 
						MCHTYP_12E_460V_P66, 
						MCHTYP_20E_460V_P86, 
						MCHTYP_20E_400V_P86,
						MCHTYP_16E_460V_P86,
						
						MCHTYP_P66_TEST, 
						MCHTYP_P84_TEST, 
						MCHTYP_P86_TEST,
						MCHTYP_P74_TEST:
							(*these nodes are ok*)
							node := i; // stops the loop from scanning again
							gMain.mchType := INT_TO_UDINT(i); // sets the machine type enum
							vfdio ACCESS ADR(gVFDio[node]);
							vfdio.DS402.Enable := TRUE;
						ELSE
							node := -1; // stops the loop from scanning again
							gAlarmInfo.PersistentAlarmFlags.InvalidVFDNode := TRUE;
					END_CASE
					
					EXIT;
				END_IF
			END_FOR 
		END_IF
	END_IF	
	
	IF TON_NodeTO.Q THEN
		node := -1; // stops the loop from scanning again
		gAlarmInfo.PersistentAlarmFlags.NoNodeDetected := TRUE;
	END_IF
	
	TON_NodeTO(IN := (node = 0) AND NOT(gSimulate), PT := T#10s);
	
END_ACTION


ACTION GetStatus:

	// check if decel
	gMotorSts.decelerating := ( vfdio.DS402.InMotion AND ( (prevVel - vfdio.actVelocity) > 3) );
	
	// check if accel
	gMotorSts.accelerating := ( vfdio.DS402.InMotion AND ( (vfdio.actVelocity - prevVel) > 3) );
	
	// check if stopped
	gMotorSts.standstill := (vfdio.actVelocity = 0);
	
	// check if at speed
	gMotorSts.atSpeed := vfdio.DS402.SetVelocityReached AND vfdio.DS402.InMotion;
	
	
	// set "old" values
	prevVel := vfdio.actVelocity;
	
END_ACTION
