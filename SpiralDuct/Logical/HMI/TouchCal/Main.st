(********************************************************
 * Copyright: B&R Industrial Automation
*********************************************************
 * Author:		Brad Jones
 * Created:		2018
*********************************************************
[PROGRAM INFORMATION]
Handles the touch calibration of an HMI if the calibration
is to be started by pushing a specific button on a screen.

Recommend: Put globally accessible hot spot on HMI screen
which sets the calibrationDatapoint to 1

Mandatory: map the calibrationStateDatapoint and
calibrationDatapoint to the visualization
********************************************************)


PROGRAM _CYCLIC

	// manually reset the calibration state datapoint once the calibration has been finished
	IF (calibrationStateDatapoint <> 4 AND calibrationDatapoint = 1) OR TON_CALTO.Q THEN
		calibrationDatapoint	:= 0;
	END_IF	
	
	// time out the calibration after 10 seconds
	TON_CALTO.IN	:= calibrationStateDatapoint = 4;
	TON_CALTO.PT	:= T#10s;
	TON_CALTO();
	
END_PROGRAM

