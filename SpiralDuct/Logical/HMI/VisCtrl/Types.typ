
TYPE
	vis_login_level_enum : 
		(
		LOGIN_OPERATOR := 0,
		LOGIN_ADMIN := 99
		);
	vis_typ : 	STRUCT 
		visibility : vis_visibility_typ;
		datapoints : vis_datapoints_typ;
		popupGeneral : vis_popup_controller_typ;
		colorMaps : vis_colorMap_typ;
		cmd : vis_commands_typ;
		loginLevel : vis_login_level_enum;
		limits : vis_limits_typ;
	END_STRUCT;
	vis_visibility_enum : 
		(
		VIS_SHOW := 0,
		VIS_HIDE := 1
		);
	vis_visibility_typ : 	STRUCT 
		alarmPopup : vis_visibility_enum;
		navButtons : nav_visibility_typ;
		ioForcing : vis_visibility_enum;
		CloseDialogBtn : vis_visibility_enum;
		ConfirmCancelBtns : vis_visibility_enum;
		LoginBtns : vis_visibility_enum;
		adminLevel : vis_visibility_enum;
		alarmRxnStopMachine : vis_visibility_enum;
		alarmRxnMessage : vis_visibility_enum;
		showWhenLaserFire : vis_visibility_enum;
		ImportExportBtns : vis_visibility_enum;
		fileBrowser : vis_visibility_enum;
		trend : vis_visibility_enum;
		hideInManual : vis_visibility_enum;
		showInManual : vis_visibility_enum;
		driveParScreen : vis_visibility_enum;
		showCalInput : vis_visibility_enum;
		showIfNSeq : ARRAY[0..4]OF vis_visibility_enum;
		showTeachOpt : vis_visibility_enum;
		showIf20 : vis_visibility_enum;
	END_STRUCT;
	vis_datapoint_typ : 	STRUCT 
		change : USINT;
		current : USINT;
	END_STRUCT;
	vis_datapoints_typ : 	STRUCT 
		page : vis_datapoint_typ;
		unitAccel : vis_datapoint_typ;
		unitPressure : vis_datapoint_typ;
		unitLength : vis_datapoint_typ;
		unitSpeed : vis_datapoint_typ;
		unitTemp : vis_datapoint_typ;
		language : vis_datapoint_typ;
		calibrationStateDatapoint : INT;
		calibrationDatapoint : INT;
	END_STRUCT;
	nav_visibility_typ : 	STRUCT 
		tabDiag : vis_visibility_enum;
		tabSetup : vis_visibility_enum;
		tabParam : vis_visibility_enum;
		tabMenu : vis_visibility_enum;
		tabHome : vis_visibility_enum;
	END_STRUCT;
	popup_function_enum : 
		(
		POPUP_IDLE := 0,
		POPUP_LOADING_JOB := 10,
		POPUP_AUTOTUNE_SERVO := 12,
		POPUP_SAVE_CHANGES := 13,
		POPUP_LASER_TEST := 14,
		POPUP_CALIB_REQ := 20,
		POPUP_CALIB_WAIT_INPUT := 21,
		POPUP_REQUEST_DENIED := 90,
		POPUP_LASER_TEST_REQUEST_DENIED := 91,
		POPUP_LOGIN := 100,
		POPUP_IMPORT_SETTINGS := 200,
		POPUP_EXPORT_SETTINGS := 201,
		POPUP_EXPORT_HISTORY := 202,
		POPUP_HELP_SECTION := 1000
		);
	vis_popup_controller_typ : 	STRUCT 
		cancel : BOOL;
		confirm : BOOL;
		function : popup_function_enum;
		visibility : vis_visibility_enum;
	END_STRUCT;
	vis_colorMap_typ : 	STRUCT 
		alarmPopupBackColor : USINT;
		diagSubmenu : ARRAY[0..5]OF USINT;
		setupSubmenu : ARRAY[0..5]OF USINT;
		paramSubmenu : ARRAY[0..5]OF USINT;
		attnAutoCal : USINT;
		attnEnableTHC : USINT;
		attnSaveCfg : USINT;
		alarmList : ARRAY[0..ALARM_LIST_SIZE]OF USINT;
		alarmHistory : ARRAY[0..ALARM_LIST_SIZE]OF USINT;
	END_STRUCT;
	vis_commands_typ : 	STRUCT  (*Commands that cannot/should not be mapped directly to the HMI*)
		calibrate : BOOL;
		loginReq : BOOL; (*Login request*)
		exportAlarmHistory : BOOL;
		exportPars : BOOL;
		importPars : BOOL;
	END_STRUCT;
	vis_minmax_typ : 	STRUCT 
		max : REAL;
		min : REAL;
	END_STRUCT;
	vis_limits_typ : 	STRUCT 
		gapHeight : vis_minmax_typ;
		trend : trendlimits_typ;
	END_STRUCT;
	trendlimits_typ : 	STRUCT 
		torque : vis_minmax_typ;
		speed : vis_minmax_typ;
		current : vis_minmax_typ;
	END_STRUCT;
END_TYPE
