PROGRAM _INIT
	
	// hide popups
	vis.visibility.trend				:= VIS_HIDE;
	vis.visibility.fileBrowser 			:= VIS_HIDE;
	vis.visibility.driveParScreen		:= VIS_HIDE;
	
END_PROGRAM
      
PROGRAM _CYCLIC
	
	// show the alarm popup if there is an active alarm and they op is not on diag pages
	IF gAlarmInfo.PendingAlarms > 0 AND vis.datapoints.page.current < PAGE_030_DIAG THEN
		vis.visibility.alarmPopup	:= VIS_SHOW;
		vis.popupGeneral.function   := POPUP_IDLE;
	ELSE
		vis.visibility.alarmPopup	:= VIS_HIDE;
	END_IF
	
	IF gAlarmInfo.rxnStopMachine AND gAlarmInfo.rxnControlOff THEN
		vis.colorMaps.alarmPopupBackColor	:= VIS_SEVERITY_1;
		vis.visibility.alarmRxnStopMachine	:= VIS_SHOW;
		vis.visibility.alarmRxnMessage		:= VIS_HIDE;
	ELSIF gAlarmInfo.rxnStopMachine THEN
		vis.colorMaps.alarmPopupBackColor	:= VIS_SEVERITY_2;
		vis.visibility.alarmRxnStopMachine	:= VIS_SHOW;
		vis.visibility.alarmRxnMessage		:= VIS_HIDE;
	ELSIF gAlarmInfo.rxnMessage THEN
		vis.colorMaps.alarmPopupBackColor	:= VIS_SEVERITY_3;
		vis.visibility.alarmRxnStopMachine	:= VIS_HIDE;
		vis.visibility.alarmRxnMessage		:= VIS_SHOW;
	ELSE
		vis.colorMaps.alarmPopupBackColor	:= VIS_SEVERITY_NONE;
		vis.visibility.alarmRxnMessage		:= VIS_HIDE;
		vis.visibility.alarmRxnStopMachine	:= VIS_HIDE;
	END_IF
	
	// file browsing handling
	gDev.cmd.reqRemoteDevice := (vis.datapoints.page.current = PAGE_002_MENU);
	
	FOR i := 0 TO 2 DO
		remoteSourceDropDownOpt[i] := UDINT_TO_USINT(gDev.sts.devicesStatus[i]);
	END_FOR
	
	// check the login level
	CASE vis.loginLevel OF
		LOGIN_OPERATOR:
			vis.visibility.adminLevel	:= VIS_HIDE;
			gMain.cmd.enableIOForcing	:= FALSE;
		LOGIN_ADMIN:
			vis.visibility.adminLevel	:= VIS_SHOW;
	END_CASE
	
	IF vis.cmd.loginReq THEN
		vis.cmd.loginReq	:= FALSE;
		
		IF vis.loginLevel = LOGIN_OPERATOR THEN
			vis.popupGeneral.function := POPUP_LOGIN;
		ELSE
			vis.loginLevel	:= LOGIN_OPERATOR;
		END_IF
	END_IF
	
	// control the commands that require extra ack to perform
	CASE vis.popupGeneral.function OF
		POPUP_IDLE:
			vis.popupGeneral.cancel		:= FALSE;
			vis.popupGeneral.confirm	:= FALSE;
			
			IF gMain.sts.loadingJob THEN
				vis.popupGeneral.function := POPUP_LOADING_JOB;
			ELSIF vis.cmd.calibrate THEN
				vis.cmd.calibrate := FALSE;
				
				IF (gMain.opMode = OP_MODE_MANUAL) AND NOT(gIO.di.LngSns) AND (gMain.step = MAIN_READY) AND NOT(gMain.sts.partAtMaxLng) THEN
					vis.popupGeneral.function := POPUP_CALIB_REQ;
				ELSE
					gAlarmInfo.EdgeAlarmFlags.CannotCalibrate := TRUE;
				END_IF
			ELSIF gMain.sts.calInPrg AND EDGEPOS(gIO.di.LngSns) THEN
				vis.popupGeneral.function := POPUP_CALIB_WAIT_INPUT;
			END_IF
			
		POPUP_LOADING_JOB:
			IF NOT(gMain.sts.loadingJob) THEN
				vis.popupGeneral.function := POPUP_IDLE;
			END_IF
			
		POPUP_CALIB_REQ:
			IF vis.popupGeneral.confirm THEN
				gMain.cmd.reqCal := TRUE;
				vis.popupGeneral.function := POPUP_IDLE;
			ELSIF vis.popupGeneral.cancel THEN
				vis.popupGeneral.function := POPUP_IDLE;
			END_IF	
			
		POPUP_CALIB_WAIT_INPUT:
			IF gCal.calPartLng > 0.0 THEN
				vis.popupGeneral.function := POPUP_IDLE;
			ELSIF vis.popupGeneral.cancel THEN
				gMain.cmd.reqAbortCal := TRUE;
				vis.popupGeneral.function := POPUP_IDLE;
			END_IF
			
		POPUP_LOGIN:
			IF vis.popupGeneral.cancel OR vis.loginLevel > LOGIN_OPERATOR THEN
				vis.popupGeneral.function	:= POPUP_IDLE;
			END_IF
			
		ELSE
			IF vis.popupGeneral.cancel THEN
				vis.popupGeneral.cancel 	:= FALSE;
				vis.popupGeneral.function	:= POPUP_IDLE;
			END_IF
	END_CASE;

	IF (vis.popupGeneral.function > POPUP_IDLE) THEN
		vis.popupGeneral.visibility	:= VIS_SHOW;
	ELSE
		vis.popupGeneral.visibility	:= VIS_HIDE;
	END_IF
	
	IF (vis.popupGeneral.function = POPUP_CALIB_REQ)THEN
		vis.visibility.showTeachOpt := VIS_HIDE; //VIS_SHOW; Requested to indefinitely hide the teach option
	ELSE
		vis.visibility.showTeachOpt := VIS_HIDE;
	END_IF
	
	IF (vis.popupGeneral.function = POPUP_CALIB_WAIT_INPUT)THEN
		vis.visibility.showCalInput := VIS_SHOW;
	ELSE
		vis.visibility.showCalInput := VIS_HIDE;
	END_IF

	popupFunction	:= vis.popupGeneral.function;
	CASE popupFunction OF
		POPUP_IDLE:
			vis.visibility.ConfirmCancelBtns	:= VIS_HIDE;
			vis.visibility.CloseDialogBtn		:= VIS_HIDE;
			vis.visibility.LoginBtns			:= VIS_HIDE;
			vis.visibility.ImportExportBtns		:= VIS_HIDE;
		1..99:
			vis.visibility.ConfirmCancelBtns	:= VIS_SHOW;
			vis.visibility.CloseDialogBtn		:= VIS_HIDE;
			vis.visibility.LoginBtns			:= VIS_HIDE;
			vis.visibility.ImportExportBtns		:= VIS_HIDE;
		POPUP_LOGIN:
			vis.visibility.ConfirmCancelBtns	:= VIS_HIDE;
			vis.visibility.CloseDialogBtn		:= VIS_SHOW;
			vis.visibility.LoginBtns			:= VIS_SHOW;
			vis.visibility.ImportExportBtns		:= VIS_HIDE;
		ELSE
			vis.visibility.ConfirmCancelBtns	:= VIS_HIDE;
			vis.visibility.CloseDialogBtn		:= VIS_SHOW;
			vis.visibility.LoginBtns			:= VIS_HIDE;
			vis.visibility.ImportExportBtns		:= VIS_HIDE;
	END_CASE
	
	CASE vis.datapoints.page.current OF
		1: 		vis.visibility.navButtons.tabHome		:= VIS_HIDE;
				vis.visibility.navButtons.tabMenu		:= VIS_SHOW;
				vis.visibility.navButtons.tabParam		:= VIS_SHOW;
				vis.visibility.navButtons.tabSetup		:= VIS_SHOW;
				vis.visibility.navButtons.tabDiag		:= VIS_SHOW;
			
		2..9:   vis.visibility.navButtons.tabHome		:= VIS_SHOW;
				vis.visibility.navButtons.tabMenu		:= VIS_HIDE;
				vis.visibility.navButtons.tabParam		:= VIS_SHOW;
				vis.visibility.navButtons.tabSetup		:= VIS_SHOW;
				vis.visibility.navButtons.tabDiag		:= VIS_SHOW;
			
		10..19: vis.visibility.navButtons.tabHome		:= VIS_SHOW;
				vis.visibility.navButtons.tabMenu		:= VIS_SHOW;
				vis.visibility.navButtons.tabParam		:= VIS_HIDE;
				vis.visibility.navButtons.tabSetup		:= VIS_SHOW;
				vis.visibility.navButtons.tabDiag		:= VIS_SHOW;
			
		20..29: vis.visibility.navButtons.tabHome		:= VIS_SHOW;
				vis.visibility.navButtons.tabMenu		:= VIS_SHOW;
				vis.visibility.navButtons.tabParam		:= VIS_SHOW;
				vis.visibility.navButtons.tabSetup		:= VIS_HIDE;
				vis.visibility.navButtons.tabDiag		:= VIS_SHOW;
			
		30..39: vis.visibility.navButtons.tabHome		:= VIS_SHOW;
				vis.visibility.navButtons.tabMenu		:= VIS_SHOW;
				vis.visibility.navButtons.tabParam		:= VIS_SHOW;
				vis.visibility.navButtons.tabSetup		:= VIS_SHOW;
				vis.visibility.navButtons.tabDiag		:= VIS_HIDE;
	END_CASE
	
	// alarm list highlighting
	FOR i := 0 TO ALARM_LIST_SIZE - 1 DO
		IF mAlarmHistorySelectedIndex = INT_TO_UINT(i) THEN
			vis.colorMaps.alarmHistory[i]	:= 1;
		ELSE
			vis.colorMaps.alarmHistory[i]	:= 0;
		END_IF
		
		IF mAlarmListSelectedIndex = INT_TO_UINT(i) THEN
			vis.colorMaps.alarmList[i]	:= 1;
		ELSE
			vis.colorMaps.alarmList[i]	:= 0;
		END_IF
	END_FOR
	
	// tab highlighting
	vis.colorMaps.diagSubmenu[0]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_030_DIAG);
	vis.colorMaps.diagSubmenu[1]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_031_DETAIL);
	vis.colorMaps.diagSubmenu[2]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_032_HISTORY);
	vis.colorMaps.diagSubmenu[3]	:= BOOL_TO_USINT((vis.datapoints.page.current >= PAGE_033_IO) AND (vis.datapoints.page.current <= PAGE_037_IO));
	vis.colorMaps.diagSubmenu[4]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_038_SDM);
	
	vis.colorMaps.paramSubmenu[0]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_010_DATETIME);
	vis.colorMaps.paramSubmenu[1]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_011_DELAY);
	vis.colorMaps.paramSubmenu[2]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_012_NETWORK);
	vis.colorMaps.paramSubmenu[3]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_013_WINDOWS);
	vis.colorMaps.paramSubmenu[4]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_014_GENERAL);
	
	vis.colorMaps.setupSubmenu[0]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_020_SETUP);
	vis.colorMaps.setupSubmenu[1]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_021_PRESSURES);
	vis.colorMaps.setupSubmenu[2]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_022_SPARE);
	vis.colorMaps.setupSubmenu[3]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_023_SPARE);
	vis.colorMaps.setupSubmenu[4]	:= BOOL_TO_USINT(vis.datapoints.page.current = PAGE_024_SPARE);
	
	TON_Init(IN := (vis.datapoints.page.current = 0), PT := T#5s);
	
	IF TON_Init.Q THEN
		vis.datapoints.page.change	:= 1;
	END_IF	

	IF gMain.cmd.enableIOForcing THEN
		vis.visibility.ioForcing	:= VIS_SHOW;
	ELSE
		vis.visibility.ioForcing	:= VIS_HIDE;
	END_IF

	IF nextIOpage AND (vis.datapoints.page.current < PAGE_037_IO) THEN
		vis.datapoints.page.change := vis.datapoints.page.current + 1;
	ELSIF prevIOpage AND (vis.datapoints.page.current > PAGE_033_IO) THEN
		vis.datapoints.page.change := vis.datapoints.page.current - 1;
	END_IF
	
	nextIOpage := FALSE;
	prevIOpage := FALSE;
	
	// set trend limits
	CASE gMain.mchType OF
		MCHTYP_P66_TEST, MCHTYP_P86_TEST, MCHTYP_P84_TEST, MCHTYP_P74_TEST:
			vis.limits.trend.torque.min := -2000.0; // 0.1 %
			vis.limits.trend.torque.max := 2000.0; // 0.1 %
			
			vis.limits.trend.current.min := -100.0; // 0.1A
			vis.limits.trend.current.max := 100.0; // 0.1A
			
			vis.limits.trend.speed.min := 0; // RPM
			vis.limits.trend.speed.max := gMain.sts.topSpeed; // RPM
		
		// 1.2E 230V
		MCHTYP_12E_230V_P84, MCHTYP_12E_230V_P66:
			vis.limits.trend.torque.min := -2000.0; // 0.1 %
			vis.limits.trend.torque.max := 2000.0; // 0.1 %
			
			vis.limits.trend.current.min := -500.0; // 0.1A
			vis.limits.trend.current.max := 500.0; // 0.1A
			
			vis.limits.trend.speed.min := 0; // RPM
			vis.limits.trend.speed.max := gMain.sts.topSpeed; // RPM
		
		// 1.2E 460V
		MCHTYP_12E_460V_P84, MCHTYP_12E_460V_P66:
			vis.limits.trend.torque.min := -2000.0; // 0.1 %
			vis.limits.trend.torque.max := 2000.0; // 0.1 %
			
			vis.limits.trend.current.min := -300.0; // 0.1A
			vis.limits.trend.current.max := 300.0; // 0.1A
			
			vis.limits.trend.speed.min := 0; // RPM
			vis.limits.trend.speed.max := gMain.sts.topSpeed; // RPM
		
		// 2.0E 460V
		MCHTYP_20E_460V_P84, MCHTYP_20E_460V_P86:
			vis.limits.trend.torque.min := -2000.0; // 0.1 %
			vis.limits.trend.torque.max := 2000.0; // 0.1 %
			
			vis.limits.trend.current.min := -500.0; // 0.1A
			vis.limits.trend.current.max := 500.0; // 0.1A
			
			vis.limits.trend.speed.min := 0; // RPM
			vis.limits.trend.speed.max := gMain.sts.topSpeed; // RPM
		
		// 1.6E 460V
		MCHTYP_16E_460V_P84, MCHTYP_16E_460V_P86:
			vis.limits.trend.torque.min := -2000.0; // 0.1 %
			vis.limits.trend.torque.max := 2000.0; // 0.1 %
			
			vis.limits.trend.current.min := -300.0; // 0.1A
			vis.limits.trend.current.max := 300.0; // 0.1A
			
			vis.limits.trend.speed.min := 0; // RPM
			vis.limits.trend.speed.max := gMain.sts.topSpeed; // RPM
			
		// 2.0E 400V
		MCHTYP_20E_400V_P84, MCHTYP_20E_400V_P86:
			vis.limits.trend.torque.min := -2000.0; // 0.1 %
			vis.limits.trend.torque.max := 2000.0; // 0.1 %
			
			vis.limits.trend.current.min := -500.0; // 0.1A
			vis.limits.trend.current.max := 500.0; // 0.1A
			
			vis.limits.trend.speed.min := 0; // RPM
		vis.limits.trend.speed.max := gMain.sts.topSpeed; // RPM
		
	END_CASE
	
	// other visibility flags
	IF gMain.opMode <> OP_MODE_MANUAL THEN
		vis.visibility.showInManual	:= VIS_HIDE;
		vis.visibility.hideInManual := VIS_SHOW;
	ELSE
		vis.visibility.showInManual := VIS_SHOW;
		vis.visibility.hideInManual := VIS_HIDE;
	END_IF
	
	CASE gMain.mchType OF
		MCHTYP_20E_460V_P84, MCHTYP_20E_460V_P86, 0:
			vis.visibility.showIf20 := VIS_SHOW;
		ELSE
			vis.visibility.showIf20 := VIS_HIDE;
	END_CASE
	
	// load in preferences
	vis.datapoints.language.change := gMain.par.pref.language;
	vis.datapoints.unitLength.change := gMain.par.pref.lengthUnit;
	vis.datapoints.unitPressure.change := gMain.par.pref.pressureUnit;
	
	// program editing interface
	FOR i := 0 TO 4 DO
		IF i < gMain.par.totalSeq THEN
			vis.visibility.showIfNSeq[i] := VIS_SHOW;
		ELSE
			vis.visibility.showIfNSeq[i] := VIS_HIDE;
		END_IF
	END_FOR
	
END_PROGRAM
