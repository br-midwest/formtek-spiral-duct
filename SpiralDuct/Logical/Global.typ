(*ALARM*)

TYPE
	Alarm_info_edgealarms_typ : 	STRUCT  (*Flags set by other programs in alarm conditions, and absorbed by the alarm handler*)
		FailedToResetEncoder : BOOL;
		FileLoadSuccess : BOOL;
		FileLoadFailed : BOOL;
		WriteParSuccess : BOOL;
		WriteParFailed : BOOL;
		CannotCalibrate : BOOL;
		CalibrationAborted : BOOL;
		CalInstructions : BOOL;
		EncoderBackwards : BOOL;
		EncoderStagnant : BOOL;
		CannotUsePart : BOOL;
		CannotStart : BOOL;
		CuttingNotAllowed : BOOL;
		DischargeNotAllowed : BOOL;
		ModeChangeWhileRunning : BOOL;
		JobFinished : BOOL;
		JobRestarted : BOOL;
		NetworkError : BOOL;
		NoModeSelected : BOOL;
		HydraulicPumpOff : BOOL;
		PartTooLong : BOOL;
		CalLatchFailed : BOOL;
		IdleInActiveState : BOOL;
		OvertravelLimit : BOOL;
		NodeUpdateNotification : BOOL;
	END_STRUCT;
	Alarm_info_persistentalarms_typ : 	STRUCT  (*Flags set by other programs in alarm conditions, and absorbed by the alarm handler*)
		IOAlarm : BOOL;
		NoNodeDetected : BOOL;
		InvalidVFDNode : BOOL;
		VFDfault : BOOL;
		VFDcommLost : BOOL;
		EstopActive : BOOL;
		AirPressureFault : BOOL;
		PumpOverload : BOOL;
		AutotuneActive : BOOL;
	END_STRUCT;
	Alarm_info_typ : 	STRUCT 
		PendingAlarms : UDINT;
		ActiveAlarms : UDINT;
		rxnMessage : BOOL;
		rxnControlOff : BOOL;
		rxnStopMachine : BOOL;
		EdgeAlarmFlags : Alarm_info_edgealarms_typ;
		PersistentAlarmFlags : Alarm_info_persistentalarms_typ;
	END_STRUCT;
END_TYPE

(*MAIN*)

TYPE
	machine_type_enum : 
		(
		MCHTYP_SIM := 0,
		MCHTYP_UNDEF_1 := 1,
		MCHTYP_P66_TEST := 2,
		MCHTYP_P84_TEST := 3,
		MCHTYP_P86_TEST := 4,
		MCHTYP_P74_TEST := 5,
		MCHTYP_12E_230V_P84 := 10,
		MCHTYP_12E_460V_P84 := 11,
		MCHTYP_20E_460V_P84 := 12,
		MCHTYP_16E_460V_P84 := 13,
		MCHTYP_20E_400V_P84 := 14,
		MCHTYP_12E_230V_P66 := 20,
		MCHTYP_12E_460V_P66 := 21,
		MCHTYP_20E_460V_P86 := 22,
		MCHTYP_16E_460V_P86 := 23,
		MCHTYP_20E_400V_P86 := 24
		);
	machine_op_mode_enum : 
		(
		OP_MODE_NONE := 0,
		OP_MODE_SETUP := 1,
		OP_MODE_MANUAL := 2,
		OP_MODE_AUTO1 := 3,
		OP_MODE_AUTO2 := 4
		);
	StateMachine_enum : 
		(
		MAIN_NULL := 0,
		MAIN_RESETTING := 10,
		MAIN_IDLE := 20,
		MAIN_READY := 30,
		MAIN_PRODUCING := 40,
		MAIN_FINISH_PROD := 41,
		MAIN_CHECK_LNG := 42,
		MAIN_START_CUT := 50,
		MAIN_CUTTING := 51,
		MAIN_DISCHARGE := 60,
		MAIN_CALIBRATING := 70,
		MAIN_ERROR := 99
		);
	main_typ : 	STRUCT 
		mchType : machine_type_enum;
		opMode : machine_op_mode_enum;
		step : StateMachine_enum;
		cmd : main_cmd_typ;
		par : main_par_typ;
		sts : main_sts_typ;
	END_STRUCT;
	main_sts_typ : 	STRUCT 
		controlOn : BOOL;
		autoMode : BOOL; (*TRUE if mode is AUTO1 or AUTO2 or any other AUTO mode*)
		loadingJob : BOOL;
		jobRunning : BOOL; (*Automatic job is currently in progress*)
		seqIdx : INT; (*Which sequence is currently being produced*)
		totalParts : UINT; (*Total number of ducts programmed in the current job (as calculated from valid sequences)*)
		cutsMadeInCurrSeq : UINT; (*Number of cuts performed in the current production sequence. Resets to 0 once the current sequence is finished and the next one is started*)
		partsReqInCurrSeq : UINT; (*Number of parts requested to be produced in the current sequence*)
		calInPrg : BOOL; (*Calibration is in progress*)
		overrideInPrg : BOOL;
		overrideSpeedRef : REAL; (*Speed reference to use when overriding speed controls, %*)
		reqSpd : speeds_typ;
		topSpeed : REAL;
		materialUsed : LREAL;
		partAtMaxLng : BOOL;
		dischClear : BOOL; (*Discharge is "clear" based on timer elapsed*)
		ACPiModel : ACPi_Model_enum;
	END_STRUCT;
	main_par_typ : 	STRUCT 
		stripThickness : REAL;
		materialGauge : gauge_select_enum; (*Gauge selector for 2.0 style machines. Switches lighter gauge allows faster run speeds*)
		materialWidth : REAL; (*Width of stock (post crimping) in mm*)
		fhDiam : REAL; (*Diameter of duct expected to be produced by the installed forming head in mm*)
		autoCutDelay : REAL; (*Delays the start of the cutting action, seconds*)
		autoSlowStartDelay : REAL; (*Time spend at cut speed when starting a new part in auto, seconds*)
		dischRetractDelay : REAL; (*Time to dwell in st21 before starting production of duct, seconds*)
		dischDelay : REAL; (*Minimum time that the discharge cylinder should be on for, seconds*)
		cutDwell : REAL; (*How long to wait knife engages, seconds*)
		brakeOnTime : REAL; (*seconds*)
		totalSeq : INT; (*Total number of sequences programmed in the current job (as indicated by non-zero lengths)*)
		manualLng : REAL; (*Used to be ManLngReq. Requested length of duct to be produced in manual mode, in mm*)
		lowSpeedFinishLng : REAL; (*Used to be AutoCutSpeedLng. Additional duct length (in mm) to be produced at cut speed*)
		userOvershoot : REAL; (*Length to be compensated for while stopping. If there is a consistent offset, operator can modify this value to affect the stopping behavior*)
		useTeachSeqInCal : BOOL; (*Whether or not to use "teach" sequence in the calibration routine*)
		pref : main_par_pref_typ; (*Machine preferences*)
		seq : ARRAY[0..50]OF sequence_typ; (*Recipe sequence information*)
	END_STRUCT;
	main_cmd_typ : 	STRUCT 
		reset : BOOL;
		stop : BOOL;
		enableIOForcing : BOOL;
		runProdSpd : BOOL; (*Call for the motor to run at production speed*)
		runCutSpd : BOOL; (*Call for the motor to run at cut speed*)
		runFineSpd : BOOL; (*Call for the motor to run at very slow speed*)
		runMotor : BOOL; (*Command to the DS402 block to run the VFD (moveVelocity)*)
		clearCurrentRcp : BOOL; (*Clears out the currently active recipe*)
		clearMaterialUsage : BOOL; (*Resets the current material usage accumulated*)
		reqCal : BOOL; (*Request calibration routine to begin*)
		runCal : BOOL; (*Commands movement on the motor during calibration*)
		reqAbortCal : BOOL; (*Requests calibration to abort*)
		runOverride : BOOL; (*Command to override the speed command (from decteach)*)
		restartJob : BOOL; (*Requests a restart for the current configured job*)
		loadRemote : BOOL; (*Load job from remote file*)
	END_STRUCT;
	speeds_typ : 	STRUCT 
		fine : REAL; (*Speed in RPM*)
		prod : REAL; (*Speed in RPM*)
		cut : REAL; (*Speed in RPM*)
	END_STRUCT;
	sequence_typ : 	STRUCT 
		Qty : INT; (*Number of ducts to be made in a given sequence. Each index is a sequence. Index 0 is not part of the recipe, and is used for operator entry of programs*)
		Lng : REAL; (*Length of duct to be made in a given sequence. Each index is a sequence. Index 0 is not part of the recipe, and is used for operator entry of programs*)
	END_STRUCT;
	main_par_pref_typ : 	STRUCT  (*User preference parameters*)
		pressureUnit : USINT;
		lengthUnit : USINT;
		language : USINT;
	END_STRUCT;
	gauge_select_enum : 
		(
		GAUGE_HEAVY := 0,
		GAUGE_LIGHT := 1
		);
	motor_sts_typ : 	STRUCT 
		standstill : BOOL; (*not moving / 0 velocity*)
		atSpeed : BOOL;
		accelerating : BOOL;
		decelerating : BOOL;
		autotuneState : drive_tuning_state_enum;
	END_STRUCT;
END_TYPE

(*DEVICE*)

TYPE
	device_typ : 	STRUCT 
		sts : device_sts_typ;
		cmd : device_cmd_typ;
		par : {REDUND_UNREPLICABLE} device_par_typ;
	END_STRUCT;
	device_par_typ : 	STRUCT 
		usb : USBDevChkPar_typ;
		cifs : CIFSDevChkPar_typ;
		selectedFile : STRING[80];
	END_STRUCT;
	device_cmd_typ : 	STRUCT 
		reqRemoteDevice : BOOL;
		refresh : BOOL;
		exportAlarmHistory : BOOL;
		exportPars : BOOL;
		importPars : BOOL;
	END_STRUCT;
	device_sts_typ : 	STRUCT 
		devicesStatus : ARRAY[0..2]OF file_device_status_enum;
		selectedDevice : file_devices_enum;
		deviceName : STRING[80];
		operationInProgress : BOOL;
	END_STRUCT;
	file_device_status_enum : 
		(
		DEV_OK := 0,
		DEV_NOT_OK := 2
		);
	file_devices_enum : 
		(
		FILE_DEVICE_LOCAL := 0,
		FILE_DEVICE_WINDOWS := 1,
		FILE_DEVICE_USB := 2
		);
END_TYPE

(*IO*)

TYPE
	IO_typ : 	STRUCT 
		di : IO_di_typ;
		dout : IO_do_typ;
		ai : IO_ai_typ;
		ao : IO_ao_typ;
		enc : IO_enc_typ;
	END_STRUCT;
	IO_enc_typ : 	STRUCT 
		LatchCount : SINT;
		LatchEnable : {REDUND_UNREPLICABLE} BOOL;
		LatchValue : {REDUND_UNREPLICABLE} DINT;
		Reset : BOOL;
		Value : DINT;
	END_STRUCT;
	IO_ao_typ : 	STRUCT 
		PotVoltageSupply : INT := 32767; (*Should always be max value (output 10 v)*)
	END_STRUCT;
	IO_ai_typ : 	STRUCT 
		CutSpeedPot : Analog_typ; (*Potentiometer for cut speed reference*)
		ProdSpeedPot : Analog_typ; (*Potentiometer for production speed reference*)
	END_STRUCT;
	IO_do_typ : 	STRUCT 
		ClearDriveFault : {REDUND_UNREPLICABLE} BOOL;
		ClinchRollGoUp : {REDUND_UNREPLICABLE} BOOL;
		ControlOnLight : {REDUND_UNREPLICABLE} BOOL;
		CR_UP_WO_Estop : {REDUND_UNREPLICABLE} BOOL; (*Output not used?*)
		CutterGoUp : {REDUND_UNREPLICABLE} BOOL;
		CutterOnLight : {REDUND_UNREPLICABLE} BOOL;
		DecoilerBrakeGoOn : {REDUND_UNREPLICABLE} BOOL;
		DischargeGoUp : {REDUND_UNREPLICABLE} BOOL;
		DischargeLight : {REDUND_UNREPLICABLE} BOOL;
		DriveEnable : {REDUND_UNREPLICABLE} BOOL;
		HomeJumper : {REDUND_UNREPLICABLE} BOOL; (*See diHomeJumper*)
		HydPumpGoOn : {REDUND_UNREPLICABLE} BOOL;
		LubePumpGoOn : {REDUND_UNREPLICABLE} BOOL;
		ProdOnLight : {REDUND_UNREPLICABLE} BOOL;
		UnlockCutterSlide : {REDUND_UNREPLICABLE} BOOL;
		VFDContactor : {REDUND_UNREPLICABLE} BOOL; (*Using a contactor to supply 3-phase to VFD*)
	END_STRUCT;
	IO_di_typ : 	STRUCT 
		AirPressureOk : {REDUND_UNREPLICABLE} BOOL; (*Pneumatic sensor?*)
		Auto1ModeSelected : {REDUND_UNREPLICABLE} BOOL; (*User input - mode selector switch*)
		Auto2ModeSelected : {REDUND_UNREPLICABLE} BOOL; (*User input - mode selector switch*)
		ClinchUpSelect : {REDUND_UNREPLICABLE} BOOL; (*User input - clincher up selector switch*)
		CntrlOffReleased : {REDUND_UNREPLICABLE} BOOL; (*User input - control off button*)
		ControlOnPressed : {REDUND_UNREPLICABLE} BOOL; (*User input - control on button*)
		CrimperActive : {REDUND_UNREPLICABLE} BOOL; (*Switch on the crimper cabinet that activates the crimper*)
		CrimperIsClear : {REDUND_UNREPLICABLE} BOOL; (*Limit switch on the crimper mechanism which indicates that the crimper is clear or out of the way of duct production*)
		CutterOnPressed : {REDUND_UNREPLICABLE} BOOL; (*User input - cutter button*)
		CutterSlideLimit : {REDUND_UNREPLICABLE} BOOL; (*Limit switch - cutter*)
		DischargeOnPressed : {REDUND_UNREPLICABLE} BOOL; (*User input - discharge button*)
		HomeJumper : {REDUND_UNREPLICABLE} BOOL; (*Jump the DO from CM8281 module to DI of encoder module; homing reference that sets RawEncoderValue to zero.*)
		HydPressureIsLow : {REDUND_UNREPLICABLE} BOOL; (*Pneumatic/hydraulic sensor?*)
		HydPumpOnSelect : {REDUND_UNREPLICABLE} BOOL; (*User input - hydraulic pump on selector switch*)
		LubePumpOnSelect : {REDUND_UNREPLICABLE} BOOL; (*User input - lube pump selector switch*)
		ManualModeSelected : {REDUND_UNREPLICABLE} BOOL; (*User input - mode selector switch*)
		ProdOnPressed : {REDUND_UNREPLICABLE} BOOL; (*User input - start production*)
		PumpOverloadTrip : {REDUND_UNREPLICABLE} BOOL; (*?*)
		SetupModeSelected : {REDUND_UNREPLICABLE} BOOL; (*User input - mode selector switch*)
		StopBtnReleased : {REDUND_UNREPLICABLE} BOOL; (*User input - stop button*)
		TravelLimIsClear : {REDUND_UNREPLICABLE} BOOL; (*?*)
		LngSns : {REDUND_UNREPLICABLE} BOOL; (*Part detection photo eye sensor *)
		VFDError : {REDUND_UNREPLICABLE} BOOL; (*Drive feedback - error being reported*)
	END_STRUCT;
	Analog_typ : 	STRUCT 
		status : Analog_status_enum;
		scaledValue : REAL;
	END_STRUCT;
END_TYPE

(*OTHER*)

TYPE
	part_production_typ : 	STRUCT 
		LngToMake : REAL; (*Used to be LengthRequested. Requested length of duct to be produced, in mm*)
		LngProduced : LREAL; (*Used to be TubeLngProduced. Length of duct produced, in mm*)
		LngToMakeAtProdSpd : LREAL; (*Used to be TargetTubeLng. Length of duct to be produced at production speed (higher speed), in mm*)
		LngToMakeCnts : LREAL; (*Used to be TargetCount. Length of duct to be produced, in encoder counts*)
		ProdDecelCnts : LREAL; (*Used to be ProductionDecelTime. Estimated counts it will take to decelerate from production speed to cut speed*)
		CutDecelCnts : LREAL; (*Used to be ExpectedOvershoot. Expected number of counts it will take to decelerate from cut speed to standstill*)
		FineDecelCnts : LREAL;
		CntsThisPart : DINT;
	END_STRUCT;
	Calibration_typ : 	STRUCT 
		calcSnsDist : LREAL; (*Calculated distance from the cut edge to the photo eye in mm*)
		latchedSnsCnts : LREAL; (*Number of encoder counts from cut edge to photo eye*)
		calPartCnts : LREAL; (*Length of duct produced during calibration in encoder counts*)
		calPartLng : LREAL; (*Length of duct produced during calibration. Measured distance from knife to end of produced duct in mm*)
		EncLatchValBfr : ARRAY[0..9]OF DINT; (*History of last 10 encoder latches*)
	END_STRUCT;
	vfd_typ : 	STRUCT 
		LFT_Input_UINT : UINT;
		LFT_Input_INT : INT;
		errorNumber : INT;
		actVelocity : INT; (*in rpm*)
		setVelocity : INT; (*in rpm*)
		DS402 : DS402Basic; (*DS402 control FUB*)
		current : UINT; (*in A*)
		torque : INT; (*in %*)
	END_STRUCT;
	drive_tuning_state_enum : 
		(
		TUNE_NOT_DONE := 0,
		TUNE_PENDING := 1,
		TUNE_IN_PRG := 2,
		TUNE_FAILED := 3,
		TUNE_DONE := 4,
		TUNE_ENTERED_R1 := 5,
		TUNE_CUSTOMIZED := 6
		);
	Analog_status_enum : 
		(
		AI_STS_OK := 0, (*00*)
		AI_STS_LOW_LIM := 1, (*01*)
		AI_STS_HI_LIM := 2, (*10*)
		AI_STS_OPEN_LINE := 3 (*11*)
		);
END_TYPE
